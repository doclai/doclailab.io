---
title: "Cái mỏ không răng, những con spider, và ranh giới mối quan hệ"
date: 2024-04-12T06:45:11+07:00
tags: [beaks, extinction, consumerism, sexism, neurodiversity, boundaries]
draft: false
---

Một vài thứ tản mạn đầu ngày khi đợi xe bus:

- [Một thảo luận](https://www.smithsonianmag.com/science-nature/why-birds-survived-and-dinosaurs-went-extinct-after-asteroid-hit-earth-180975801/) về chuyện vì sao những loài chim có mỏ, cũng như các loài thằn lằn giống chim khác, như là các loài khủng long duy nhất đã vượt qua được kỳ đại tuyệt chủng cách đây 66 triệu năm. Với những gì đang tìm thấy thì họ đặt ra câu chuyện rằng, sự thật là thời gian dài trước đó đã có sự tách biệt khi những họ hàng khác của loài chim bắt đầu bớt sử dụng răng, dẫn đến việc chúng tiến hóa chỉ còn mỏ để mổ thay vì nhai, phù hợp với chế độ ăn thực vật, hạt. Và chính mỏ không răng này đã giúp chúng sống sót với nguồn thức ăn hạn hẹp sau khi những cánh rừng bị tàn phá.

- Spider quét của OpenAI [bị mắc lại](https://mailman.nanog.org/pipermail/nanog/2024-April/225407.html) ở một trang web có cấu trúc kỳ dị với hàng triệu website con giống nhau và đều chứa robots.txt

- Kỹ thuật [thao túng tâm lý](https://www.psychologytoday.com/intl/blog/the-social-consumer/202404/how-temu-uses-psychological-hacks-to-encourage-overspending) với sự khan hiếm để gia tăng sự tiêu thụ được Temu sử dụng trên nền tảng của họ. Mới nổi lên gần đây, Temu, một nền tảng mua sắm trực tuyến được phát triển bởi Pinduoduo - một công ty có trụ sở tại Trung Quốc.

- Nhìn sự phân biệt giới tính (sexism) như là [một đồng xu có hai mặt](https://www.psychologytoday.com/intl/blog/compassionate-feminism/202404/hostile-and-benevolent-sexism-two-sides-of-the-same-coin) (hostile và benevolent), với cùng một lối tư tưởng về tính thứ bậc phân cấp, chỉ khác biệt ở cách ứng xử ban đầu. Nhìn rộng ra, chợt nghĩ về sự nhạy cảm trong việc nhìn thấy các mô hình kiểu này đã và đang được thiết kế để đỡ vướng phải cản chở và dễ tiếp cận hơn.

- [Ẩn dụ](https://www.psychologytoday.com/intl/blog/beyond-mental-health/202404/playing-with-5-whole-decks-an-autism-metaphor) để hiểu hơn về người tự kỷ (hoặc những người neurodiverse nói chung): so sánh với việc lấy lá Joker từ trong bộ bài. Với một bộ bài thì người tự kỷ và người bình thường tiếp cận cách lấy lá Joker khác nhau, hơn nữa, người tự kỷ còn có nhiều bộ bài cần xử lý hơn người bình thường do họ bị ngợp trong tiếp nhận các giác quan.

[6 lĩnh vực xác lập ranh giới trong mối quan hệ](https://www.psychologytoday.com/intl/blog/what-mentally-strong-people-dont-do/202404/6-boundaries-that-will-strengthen-your-relationship) gồm cả một số câu hỏi gợi ý, được tóm tắt lại từ cuốn sách _13 Things Mentally Strong Couples Don’t Do_ của tác giả, để thảo luận với partner của bạn, đó là: physical, financial, social, sexual, emotional, time.
