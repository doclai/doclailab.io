---
title: "Đọc người khác viết"
date: 2023-05-08T07:08:10+07:00
tags: ["reading"]
toc: "false"
draft: false
---

Cũng là cùng dịp sau khi đi thiền lần nữa về, một khóa tôi ở lại với thực tại và hướng tới tương lai nhiều hơn so với khóa thiền cũ trước kia, tôi không định viết về những trải nghiệm tôi có trong khóa thiền. 
Có một điều tôi nhận ra khi đọc lại các bài viết bằng tiếng mẹ đẻ của các tác giả Việt Nam. Phải mất một thời gian để nhận thức được sự xung hấn bên trong khi đọc các bài viết buổi sáng sau khi thức dậy tầm 4 tiếng. Đó có thể xuất phát từ nhiều nguyên nhân, nhưng dù sao thì tôi thấy đây là một thời điểm thật phù hợp để được đắm chìm trong ngôn ngữ tiếng mẹ đẻ.
Nhẹ nhàng lướt qua một vài bài viết trên mạng rồi chợp mắt vài phút trước khi lên đường.
