---
title: "Mối quan hệ đơn phương: Căn tính và lòng tự trọng"
date: 2022-10-10T07:57:32+07:00
tags: [unrequited, identities, self-esteem]
toc: "false"
draft: false
---

![](../don_quixote.jpg)

Nhìn quá trình trải qua một tình yêu đơn phương có lẽ là cách để tôi bắt đầu vài dòng ngắn ngủi ghi chép về chủ đề của tháng sắp được thảo luận ở ba miền, một tháng Mười bàn về căn tính và lòng tự trọng.

Bàn qua về các căn tính (identities ~ self-concept), hay gọi là, các hình dung về bản thân, tôi nghĩ về chuyện, chúng hình thành nên các thói quen tâm trí (habits of mind, một khái niệm được chúng ta thảo luận tháng vừa rồi về sự học suốt đời). Các căn tính này ảnh hưởng tới động lực để bản thân ta có xu hướng thực hiện những hoạt động khớp với, và tránh thực hiện các hoạt động đi ngược lại, các hình dung này. Khi các căn tính của cá nhân này tham gia vào quá trình đánh giá (self-verification) bởi một hệ thống giá trị và sau đó đạt được một chuẩn nhất định, cá nhân đó có được lòng tự trọng (self-esteem).

Quay trở lại câu chuyện ban đầu, trong suốt cuộc đời, quá trình tương tác với người khác ảnh hưởng tới việc tự xây dựng hình dung về bản thân (qua sự hình dung cách người khác nhìn bản thân, cùng cách hiểu cảm xúc người khác) liên hệ tới cả hai bên của một mối quan hệ đơn phương. Trên thực tế, khi mô hình của sự cuồng nhiệt theo đuổi và sau đó đạt thành công trong mối quan hệ đơn phương lãng mạn được xuất hiện nhiều và nuôi dưỡng trên phương tiện truyền thông và trong các cộng đồng, chúng ta biết với tình huống đó có thể làm gì (và thực tế hầu hết các thứ chúng ta làm đều học qua cách này), nhưng rất ít tài liệu cho chúng ta biết về để học cách đưa ra sự từ chối và buông bỏ. Với nhu cầu cơ bản của một sinh vật xã hội, từ đây, ta có thể thấy một song đề khi người nhận tình cảm không muốn đưa các thông điệp từ chối rõ ràng, và người trao đi tình cảm không muốn nghe những thông điệp từ chối đó. Một trong số các câu chuyện chính của song đề này là về lòng tự trọng, xuất hiện ở cả hai phía của mối quan hệ này. Hai bên đều diễn ra quá trình cảm thấy được chấp nhận, chỉ là theo chiều hướng và cách khác nhau. Người được nhận tình cảm trải qua quá trình này, tăng cường sự tự trọng của họ, cho tới khi họ nhận ra đây không phải một đối tác phù hợp, và giảm sự quan trọng của sự chấp nhận này xuống. Ở phía bên kia, người trao đi tình cảm, họ có thể đặt kỳ vọng để xác nhận giá trị bản thân trong mối quan hệ này, theo đó mang âm hưởng tới cảm nhận chung về những mối quan hệ thân mật họ có thể có về sau. Dù sao thì, nếu mối quan hệ thất bại, có nhiều cách khôi phục lại mức độ lòng tự trọng mong muốn, như có thể hình dung về sự liên hệ giữa căn tính và lòng tự trọng, đơn giản nhất có lẽ sẽ đi xây dựng những mối quan hệ mới, để bản thân cảm thấy được chấp nhận.

Tôi kết thúc những ghi chép ngắn này bằng một câu nói của Robert Louis Stevenson, liên quan đến sự tự lực (self-reliance) tháng Chín chúng ta vừa được thảo luận, như sau: “Life is not a matter of holding good cards, but of playing a poor hand well” (Cuộc sống không phải là chuyện bạn sở hữu các quân bài tốt, mà là việc bạn chơi hay các quân bài xấu).

<br>

[Bài viết](https://tamlyungdung.org/moi-quan-he-don-phuong-can-tinh-va-long-tu-trong/) được tôi đăng trên website Nhóm học Tâm lý ứng dụng, trong mục Chia sẻ học viên

<small>
Tài liệu tham khảo:
<ul>
<li>Alicia D. Cast and Peter J. Burke (Mar., 2002). A theory of self-esteem, pp. 1041-1068, Social Forces, Vol. 80, No. 3.</li>
<li>Crocker, J., & Park, L. E. (2004). The costly pursuit of self-esteem,  pp. 392–414, Psychological Bulletin, 130.</li>
<li>Daphna Oyserman, Kristen Elmore, George Smith (2012), Self, Self-Concept, and Identity, Handbook of Self and Identity, pp. 69-104, The Guilford Press.</li>
<li>Jennifer K. Bosson, William B. Swann, JR (2009), Self-esteem, Handbook of Individual Difference, pp. 527 – 546, The Guilford Press.</li>
<li>Laurie H. Ervin and Sheldon Stryker (2001), Theorizing the Relationship between Self-Esteem and Identity, Extending Self-Esteem Theory and Research Sociological and Psychological Currents, Cambridge University Press, pp. 29 – 55.</li>
<li>James Henslin (2015), Socialization, pp.65-94, C.3, Ess. Of Sociology, Pearson, 2015.</li>
<li>Roy F. Baumeister, Dawn Dhavale, pp. 55 – 71, Two sides of Romantic Rejection, Interpersonal Rejection, Oxford University Press, 2001.</li>
</ul>
</small>