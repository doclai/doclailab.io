+++
title = "Đạp xe xuyên Đông Dương - Phần 11"
date = "2025-01-18"
+++

## Ngày 27

Dừng lại mua cốc nước mía gần trưa thì có dịp ngồi trò chuyện. Chiếc xe đạp làm đầu câu chuyện để cô chú hàng nước mía hỏi chi tiết thêm về hành trình của tôi đã qua. Chú tầm U60, chỉ vào chiếc xe máy, đã ấp ủ từ lâu dự định xuyên Việt với vợ chú. Vợ chú hỏi tôi có qua Thái Lan gặp được sư Minh Tuệ không, có quay video không. Rồi tạm biệt cô chú tôi lên đường.

Trên từng bước đạp tôi nghĩ xem có nên truyền tải thông tin như nào. Hồi 2020 tôi vác máy quay khắp nơi, nhưng rồi thấy tôi không tập trung vào trải nghiệm được. Tôi không vào vai quần chúng khi cầm máy quay được. 

Blogging lại bằng viết chữ và hình ảnh như này có thể kén độc giả hơn, đặc biệt những khán giả có tuổi. Cơ mà viết giúp tôi sắp xếp được lại những gì tôi trải qua và có thể đưa được những cảm nhận mà với khả năng làm video của tôi khó diễn đạt (đó có vẻ cũng là lý do tôi không tập trung được vào trải nghiệm). Trước thì tôi lọ mọ một trang web riêng cơ mà giờ sẽ duy trì thêm cả Facebook thường xuyên hơn.

Sau gần một tháng trên đường với sự tĩnh lặng và rộng lớn của thiên nhiên, tôi vẫn chưa thích nghi được với sự chật chội, ồn ào của Sài Gòn, và chắc chắn, Hà Nội cũng vậy, nếu tôi trở về. Tôi nhớ thời gian vẫn còn tĩnh lặng mấy ngày trước, trước khi bị phá vỡ, ở Bình Dương và Bình Phước.

Đã tính ở lì trong phòng, trò chuyện với một chú người Đài Loan thì lại có động lực lết ra ngoài đường kiếm cái gì đó để ăn. Vẫn cứ 2 suất cơm, xong ngồi im trong một góc quán viết những dòng này.

Chính thức kết thúc hành trình của tôi sau 27 ngày.
