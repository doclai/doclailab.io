---
layout: post
title: Từ ký ức sai lệch tới nhu cầu về nhận thức
date: 2022-05-25T09:28:55+07:00
tags: [reading, memories]
---

Hỏi hết người này người kia về một sự kiện diễn ra và mọi người đều khẳng định chuyện tôi kể không tồn tại. Nó không đơn thuần là một lỗi đơn giản về trí nhớ như việc bạn quên hay nhớ sai chi tiết, mà có thứ gì đó nhả keo những hình ảnh thực tế không xuất hiện cùng nhau và khiến cho tôi có cảm giác rất chắc chắn về chúng. 

Sự không chắc chắn của trí nhớ khi ảnh hưởng bởi cách miêu tả thông tin được nghiên cứu bởi Loftus và Palmer vào năm 1974. Trong thí nghiệm theo dõi một tai nạn, từ một thay đổi về cách dùng từ ngữ để miêu tả cách va chạm của hai xe trong câu hỏi các nhân chứng tham gia, ký ức một tuần sau đấy của họ có thể xây dựng theo một cách khác hẳn.

Tính dễ bị ảnh hưởng của trí nhớ lớn hơn với những người cao trong nhu cầu về nhận thức (Need for Cognition – NC) thực hiện bởi Graham vào năm 2007. Cụm từ về khác biệt mặt cá nhân này được xây dựng qua một thời gian dài. Khởi đầu với ý tưởng của Cohen, Stotland, và Wolfe vào năm 1955, NC được khái niệm hóa như “một nhu cầu tạo nghĩa về thế giới”. Vào 1982, theo Cacioppo và Petty, NC phản ánh một động cơ nội tại ổn định được phát triển theo thời gian, hơn là nhu cầu về cảm quan truyền thống. Ý tưởng này được hỗ trợ bởi nghiên cứu chỉ ra rằng NC chỉ liên hệ mức trung bình với năng lực nhận thức, và tiếp tục tiên đoán các kết quả liên quan sau khi năng lực nhận thức được kiểm soát (Cacioppo và cộng sự, 1996). Cacioppo và Petty phát triển cấu trúc NC trong thời đại Lý thuyết quá trình kép bắt đầu nổi trong tâm lý học xã hội. Lý thuyết quá trình kép nói về cơ chế hình thành suy nghĩ của chúng ta gồm quá trình ẩn tàng mang tính vô thức (unconscious) và quá trình hiện tàng mang tính nhận thức (conscious). Trong bối cảnh của Lý thuyết quá trình kép này, NC được dùng như cách xác định cơ chế một đánh giá cá nhân được hình thành và thay đổi. Một số lượng đáng kể các nghiên cứu chỉ ra rằng các cá nhân thấp trong NC, khi không có những động cơ khuyến khích như các cá nhân cao trong NC, thường chỉ dựa vào những tín hiệu đơn giản trong các tình huống thuyết phục, và các khuôn mẫu, để đánh giá người khác, so với các cá nhân cao trong NC. Các cá nhân cao trong NC có xu hướng xét toàn bộ các thông tin liên quan. Xu hướng này của các cá nhân cao trong NC cũng giúp họ liên kết các thông tin có liên quan về mặt ngữ nghĩa, nhưng không xuất hiện, như trong nghiên cứu của Graham, và hiện ra nhiều các ký ức sai lệch hơn.

Tài liệu tham khảo: 
- [The Misinformation Effect and False Memories](https://www.verywellmind.com/what-is-the-misinformation-effect-2795353), truy cập ngày 25/05/2022
- [Need For Cognition - Wikipedia](https://en.wikipedia.org/wiki/Need_for_cognition), truy cập ngày 25/05/2022
- Handbook of Individual Differences in Social Behavior (2009), Mark R. Leary, Rick H. Hoyle.