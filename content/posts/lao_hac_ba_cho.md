---
layout: post
title: Nam Cao có nên cho Lão Hạc dùng bả chó?
date: 2022-05-19T09:28:55+07:00
tags: [tlud]
---

![](../009s.vi.jpg)

Nam Cao là một nhà văn hiện thực xuất sắc với các sáng tác về nội dung người nông dân nghèo đói bị vùi dập và người trí thức nghèo sống mòn mỏi, bế tắc trong xã hội cũ. Nhiều người vì tò mò đã dành thời gian đọc tượng đài “Lão Hạc” và đều cảm giác ớn lạnh đến rùng mình, rồi phải thốt lên đó là một tác phẩm văn học “vô đạo đức”.

Nhiều tờ báo đưa tiêu đề nổi bật trên trang nhất “Nam Cao xin lỗi và ẩn tác phẩm Lão Hạc”. Những đoạn cuối của truyện ngắn Lão Hạc cổ xúy cho hành động tự tử, vô đạo đức, cần phải gỡ bỏ ngay, tránh tiếp xúc với giới trẻ.

Một nhân vật nổi tiếng có tầm ảnh hưởng chia sẻ quan điểm gay gắt: "Tác phẩm nhiều những thông điệp tiêu cực, và những gợi ý cho những hành động ngông cuồng, bất mãn và gây rối xã hội. Chả lẽ giờ đây ý tưởng đã cạn kiệt đến thế, đến nỗi để muốn có cái mới, người ta phải đem vào nghệ thuật cả những thứ như thế này? Trong khi tất cả chúng ta từng ngày từng giờ còn đang cố gắng ngăn chặn những suy nghĩ tiêu cực, mất phương hướng nơi các bạn trẻ… Cả tác phẩm có thể là những cô đơn tuyệt vọng, nhưng hãy nên là ánh sáng loé lên ở những giây cuối cùng. Cuộc sống này đâu bao giờ hết những thứ để yêu thương”.

Trong khi đó, một người khác chia sẻ: "Nam Cao đã nhận được hàng triệu lời tung hô, chúc mừng qua các thế hệ. Nam Cao không biết nhiều cha mẹ sẽ giật mình sợ hãi khi con họ đọc trích đoạn này. Nhất là khi liên tục những vụ nhảy lầu tự tử đã diễn ra khiến các bậc làm cha, làm mẹ chưa hết bàng hoàng.

Nếu Nam Cao biết, việc phân biệt giữa phim ảnh với ngoài đời thực là khó khăn với lũ trẻ. Nếu Nam Cao biết, tự tử có tính lây lan rất mạnh. Nếu Nam Cao biết lũ trẻ chưa đầy đủ nhận thức, thậm chí hầu hết phim Hàn đều có câu quen thuộc khi mở đầu phim: Câu chuyện trong phim chỉ là hư cấu. Mỗi sản phẩm khi đưa ra đại chúng, người ta luôn phải gắn nhãn giới hạn độ tuổi.”

Chúng ta hãy cùng thảo luận xem, từ việc liệu Nam Cao có nên cho Lão Hạc dùng bả chó?, rộng hơn là đến cách thể hiện của tác giả và tác phẩm văn học trong đời sống.