---
layout: post
title: Sự không mạch lạc (2)
date: 2020-06-15T09:28:55+07:00
tags: [learning, books]
---

Từ bài viết đầu tiên về sự-không-mạch-lạc, tôi thấy việc trích dẫn nhiều trong khi viết làm cho những thông tin không được "nội địa". Tiếp theo, suy nghĩ bản thân sau khi viết cũng không mạch lạc, với mục đích ban đầu là để hiểu được suy nghĩ bản thân và các ý tưởng hơn.

Có thể là trong quá trình mình học phổ thông, mình đã vô tình nhìn nhận việc có được một bài viết nhiều trích dẫn, hoặc từ trên phim ảnh, việc xuất ra một thứ gì đó trừu tượng, bằng lời hay bằng văn bản, là một điều gì đó hay ho. Nhiều workshop họ có thể dạy cách viết thể hiện những gì bản thân muốn, nhưng lại chưa chỉ người học cách viết trung thực.

Trở về với cuốn sách của David Bohm, ông có đưa ra khái niệm _những vật tạo tác_ (artifacts) vào định nghĩa của tư duy. 

> [...] Hệ thống máy tính điện tử, các nhạc cụ, xe hơi, nhà cao tầng - tất cả những cái đó là minh họa của tư duy dưới dạng _cố định, cụ thể_ của nó. Theo quan điểm của Bohm, tách rời một cách cơ bản ý nghĩ với các sản phẩm của nó, thì chẳng khác nào bảo rằng một ai đó là đàn ông hay đàn bà là một hiện tượng chẳng dính dáng gì với quá trình di truyền vốn xác định giới tính từ ban đầu. [...]

Khó để một người có thể nhận ra vấn đề của một tư duy khi họ bám vào chính những vật-tạo-tác, sản phẩm của kiểu tư duy đó, để củng cố lại niềm tin.

Chẳng hạn, trong cuốn _Dẫn nhập về hiểu biết_ (Knowledge: A very short introduction) có nhắc tới việc open các câu hỏi là một phương pháp gia tăng lỗi lo âu.
