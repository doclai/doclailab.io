+++
image = "cross_indochina_01.jpg"
date = "2024-12-21"
title = "Đạp xe xuyên Đông Dương - Phần 1"
type = "gallery"
+++


## Ngày 1

Chào buổi sáng mọi người.

Buổi sáng chủ nhật yên bình.

Mình đã nhanh chóng kịp rời nội thành trước 9h.

Hiện đang nghỉ chân ở Khoái Cầu, Thường Tín, Hà Nội.

Ở Phủ Lý, Hà Nam nhiều người nước ngoài phết. 

Mấy lần đạp ngang qua một vài nhóm người châu Á. Gốc Tàu hay Hàn gì đó.

Càng đi càng thấy đường thủ đô Hà Nội vừa xấu mặt đường mà lại ít cây đẹp. Đi Ninh Bình cả ngày không bị vấp cái ổ gà nào.

Như ảnh đây thì cả trục đường này ở Hoa Lư toàn cây si thân to đùng :))

Bắt gặp anh tài Bằng Kiều xuất hiện ở buổi event cho Highland Coffee ở TP Ninh Bình.

## Ngày 2

Chào tạm biệt anh Masato Ohya để hôm nay anh lên đường vào Huế.

Mình từng vào Tràng An rồi nên hôm nay sẽ không đi sông nước nữa, tiếp tục check-in các điểm khác ở cố đô.

Mộ vua Lê Đại Hành và những câu chuyện tình, chuyện nhà, chuyện công việc của những người từng đứng đầu quốc gia từ bà bán nước kể như một hướng dẫn viên du lịch. Tiếc là mình không còn tiền mặt gửi bà mặc dù mình không cần mua gì.

Fun fact sau khi tìm hiểu về Mr Lê Đại Hành thì các bạn quên gì thì quên chứ không thể quên rằng: ngài là vị vua đầu tiên có phong cách không quỳ khi nhận sắc phong từ phương Bắc.

Einar là một chàng trai đến từ Tây Ban Nha. 
Anh ta kể một cách đầy thú vị về tài xế xe khách vừa mở vinahouse vừa phóng bạt mạng trên đường, (vãi cả) lỡ phà Cát Bà (và được thu xếp thuyền riêng), và đồ ăn của Việt Nam cầu kỳ hơn đồ ăn Tây Ban Nha như nào.
Mai anh ta sẽ đi Hang Múa.
Các bạn có thể thấy anh ta mặc áo cộc và anh ta bảo lên Sapa anh ta đã phải mua áo khoác.

Hãy giữ sức khoẻ như Einar =))

## Ngày 3

Bà Triệu là nữ nhân độc thân chí lớn, nhận nguồn cảm hứng từ sớm của hai idol thời bấy giờ là Trưng Trắc và Trưng Nhị. Núi Tùng khu căn cứ Bồ Điền (hiện giờ là thôn Phú Điền) đây là nơi có lăng mộ của Bà Triệu và 3 vị tướng họ Lý.

Chỗ mình đứng lần lượt là cầu Hàm Long (xa xa là cầu Hàm Rồng), quảng trường Hàm Rồng to không kém quảng trường gì đó ở Hà Nội (nhiều cây hơn rất rất nhiều), và Thiền viện Trúc Lâm Hàm Rồng

Gặp anh giai người Đức đạp tour 7 tháng từ Đức, đích đến Australia. Giang hồ không hẹn mà gặp.

## Ngày 6

Một trong số các kinh nghiệm đạp xe đường dài là: người bản địa chào ta bằng ngôn ngữ nào thì ta chào lại bằng ngôn ngữ đó, thể hiện sự tôn trọng họ.

Kinh nghiệm này áp dụng với cả đồng bào ta ở Việt Nam 🤣

Mai rời Hà Tĩnh rồi, nhớ lắm lúc trời còn khô ráo, với những bờ biển đẹp, hoang sơ, dài hút tầm mắt.

Sự hiện diện của mình làm cơ hội cho cô bán phở người Bắc tâm sự rằng, đến hơn 50 rồi cũng chưa đi được đâu, quanh quẩn hết với con rồi lại đến cháu, làm ở đâu hay ở đó. Hẳn cô đang tiếc thời thanh xuân lắm.

## Ngày 7

Dự kiến kế hoạch sẽ thay đổi, mình sẽ không đạp thẳng vào Đà Nẵng, mình sẽ quay lại để xe ở gần đường AH15 (ranh giới Nghệ An, Hà Tĩnh). Để vào họp team trong Đà Nẵng, mình sẽ dùng xe khách, sau đó trở lại AH15 để bắt đầu hành trình sang Lào qua cửa khẩu Cầu Treo. 
Có ba lý do: 
- Thời tiết là một (cuối tuần sau mạn Quảng Bình - Đà Nẵng có mưa to 10mm, Nghệ An thời tiết đang ổn)
- Hai là mức độ bảo dưỡng (mình mang đồ vệ sinh bảo dưỡng xe đi cùng, và đi dính bùn sẽ phải bảo dưỡng xe liên tục hàng ngày thay vì tầm 250km)
- Ba là chặng đường quay lại một trong số các cửa khẩu từ Đà Nẵng không hấp dẫn nếu có 2 lý do trên (kiểu gì mình cũng phải quay lại nếu vào Đà Nẵng)

Bằng cách nào đó, Hà Tĩnh đã níu chân mình được tới 3 ngày với nhiều câu chuyện và suy nghĩ về người và đất Hà Tĩnh.
Hà Tĩnh cho mình trải nghiệm được 80km đạp dưới trời mưa bay bay như thế nào để có thể ra được quyết định sớm về chặng đường sắp tới.
Sáng nay có vẻ Hà Tĩnh tiễn mình đi bằng một tiết trời khô ráo, cùng bát súp lươn làm bữa trưa.

Yêu Hà Tĩnh!

Đã tìm được chỗ trọ cho em nó trước mặt trời lặn. Gặp bác giai từng qua Campuchia luôn. Nhìn bác phong độ không cơ chứ.

Yêu Hà Tĩnh!

Ngày thứ 8 trong hành trình của mình sẽ bắt đầu trở lại vào 30/12, sau khi mình trở về từ Đà Nẵng.

