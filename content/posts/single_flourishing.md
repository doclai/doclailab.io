---
title: "Độc thân và hưng thịnh"
date: 2023-09-21T06:45:11+07:00
tags: ["single", "flourishing", "singlism", ]
draft: false
---

Ở [một thảo luận dài 23 trang về chủ đề độc thân](https://onlinelibrary.wiley.com/doi/epdf/10.1111/jftr.12525), góc nhìn căp-đôi-trung-tâm và những diễn ngôn cặp-đôi-trung-tâm đang được coi như là tiêu chuẩn dùng để soi xét đời sống độc thân, dẫn tới sự xói mòn các tự sự của đời sống độc thân.
