---
title: "Màn đêm tâm hồn"
date: 2023-04-12T11:08:15+07:00
tags: ["soul"]
toc: "false"
draft: false
---

Với chấn thương thể chất trong vài tháng, tôi đã được tặng kèm thêm tấm vé một chiều trên con đường tinh thần. Nhờ có sự từ chối tham vấn tâm lý, tôi đã thưởng thức nó theo một cách rất riêng. 

Trong hầu hết thời gian đó, tôi ở một mình trong phòng riêng. Các bữa ăn hàng ngày tôi được nhận tại đây, do cần hạn chế vận động. Sau đấy là những bước tiếp theo tôi rời xa dần với đời sống xã hội: từ chối nhận lời qua thăm hỏi của đồng nghiệp, chỉ thỉnh thoảng Internet được tôi bật lên để quay lại tương tác với một vài người quen. Sau đó, dành thời gian đi qua đi lại với vài cuốn sách cũ cạnh cửa sổ trở thành hoạt động thường ngày của tôi. Một số người bạn sau tháng đầu tiên không nhận được thông tin gì từ tôi, cũng như đứng từ con người tôi trước đó, chia sẻ sự khó chịu với một lối sống "bí bách" như vậy, sẽ tìm cách gọi tên cho nó, bắt đầu đi tìm biểu hiện, và đưa ra "chẩn đoán". Có thời điểm, tôi đã mất gần chục giây mới giật mình biết rằng tôi đang làm một hành động mà tôi biết là không nên làm. 

Cơ mà, rồi khi thấy được sự mong manh của ý thức đó, tôi đi qua thời gian này một cách chậm rãi, trong lúc nhấm nháp tất cả sự vô định, bất lực, chán chường, ghê sợ, tôi nhận ra rằng tôi coi nó như một tài sản. Ngày qua ngày, tôi thấy nó chẳng thể mất đi, và tôi mong rằng nó không kết thúc quá sớm.

Tôi kết thúc những chia sẻ ngắn này bằng câu từ của một nhà thơ mà tôi mang theo trong hành trình: "Mùa hạ nhất định sẽ đến. Nhưng mùa hạ chỉ đến cho những kẻ nào biết chờ đợi, chờ đợi một cách trầm lặng và cởi mở như là mình đã có cả vĩnh cử trước mắt mình."
