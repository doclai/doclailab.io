---
layout: post
title: Những bước đi
date: 2022-01-16T09:28:55+07:00
tags: [mind]
---

Cái tâm trí 

nhảy giữa các phân cảnh của trò chơi, 

những người bạn mình thật dễ thương làm sao.

Cảm giác rượu tan. 

Bước trên con đường mưa của Võng Thị. 

Những ký ức hòa với nhau 

như cách vị của rượu trộn lẫn spaghetti. 

Đôi môi đỏ của người này. Cái liếc mắt của người kia. 

Những cặp kính. Những đuôi áo. Những sợi tóc mái. 

Những nụ cười. Những sự im lặng. 

Những cái nhìn nhau giản dị. 

Nhìn thấy câu chuyện của tâm.

Sự ngập ngừng trong câu chữ. 

Cái áo mưa mua thừa. 

Thấy mình thiếu sự khiêm tốn với người khác. Dựa dẫm. 

Thầm cảm ơn sự khoan dung.

Rồi lại bước đi dưới mưa đêm phố gần nhà. 