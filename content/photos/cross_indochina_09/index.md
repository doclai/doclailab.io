+++
image = "cross_indochina_09.jpg"
date = "2025-01-15"
title = "Đạp xe xuyên Đông Dương - Phần 9"
type = "gallery"
+++

## Ngày 23

Đường quốc lộ 7 của Campuchia bắt đầu xịn lên từ sau cầu hữu nghị Campuchia - Trung Quốc, sau 50km tới đoạn xịn nhất thì mịn như cao tốc Hà Nội - Hải Phòng. Mịn được như này mà suôn sẻ thì đóng 3 ngày về tới biên giới Việt Nam, kịp vé về Hà Nội vào 23/1.
Do hôm qua nằm gần quốc lộ quá, không sâu giấc được, nay mình 2 đĩa cơm, 2 tô mỳ, 2 cốc mía, vẫn lờ đờ đạp không lên, đành hạ trại sớm, mai tính sau.

Kết thúc ngày 23 gần Phumi Prêk Preah

## Ngày 24

Sáng sớm nay làm bữa sáng như này gần Phumi Prêk Preah. Mình ban đầu bất ngờ vì giá đắt gấp đôi với suất bình thường, xong mang ra và ăn thì đúng no gấp đôi bình thường thật, đỡ phải gọi 2 suất =)). Ban đầu mình thắc mắc, được anh khách lái xe tải giải thích một xíu, rồi sau nhận ra là phở sốt vang. So sánh lại, thì ăn no với ngon hơn sốt vang lúc ăn ở một quán đông khách ở Thường Tín, Hà Nội ngày 1 khởi hành.

Giang hồ không hẹn mà gặp (phần 4)

Sáng đạp đến gần Sre Sbov thì gặp đội này. Nay đội giang hồ này đông đáo để, già trẻ đều có cả. Gia đình người Pháp này xuất phát từ quê hương, đi qua Uzbekistan, có vẻ gần giống như anh người Đức mình có kể ở "Giang hồ không hẹn mà gặp (phần1)". Đoàn có đôi vợ chồng cùng 3 bé: một bé 5 tuổi ngồi trên xe đạp đôi cùng người bố, một bé 9 tuổi, và bé lớn nhất 13 tuổi. Họ đã đi từ Bắc vào Nam Việt Nam và giờ vòng lại quay về nhà. Các bé được dẫn đi từ sớm này tuyệt vời ghê.

Đường quốc lộ 7 của Campuchia vẫn bằng phẳng và cực mịn, trừ đoạn ngã ba rẽ vào Krong Kracheh đang sửa bụi mù bụi đỏ, nên mình thong thả đóng ngon lành gần 100km hôm nay.

Bữa thứ hai trong ngày ở khu ăn đêm Krong Kracheh. Vài con tôm, dăm ba miếng thịt, xúc xích. Carb nay xài chán chê rồi, giờ nghỉ thì mình chỉ thêm protein thôi.

Chỗ mình ngồi này ở giữa khu ăn đêm, đối diện cạnh nhà hát của tỉnh Kratie. Đoạn này sáng đèn nhất khu, đông vui với tụ tập nhiều bạn trẻ.
