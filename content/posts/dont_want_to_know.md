---
layout: post
title: Bên dưới nhu cầu "không muốn biết"
date: 2022-05-12T09:28:55+07:00
tags: [tlud]
---

![](../005s.vi.jpg)

“Em có thể làm thế được. Nhưng chỉ đơn giản là anh không muốn biết. Dù là yêu nhau nhưng không phải chúng ta sẽ cùng chung một thế giới. ”

Dù thân mật, chúng ta cũng như những con nhím của Schopenhauer. Với nhu cầu muốn được sưởi ấm trong mùa đông lạnh giá, những con nhím xích lại gần nhau hơn. Khi xích lại gần nhau, chúng lại bị chính những cái gai nhọn đâm vào mình, và rồi chúng phải tách nhau ra. Những con nhím sẽ dần học được khoảng cách nào là an toàn nhất để vừa đủ ấm và không bị những cái gai đâm vào mình.