---
layout: post
title: "Nỗi đau buồn: Mối quan hệ với người đã khuất"
date: 2022-05-14T09:28:55+07:00
tags: [tlud]
---

![](../007s.vi.jpg)

Nỗi đau buồn: Mối quan hệ với người đã khuất

Nếu chúng ta chia sẻ cuộc sống với một người, chúng ta dồn tâm trí vào quá khứ, chúng ta bắt đầu có những kỳ vọng và giấc mơ. Chúng ta thấy khó để khít khớp cái mất của họ với tương lai định trước của mình. Về bản chất, mối quan hệ tiếp diễn này khớp vào một mạng lưới nối liền ký ức của ta về họ khi còn sống với những ký ức gần đây về sự ra đi của họ, và trải nghiệm của ta về một cuộc đời thiếu họ.

Cuộc thảo luận này sẽ cùng bàn về chuyện tìm một nơi chốn mới cho những ký ức và cảm xúc sau khi người thân yêu ra đi.