---
title: "Mối quan hệ với thiên nhiên: các trào lưu hiện đại"
date: 2022-08-08T13:38:26+07:00
tags: [tlud]
draft: false
---

![](../016s.vi.jpg)

(Từ giờ ở các bản mô tả của sự kiện sẽ có một mục nhỏ ở đầu mang tên TL;DR tóm tắt nội dung giới thiệu sự kiện bên cạnh nội dung giới thiệu sự kiện chính)

__TL;DR (tóm tắt nội dung giới thiệu sự kiện):__

Bạn nghĩ sao về mối quan hệ của chúng ta với thiên nhiên qua hai trào lưu số hóa và thương mại hóa thiên nhiên?

__Nội dung giới thiệu sự kiện:__

Chúng ta đang đến điểm mà khả năng con người có thể giả lập thị giác được hầu hết mọi thứ, liệu chúng ta có thể số hóa thiên nhiên trong một thời đại khan hiếm cơ hội trải nghiệm với thiên nhiên thật? Việc đưa được thiên nhiên vào trải nghiệm từ các kênh truyền hình thế giới động vật, vào điện ảnh 3D, vào trò chơi điện tử, trong những thiết bị thực tế ảo, tới những tấm màn hình vô cực siêu nét trên tường ở các khu trung tâm thương mại hay có khiến cho bạn tin vào hiện thực đấy không? Vào năm 2009, Peter H. Kahn và cộng sự tại đại học Washington đã thực hiện một nghiên cứu nổi tiếng về mối quan hệ của con người với thiên nhiên và phiên bản số hóa của thiên nhiên. Phiên bản số hóa này được tạo trong một căn phòng, bằng cách lắp đặt camera lên đỉnh của một tòa nhà trong khuôn viên trường đại học, và hiển thị lại quang cảnh thiên nhiên xung quanh theo thời gian thực trên một “cửa sổ” màn hình plasma 50-inch. Ông và cộng sự so sánh tác động sinh lý và tâm lý của trải nghiệm phiên bản số hóa này, với trải nghiệm cửa sổ kính thông thường có cùng một khung cảnh, và với trải nghiệm không có cửa sổ trong phòng. Ông và cộng sự phát hiện rằng để phục hồi nhịp tim từ căng thẳng mức độ thấp, căn phòng có cửa sổ kính thiên nhiên có tính phục hồi cao hơn là căn phòng không có cửa sổ. Với cùng một đo lường về sinh lý như vậy, “cửa sổ” màn hình lại không mang lại khác biệt gì so với trường hợp không có cửa sổ. Tuy nhiên, điều này không có nghĩa rằng, phiên bản số hóa thiên nhiên không có tác dụng. Ở trong một nghiên cứu khác, ông và cộng sự tìm ra rằng những người làm việc thời gian dài trong một văn phòng với “cửa sổ” như vậy được báo cáo có những tín hiệu tích cực hơn là không hề có cửa sổ, cho dù nó không được tốt như cửa sổ thiên nhiên thật.

Một trào lưu khác xuất hiện liên quan tới thiên nhiên khi nhiều người nhìn thấy cơ hội đó để kinh doanh, dù ít nhiều với suy nghĩ rằng việc tạo điều kiện cho mọi người đều có tiếp xúc dễ dàng và nhanh chóng tới thiên nhiên hơn sẽ giải quyết được vấn đề của trào lưu ban đầu trên? Kể từ khi chúng ta tiết kiệm thời gian hơn, chúng ta bắt đầu đánh mất dần một quá trình nằm giữa, mà có lẽ nhiều thế hệ sau sẽ không còn ý thức sự tồn tại của nó: sự giao thoa thế giới loài người và thiên nhiên hoang dã. Không còn những sự nỗ lực và hứng khởi của việc đi bộ, định hướng và xử lý những trục trặc nhỏ xảy ra trên con đường. Nó đã từng đem tới một sự tiếp chạm sâu thẳm và vĩ đại theo cách “rời khỏi nơi trú ngụ của loài người, và rồi trở về” của những con người cổ xưa. Những đoàn thợ săn bắt đầu tách lẻ ra thành từng nhóm nhỏ, rời đi trên những con đường riêng, rồi vài ngày tới một tuần sau mới gặp lại. Những hành trình này khiến tâm trí trở luôn tỉnh thức hơn với những tác động của xung quanh, và học cách tìm lấy con đường của bản thân. Chúng ta cũng dần đánh mất cả sự kính sợ, khiêm tốn, rồi sau đó là cơ hội học sự thích nghi với những gì nằm ngoài khả năng và sự chiếm hữu của loài người.

Buổi tối này chúng ta sẽ cùng suy nghĩ và chia sẻ với nhau về mối quan hệ của chúng ta đã có, đang có, và sẽ có với thiên nhiên.

Tham khảo: Blog Human-Nature (C) Peter H. Kahn.
