---
layout: post
title: Từ bất hòa nhận thức, đi tìm mục đích cuộc đời
date: 2022-05-28T09:28:55+07:00
tags: [cognitive, purpose]
---

Trong nhiều những hướng dẫn để tìm thấy mục đích cá nhân của cuộc đời, mình nhiều khi thấy nhụt chí, mệt mỏi khi thấy những thứ cụ thể và dễ hình dung mà nhiều những người khác có thể chọn được sẽ không phải là một thứ mình muốn đi theo. Thật là hay ho khi một buổi sáng như này mình lục lọi trên internet đọc về Elaboration Likelihood Model, mình có thể miêu tả rằng bản thân đang có một bất hòa nhận thức (cognitive dissonance) xuyên suốt tuổi trẻ. Nhiều khi đọc lại những kết quả của chiêm tinh như một cách tham khảo và soi chiếu lại, có một đoạn về mục đích cuộc đời, mà mình thường cho rằng “chắc mình vẫn có thể sinh sống như những người khác, viẹc đặt câu hỏi về ý nghĩa cuộc đời có thể thỉnh thoảng ghé qua, và biết đâu đấy những thứ hay ho kia lại trở thành ý nghĩa cuộc đời mình”. Nhìn lại thì cho dù mục tiêu mình có mơ hồ, kể cả là chuyện đi tìm ý nghĩa cuộc đời chính là ý nghĩa cuộc đời mình, mình vẫn cần có sự chuẩn bị cho nó. Chẳng hạn, làm việc trong tổ chức nhà nước về lâu dài, với những hệ thống khiến một người càng trở nên lệ thuộc hơn về tinh thần và thói quen, sẽ không giúp ích cho mình trong việc đi trên con đường tìm kiếm. Qua trải nghiệm sinh hoạt cá nhân, và từ các mối quan hệ thân mật, đồng nghiệp, gia đình, mình tìm thấy phần mà sẽ giúp hay không giúp mình trên con đường này. Và cũng có cả những thứ từ mình tự mình ngáng chân để bảo vệ bản ngã trên con đường này. 

May mắn là hiện tại mình có thời gian ở một nơi mới này để ngẫm ra. Khẳng định được, rằng mục đích của cuộc đời mình chính là tìm kiếm ý nghĩa cuộc đời, giờ đã làm mình sáng rõ hơn nhiều thứ.

Trước giờ mình tiếc nuối những năm tuoỏi trẻ mình phải vật lộn với sự bất hòa nhận thức, và thấy những năm đấy phung phí quá. Những initiation của tuổi trẻ mà mình đã đầu tư cho con đường này qua thời gian lang thang, nghĩ ngợi, viết và gặp gỡ một số người càng khiến cho những quyết định sau này nên trân trọng những năm tháng đó.