---
layout: post
title: "Quét sạch"
date: 2022-03-31T09:28:55+07:00
tags: [tlud]
---

Tôi giải nén một tệp tin được nhận. Tôi ngưng thở. Tôi dụi mắt nhìn lại xem mình vừa mới làm gì. Mọi thứ đó ghi đè lên những thứ tôi mất cả ngày làm. Cảm thấy một sự trống rỗng tràn ngập xung quanh.

Phải mất một lúc sau tôi mới nghĩ ra rằng cần tìm cách đi an ủi bản thân trong khi chưa muốn làm gì tiếp. Tôi lại chợt nhớ đến thực hành vẽ bức tranh cát của những nhà sư Tây Tạng. Họ mất cả tuần để vẽ một bức tranh mandala kỳ công trên một mặt sân lớn, và có một nghi thức để phá hủy nó trong vòng có một ngày.

Một trong số các tinh thần của Phật Giáo là sự vô thường. Làm nên những bức tranh mandala đó đã là một sự thiền tập. Chính sự thực hành phá hủy này thể hiện sự vô thường đó, thực hành sự không dính bám, kể cả với những người đã tạo tác nên nó.

Buổi thảo luận này chúng ta sẽ thảo luận về những cảm giác mất mát/quét sạch, cả về cảm nghĩ của bạn về thực hành vẽ mandala cát của những nhà sư Tây Tạng.