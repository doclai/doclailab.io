---
layout: post
title: Nhóm thảo luận tự do về sách
date: 2022-02-09T09:28:55+07:00
tags: [books, reading, discussions]
---

Nhóm hoạt động vào mỗi tối thứ tư hàng tuần, trừ tuần có ngày Tết Nguyên đán. Trước buổi hoạt động này, một thành viên của nhóm (đã đăng ký làm chủ trước đó 2 tuần) có một bài viết chia sẻ về một cuốn sách, mà thành viên này đã đọc, gửi tới mọi người trong nhóm. Bài viết này được các thành viên khác trong nhóm đọc như một cách để gợi hình dung về ý tưởng mà bản thân quan tâm. Từ đấy, tới buổi hoạt động, các thành viên được quyền trao đổi tự do về bất kỳ điều gì liên quan tới cuốn sách và bạn chủ. Hoạt động này tạo cơ hội cho các thành viên trong việc: thể hiện bản thân qua câu chữ, thể hiện bản thân qua lời nói, và khả năng lắng nghe.