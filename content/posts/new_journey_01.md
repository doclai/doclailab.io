---
layout: post
title: A new journey (1)
date: 2024-04-11T09:28:55+07:00
tags: [living, reflection]
---

_(other languages below)_

Mình bắt đầu một hành trình mới sau khi cơn bão Yagi đi qua miền Bắc.

Sáng sớm ngày sông Hồng dâng nước lên ngập các khu vực ven đê, mình chat với một người bạn, câu hỏi về cuốn sách đang đọc mà mình nhận được nhắc lại một phần ký ức của mình. Có thời gian mình đến công ty sớm và dành 30 phút đầu ngày chưa bận việc để đọc tạp chí và vài trang của cuốn sách mới đang đọc dở. Và giờ khi tạm biệt những người đồng nghiệp cũ, mình cũng đã gần như quên mất rằng mình từng có nhu cầu như vậy. Nhưng rồi thì, mình cũng nhận ra bản thân đọc rất nhiều những chia sẻ trải nghiệm mang tính cá nhân gần đời sống trên reddit hơn là đọc những tài liệu giả tưởng như trước kia. Bằng cách nào đó, hiện tại với mình, đời sống thực tế với tất cả những sự bí ẩn vốn có của nó quyến rũ hơn cả những tiểu thuyết giả tưởng.

I started a new journey after Yagi passed over the North of Vietnam.

Early morning the day when Red River water level raised and flooded whole area near the dike, I chat with one friend, the question about my currently reading book that I received reminded me old memory. There are times I got to my company early and spent the first 30 minutes of day without any burden to read magazines and some pages of an unfinished book. And now that I've said goodbye to my old colleagues, I almost forgot that I had such need. But then, I realized that I read a lot more personal experience sharing on Reddit than fictional material like before. Somehow, to myself, the real life with all of its intrinsic mysteries is more attractive than fictional material.
