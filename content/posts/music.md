---
layout: post
title: ". . ."
date: 2019-05-28T09:28:55+07:00
tags: [music]
---

Ngày mà tôi đã cực kì xuống sức sau buổi xem tarot hôm qua. Chọn một địa điểm cá nhân, trong một khu vườn, một ngày đủ hơi ẩm, nắng không quá gắt, gió thổi nhẹ.
Tôi lắng nghe [bài hát này](https://youtu.be/p7PGRtq33Mk), bài Believer được tinh chỉnh lại. Cùng nghe với tôi rồi hãy đọc tiếp những dòng dưới.
Đã lâu rồi, chị TT mang lại trải nghiệm nhạc với cả khứu giác và thị giác cho tôi. Cảm giác của một thứ đã lâu mới tìm lại. Mùi của căn phòng, mùi của đàn, rồi tưởng tượng ra mùi của nhạc công. Rồi ngồi đó mà như căn phòng đâu còn trần nhà nữa, cửa sổ đâu rồi, và thì một mình đối diện với nhạc công. Một màu trắng của âm thanh tràn vào mọi thứ.
Có một điều chị TT đã nhúng tôi, đẩy tôi vào một thứ tự do đầy tinh khiết của âm nhạc. Rồi nhận ra khi rời một tầm nhìn cao khi thưởng thức piano, tôi lại trở về thành một trong vô số những người ở Shawshank, lầm lũi hàng ngày bước đi, nhưng vẫn biết rằng trong một sáng trong trẻo đã nghe điều gì đó, "mặc dù không hiểu người nhạc công muốn truyền tải điều gì", nhưng cảm thấy như được mọc thêm đôi cánh, rũ bỏ quần áo, rồi lượn giữa những thứ cao nhất của thế giới này.
Khi nghe bài hát mà có kết nối, là tôi bắt đầu nghe thấy cả quá khứ của nhạc công, những ngày "tẻ nhạt" họ đã trải qua bên bài hát/bản nhạc một cách "không hoàn hảo", sự thôi thúc trong họ với một niềm tin vươn tới được cái đẹp. Âm nhạc là thứ nói lên một sự thật mà không hề xuất hiện ở những buổi nói chuyện trước của chúng tôi ở "MIT khoa học xã hội", rằng _kỷ luật mang lại sự tự do_. Có câu nói của một học giả rằng _"Đừng đòi Tự-Do, nếu ngươi tự thấy không đủ sức sống Tự-Do. Muốn Tự-Do, cần phải có nhiều đức tính mà những kẻ nô-lệ không bao giờ có đặng"_. Đó cũng chính là cái quá khứ của nhạc công mà tôi nhắc tới, họ dám dấn thân, phía sau cánh gà, ở nhà, những câu chuyện riêng, và những thứ đẹp đẽ họ mang lại, những gì chúng ta thấy là phía trên sân khấu, trải nghiệm tự do cho những người nghe.
Âm nhạc thực sự có khả năng để khôi phục được sinh lực, chữa lành. Tôi tin vào việc, tương tự như được đọc một cuốn sách đúng thời điểm, thì cũng có những bản nhạc xuất hiện đúng thời điểm, không để ru ngủ người nghe, mà đưa họ trở về với sự tò mò thật tự nhiên.

> All grown-ups were once children... but only few of them remember it
- Antoine de Saint-Exupéry, The Little Prince 

Âm nhạc cũng có tính cá nhân không khác gì văn học, nhưng nó đi một cách trực diện, bước qua mọi rào cản, và có khả năng dung hòa được những tâm hồn riêng biệt. Và dĩ nhiên, mỗi người có một cách thưởng thức âm nhạc riêng. Với tôi, rất nhiều bài hát nghe ổn thì tôi chỉ nghe được vài lần rồi sau đó phải xóa ngay khỏi máy. Chỉ cần nghe thêm một vài lần nữa là tôi có thể cảm thấy buồn nôn, và ra quyết định chỉ một trong hai cái: cái máy hoặc bài hát tồn tại. Tương tự với những lần tôi qua phòng tập, hay quán cafe, có thể những bài hát ở đó hấp dẫn lắm đấy, nhưng, đừng để tôi biết tên của chúng, không thì tôi sẽ chẳng thể qua đó được nữa đâu.
Cảm xúc và suy nghĩ của tôi thường đồng nhất, và hầu hết các thứ âm nhạc tôi trải qua vẫn cố gắng len lỏi, có khi là can thiệp một cách thô bạo vào dòng suy nghĩ và sau đó nhấn chìm cả tâm trí của tôi. Hoặc có thứ âm nhạc nào đó mà có chỗ cho những suy nghĩ của mỗi cá nhân được chảy tự nhiên hơn mà tôi chưa tìm thấy?
Rồi tôi nhận ra rằng tôi không đơn độc khi một nhạc sĩ có suy nghĩ:

> Người viết nhạc kỵ 2 điều:
Không được nghe nhạc của người khác vì sẽ viết giống họ.
Không được hát nhạc mình viết vì bài thứ hai viết sẽ giống bài thứ nhất.
- Trần Tiến

Với tôi âm nhạc là tiếng nói từ bên ngoài, nhưng những gì xuất hiện trong cái yên lặng thường khiến con người sợ hãi. Những lúc đi trên đường vắng, mọi người hay làm gì? Họ huýt sáo, hát nho nhỏ một bài hát nào đó rồi đi thật nhanh. Những con người sợ một mình. Hình như cách đối xử với âm nhạc cũng là cách đối xử với con người. Đi đâu cũng thấy những đôi tai được lấp đầy bởi headphone, có thể đó là một cách tránh ô nhiễm tiếng ồn, có thể là nghe podcast, trả lời điện thoại, ... Tiếng ồn. Sự vội vã của thế kỷ 21. 

Họ

Sợ

Sự im lặng.

<br>

Tôi có một một đứa bạn. Nhạc đỏ, nhạc cổ điển, nhạc trữ tình ... nó nghe cả. Hồi đi quân sự với nó, lên phòng, ngồi xuống bên cạnh. Hỏi lúc nó đeo tai nghe, rằng đó là bài gì vậy? Nó đưa tôi.

Không có gì cả.

Thật sự không có bài hát gì cả. Nó đưa tôi nghe sự im lặng. 
Và tôi nằm đó, thưởng thức.

