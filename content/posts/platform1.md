---
layout: post
title: "Biểu tượng trong Platform"
date: 2020-03-30T09:28:55+07:00
tags: [movies, thoughts, reading]
---

Có một vài thứ khi trò chuyện với người bạn của tôi về bộ phim mới nổi gần đây trên Netflix có tên Platform. Ở Việt Nam đã có bản vietsub, ai chưa xem thì nên xem trước, rồi hãy đọc bài viết này.
Chúng tôi trò chuyện về đoạn cuối phim: tầng số 333 và tại sao Goreng lại ở lại dưới đáy tầng 333 mà không theo cô bé lên tầng 0 trên chiếc bàn ăn?
Ở các bộ phim phương Tây, nơi người dân chủ yếu theo tôn giáo sử dụng kinh thánh, nên hẳn con số 333 có mang ý nghĩa tôn giáo từ đó. Chắc chắn chúng ta cần đến hiểu biết về kinh thánh để lý giải điều này. Tìm theo cụm từ "biblical meaning of number 333" chúng ta sẽ tới được một số lý giải về [ý nghĩa biểu tượng trong kinh thánh](https://www.biblestudy.org/bibleref/meaning-of-numbers-in-bible/333.html):

> Multiple threes (333) are used in Biblical discussions of the Promised Land. Although the phrase "land flowing with milk and honey" is referenced three times previously (Exodus 3:8, 17, 13:5), Exodus 33:3 is the first time God commands the Israelites to begin their journey to their inheritance. The phrase "milk and honey," used twenty times in the Old Testament, describes the exceptional fertility and beauty of the land God promised.

Còn về Goreng và quyết định ở lại ở căn hầm dưới tầng 333, tôi không nhớ rõ toàn bộ chi tiết nhưng cơ bản có vài ý:
- Goreng tới Platform tự nguyện, để nhận tấm bằng (degree) nào đó. Có Trong lúc mê sảng ở tầng 6, ý nghĩ đã chợt lóe lên trong Goreng, rằng cần làm gì đó để tác động tới Baharat. Và rủ Baharat đi cùng.
- Thông điệp của việc đi xuống dưới cùng (từ tầng 6) để gửi đi thông điệp cùng Baharat, một cách thật linh hoạt, chính Goreng là người chọn cái bánh ngọt không phải thông điệp, mà chính là cô bé đang đói chú dưới giường kia.
- Goreng không phải thông điệp, như lời ảnh ảo Trimagasi nói, và việc Goreng đứng trên đó để đi lên sẽ trực tiếp công nhận (credit) ông là người đã tạo khoảng khắc. 
Ý tưởng này khiến tôi lục lại một bài viết tôi từng đọc cách đây vài tháng, về [anonimity of literature and graffiti](https://aeon.co/essays/lessons-from-ancient-rome-on-the-power-of-anonymity):

> Imagine yourself stumbling across a big, raw line of political graffiti etched on a public wall, perhaps something like ‘Smash Capitalism Now!’ Part of the power and shock of the statement is that it hovers in the world of the unsigned, set free from the constraints and relativism of an individual subjectivity. If we knew the name of the single scribe calling upon us to smash capitalism this very minute, we wouldn’t take it as seriously. It wouldn’t siphon as much power, and we wouldn’t take it as a loaded act designed to make something happen for many through words. For the graffiti to strike the reader as a sign of revolution in the making depends on the fiction of its collective origins, or its possible unlimited appeal; that is, because it comes from anywhere and nowhere, the sentiment seems to swarm from everywhere. Whether you’re for or against this impending revolution, the graffiti carries a potent effect of universality; for a crowded second, you believe, and either you panic or you rejoice.

Có dịp tôi đọc lại.
Chúc ngủ ngon cả nhà.
