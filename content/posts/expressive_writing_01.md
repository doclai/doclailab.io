+++
title = "Về lối viết biểu đạt (about expressive writing)"
date = "2025-01-28"
+++

Ngoài việc mình vẫn có những bài viết ngắn trên blog, một cách đầy hơi thở và bụi bặm của đời, sự chật chội và ngột ngạt dẫn dắt sự chú ý của mình đặc biệt hơn tới chuyện viết biểu đạt. Với cuốn sách Writing to heal của James W. Pennebaker đồng hành cùng, mình tới với lối viết này cùng những đoạn văn ngắn khác, và chú ý hơn về tác dụng của lối viết này tới bản thân. Tác giả có kể về hai câu chuyện mà mình muốn để lại nguyên văn ở đây:


> We live our lives in a web of connections. Changing one aspect of our lives has the potential to affect many others. The ways you deal with your trauma may be exactly what your friends and family desire most. If you change your coping strategies, you might affect your closest relationships in ways you never imagined. Two stories illustrate this problem.

> Two stories

> Several years ago, I worked with a young woman whose husband had died suddenly almost a year earlier. Through her coworkers I learned that she was viewed as a pillar of strength; she had been happy, courageous, even inspirational in her optimism in the wake of her loss. She came to me because she felt she needed to write about her husband’s death, which she did. By the last day of writing, she was transformed. She was more relaxed, her blood pressure was lower, and she was deeply appreciative about the writing experience.

> Two months later, we met to discuss her life and the writing intervention. In the interim, she had quit her job, stopped seeing her friends at work, and had moved back to her hometown. She said that all of those changes were the result of the writing. Because of the writing experience, she had realized she was on a life path she no longer wanted. She was putting up a false, cheerful front for her friends and she discovered that the only people she could be truly honest with were her childhood friends.

> Was expressive writing good for her? Some would say that it undermined her career, her financial future, and her entire social network. She maintained it was a lifesaver.

> The second case is even more striking. A woman in her early forties with three children told me of her need to write in order to deal with a series of terrible childhood events that haunted her. After several days of writing, she reported that she felt free for the first time in her life. Over the next few months, however, she left her husband and, with her children, she moved into low-income housing where she barely eked out a living. She went through a period of deep, almost suicidal depression from which she gradually escaped.
In a recent interview with her, she maintained that the writing was the direct cause of her divorce, depression, and poverty. But like the woman whose husband died, she is also grateful for the writing she did and the insights she gained. She said that deep down she knew she had to address some of the basic issues from her past that had been causing her profound unhappiness and conflict. The cost was higher than she had anticipated, but in retrospect, she thought it had been worth it.

> These two cases suggest that writing can be a significant threat. By reducing your inner conflicts, you may affect the course of your life and the lives of others in unintended ways. Statistically, we have found that most people report that the life changes following emotional writing are beneficial. Indeed, even these two individuals whose lives were so deeply changed are appreciative of the power of this kind of writing.

> Nothing is as simple as it seems.


Tôi từng dành thời gian hàng năm trời trước đây, ngồi ở một góc quán cafe khi mình gần như là khách hàng duy nhất, viết theo lối này. Nhưng rồi thời điểm đó mình không biết tới chuyện bước khỏi lối viết này và làm khác đi được, nên, mọi thứ thời điểm đó vẫn luẩn quẩn, như tác giả có chia sẻ trong cuốn sách, khi không thấy tiến triển, người viết cũng cần hình dung tới điểm dừng trong thực hành này. Việc phân tích quá nhiều có thể gây hại, lan man tới những thứ vốn không cần phải dành sự tập trung.

Đây là thực hành cho ta hình dung được một cách ta có thể trải rộng nỗi niềm nhận được, như cách Rilke kể trong bức thư ông viết, ra với mênh mông sâu thẳm giữa những hòn đảo biệt lập của đời sống cá nhân mỗi người, để "thử đi đôi giày của người khác" trong chính thực hành này. Ở đó, dù sớm hay muộn, có một thời điểm quan trọng, người viết bắt đầu có thể hướng sự quan sát tới trải nghiệm của những người khác trong ký ức tổn thương, và có thể kể về câu chuyện một cách mạch lạc hơn.

```
No man is an island,
Entire of itself;
Every man is a piece of the continent,
A part of the main.
If a clod be washed away by the sea,
Europe is the less,
As well as if a promontory were:
As well as if a manor of thy friend's
Or of thine own were.
Any man's death diminishes me,
Because I am involved in mankind.
And therefore never send to know for whom the bell tolls;
It tolls for thee.

John Donne
```
