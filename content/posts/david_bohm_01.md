---
layout: post
title: Sự không mạch lạc (1)
date: 2020-06-10T09:28:55+07:00
tags: [learning, books]
---

Trong cuốn sách _Tư duy như một hệ thống_, tác giả David Bohm đưa ra một khái niệm thú vị về tư duy, là sự-không-mạch-lạc. Bài viết này sẽ giữ tiêu đề một cách giản dị theo khái niệm của ông đưa ra từ chương đầu cuốn sách. Vậy thì sự-không-mạch-lạc là gì?

Ở trang 89, D. Bohm có trả lời cho câu hỏi _Vậy tiêu chuẩn của sự mạch lạc là gì?_:

> [...] Không có thứ tiêu chuẩn nào duy nhất cho sự mạch lạc cả, nhưng bạn lại phải nhạy cảm đối với sự không mạch lạc. Và như ta đã nói, phép thử đối với sự không mạch lạc chỉ có được khi bạn xem những kết quả bạn đạt đến có đúng theo ý muốn của bạn hay không mà thôi.

Ở trang 98, D. Bohm có nói về mặt nhận thức để học hỏi sự-không-mạch-lạc: 

> [...] Đáng buồn là trong nhà trường, người ta lại thường xuyên dạy rằng khi bạn đã hiểu cái gì một cách trừu tượng thì tức là bạn đã hiểu nó một cách hoàn chỉnh. Nhưng ngay cả ở điểm này, khi cần mang những gì bạn học được ra thực hành thì bạn thường không làm được.

Đây là tình huống mà ngôn ngữ giới trẻ gọi là "chém gió". Và cũng là hiện tượng xuất hiện ở nhiều những nhóm nhỏ mang tính học thuật, khi mọi người thường đánh giá cao việc có thể trình bày một cách trừu tượng thay vì đưa ra các giải pháp thực tế. Câu này làm tôi liên hệ tới một câu nói khác trong cuốn _Self-univesity_, trích dẫn về _triết lý và cá nhân_, James Feibleman viết:

> [...] A philosophy in this sense is not something that an individual understands but something by means of which he understands everything else. It lies as far down in his unconscious mind as do the presuppisitions of which he remains unaware. When a man holds a philosophy consciously it usually means that he does not hold it very deeply. But when he does not know that he holds it and assumes that when he acts he does so from instinct and not from philosophy, then it is possible to say, with Parmenides, that he holds a philosophy in the inalienable sense that makes it more accurate to say that it holds him. This, then, is the way I mean the philosophies to be understood, namely, as descriptions of the unconscious fundamental beliefs of the individual at various periods in his career.

Sự không mạch lạc xuất hiện trong lúc đọc, những cuốn sách mang phong cách diễn giải đưa đẩy, kiểu như cuốn của D. Bohm, không thể đọc theo kiểu tiểu thuyết, đọc liền mạch được. Ngoài việc mất tập trung bởi điện thoại, thì việc chỉ đơn giản cố đưa mắt trên những trang sách thì không thể hiểu được tác giả đang trình bày cái gì nữa. Có cái gì đó như việc càng cố thì càng không đạt được mục tiêu. Hoặc chúng ta cần cố theo một cách khác?
