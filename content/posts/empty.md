---
layout: post
title: Toàn vẹn
date: 2021-12-08T09:28:55+07:00
tags: []
---

Nhận ra cái sự "vốn có" và "toàn vẹn" khi tìm ra cách hỗ trợ về âm thanh, những cái họ cần, những cái mình cần.

Sau đó là tương tác với con người, và sau cùng là đưa tiễn những thứ vốn tự do về với đất trời. 

Những sự thật thường trộn lẫn trong hỉ nộ ái ố hàng ngày. Lần này nhìn thấy cái "trống không" sau khi thấy cái "trống rỗng" ban đầu.

