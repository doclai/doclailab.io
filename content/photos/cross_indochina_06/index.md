+++
image = "cross_indochina_06.jpg"
date = "2025-01-09"
title = "Đạp xe xuyên Đông Dương - Phần 6"
type = "gallery"
+++


## Ngày 17

Hạ lều cách nhà dân đối diện 40m mà đêm nhất cử nhất động trong lều đều bị con chó bên kia đường phản ứng (nó quan tâm mình ghê cơ chứ). Xong nằm đọc về thính giác của chó. Chỗ hạ trại mà làm quả sân bóng cho trẻ con là tuyệt vời.

Kết thúc ngày 17 ở Non Phay.

## Ngày 18

Vào khu này toàn nhà dân sát nhau, cánh đồng trống cũng không tiện hạ trại, dễ bay lều (theo nghĩa đen). Thấy có bác giai đang làm ở gần cái chòi, mình mò xuống hỏi xin nghỉ tại chòi, tiện thể bơm nước (giếng bơm tay) hộ bác luôn.

Mấy ngày nay ở Lào, đường lên dốc không bị gấp, nên mình dễ bị ảo giác và nghĩ là đường bằng mà hay có vấn đề gì sức khỏe, sao mình đi chậm thế, cho tới khi dừng xe nghỉ quay lại thì mới hiểu mình vừa qua một con dốc. Nay ngoài dốc thì còn thêm cả ngày gió siêu mạnh tới mức mình phải giữ thăng bằng xe và dùng aero bar (dụng cụ tì cùi chỏ lên tư thế plank, giảm sức cản không khí khi đạp xe) liên tục, nên không đi được xa như bình thường.

Nay và hôm qua có vài chuyện hay, nhưng lười chụp, lười viết.

Kết thúc ngày 18 ở Nondisay.

Ảnh chụp góc nhìn từ cái chòi bình minh hôm 19.
