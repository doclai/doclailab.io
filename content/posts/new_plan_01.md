---
title: "Thay đổi thói quen"
date: 2023-04-13T09:39:19+07:00
tags: ["habits"]
toc: "false"
draft: false
---


Liẹu bằng cách nào có thể thay đổi thói quen tiêu khiển của mình bằng những hoạt động khác.

Có thể sẽ là một thứ mang tính chất bụi bặm hơn khi kết hợp được cả low level pleasure và high level pleasure. Vẫn lướt Facebook videos như một cách tiêu khiển trong khi mình vẫn còn nhiều những kế hoạch khác đang chạy song song trong tuần. Thấy rệu rã nhưng lại vẫn tốn gần tiếng đồng hồ hàng ngày để lướt Facebook. Đọc lại các chia sẻ của Aaron Swartz.
