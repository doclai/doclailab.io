---
layout: post
title: Leo núi như một thói quen (2)
date: 2024-01-24T10:13:55+07:00
tags: [hiking, habits]
---

Kể chuyện tiếp về leo núi.

Ngoài câu chuyện về rủ rê chúng bạn night hiking gần đây thì còn là câu chuyện về quyết định mới thay đổi lối sinh hoạt: từ chuyện có thể thực hiện những chuyến cafe thường xuyên (nếu thời tiết cho phép) trên đỉnh núi với bộ đồ nghề nho nhỏ, tới câu chuyện 2MAD (giảm số lượng bữa ăn hàng ngày của tôi xuống thành 2 bữa).

Xuống núi đầu giờ chiều, mà chỉ dùng 150ml ngũ cốc nóng, không dùng bữa trưa, tôi thấy cơ thể thật sự nhẹ nhõm. Trên con đường xuống núi, tôi bắt gặp rất nhiều thân gỗ nằm la liệt nhắc nhở nhẹ cho một kế hoạch về địa điểm cho lớp bushcraft 101 đã âm ỉ từ lâu. Những bước chân xuống núi, qua tới những khung cảnh quen thuộc trong lúc một tôi, làm tôi nhớ lại người tình cũ đã từng gạt đi ý tưởng trước kia của tôi, ý tưởng cho việc trở đi trở lại Hàm Lợn một cách thường xuyên như việc đi cafe vậy.

Xuống tới đồng bằng tôi vẫn còn cảm giác nhẹ nhàng, khỏe khoắn, cảm giác kết nối một cách kính cẩn và sâu sắc với mẹ thiên nhiên với tất cả những bài học đã, đang, và sẽ có. 

