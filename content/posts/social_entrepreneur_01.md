---
layout: post
title: social entrepreneur (1)
date: 2021-05-16T09:28:55+07:00
tags: [books, reading, social entrepreneur]
---

Thuật ngữ khởi nghiệp xã hội là một thuật ngữ không quá mới, nhưng không nhiều người biết, cùng với ý tưởng kinh doanh dựa trên lợi ích cho xã hội, trong chiều dài lịch sử có không ít những người đã làm những điều như vậy. 

Trong cuốn sách, Getting Beyond Better, tác giả đưa chúng ta tới với những sự kiện lịch sử, rồi xây dựng khung hiểu hiểu biết về _social entrepreneaur_ (SE), với góc nhìn của một _reflective practioners_, tức nghĩ trong khi hành động, phản tỉnh để củng cố lý thuyết và thực hành.

Từ một nghiên cứu được công bố bởi Stanford vào năm 2007 với ý muốn làm rõ khái niệm SE, có hai yếu tố quan trọng để phân biệt và xác định. Đầu tiên là sự trực tiếp, rằng cá nhân hay tổ chức đó có trực tiếp hay gián tiếp gây ảnh hưởng lên xã hội. Thứ hai là xu hướng ảnh hưởng, khiến cho cộng đồng đó giữ ổn định mô hình hiện tại, hay chuyển hóa hoàn toàn. Như vậy với hai tiêu chí này, chúng ta có thể hình dung như sau:

|  	| giữ ổn định 	| chuyển hóa 	|
|:-:	|:-:	|:-:	|
| trực tiếp 	| social service provider (SSP) 	| social entrepreneur (SE) 	|
| gián tiếp 	| anything else 	| social activism (SA) 	|

Lưu ý là SE không hề coi là "ưu việt" hay như nào đó so với các SSP hay SA. Một SE cần có những tính chất sau:

- Sự xác định được __trạng thái ổn định không công bằng vốn có__ dẫn tới bị loại trừ, bị lề hóa hay sự chịu đựng của một lớp con người (_The identification of a stable but __inherently unjust equilibrium__ that causes the exclusion, marginalization, or suffering of a segment of humanity - a group that lacks the financial means or political clout to effect transformational change on its own_).
- Quá trình phát triển, thử nghiệm, tinh giản, mở rộng __giải pháp chuyển hóa__ (_The development, testing, refining, and scaling of an __equilibrium-shifting solution__, deploying a social value proposition that has the potential to challenge the stable state_).
- Quá trình __hoàn thiện__ và __duy trì__ trạng thái ổn định mới (_The __forging of a new stable equilibrium__ that unleashes new value for society, releases trapped potential, or alleviates suffering. In this new state, an ecosystem is created around the new equilibrium that __sustains and grows__ it, extending the benefit across society_).

Các SE luôn cần nhớ tới chữ __chuyển hóa (equilibrium change)__. 

Đây là một thứ khó. Trên thực tế, có nhiều hơn rất nhiều những SSP và SA thành công, trong khi với SE rất ít. Hơn nữa, chúng cần thời gian. Vậy nên SE cần có các tiêu chí: hiểu về cộng đồng, kiến tạo viễn cảnh mới, xây dựng mô hình cho thay đổi và mở rộng giải pháp.

Chúng ta có thể kể tới một SE từ thời xa lắc xa lơ, đó là trường hợp của Gutenberg. Cho dù ông không phải là người sáng chế ra bản khắc in (điều mà người Trung Hoa họ đã làm trước ông lâu rồi), điều ông làm là dựng lên những chiếc máy in công nghiệp đầu tiên, đưa sách xuất bản hàng loạt thay vì chép tay bởi các thầy tu, phá bỏ sự độc quyền tri thức của nhà thờ và tầng lớp tinh hoa. Tuy vậy, SE của ông không phải hoàn hảo, khi vài trăm năm sau phát minh của ông, sách hầu hết vẫn dừng lại ở tầng lớp quý tộc.

Hai thái cực của SE có thể kể tới khi phân tích đó là: ảnh hưởng từ phía chính quyền (government-led) và ảnh hưởng từ thị trường (business-led). 

Ảnh hưởng từ phía chính quyền có thể kể tới sự kiện vua John năm 1215. Ông trước đó đã lưu hành một điều luật rằng mọi tài sản ở Anh Quốc ông đều có thể trưng dụng được (dẫn tới việc không ai dám đầu tư), ai có điều gì trong hoàng gia mà không ưng ý đều bị thất sủng và gia tài của họ đều được xem xét. Điều luật này khiến cho những lãnh chúa và chủ đất không thoải mái trong kinh doanh, dẫn tới đình trệ kinh tế. Mô hình này càng ngày càng ổn định hơn cho tới khi ông bị gây áp lực từ bỏ chúng. Một ví dụ khác có thể kể tới là Martin Lurthur King ở Hoa Kỳ.

Phía thị trường không thể không kể tới, khi nhìn thấy sau cách mạng công nghiệp thế kỷ 19, sự giàu có của chúng ta tạo ra trong vài năm bằng nhiều thế kỷ trước cộng lại. Có thể nhìn vào trường hợp Apple tạo sức ảnh hưởng cho tới hiện tại khi mỗi người đều mang một cái máy tính cùng mình để nghe gọi hàng ngày. Apple, cùng với Steve Jobs, Steve Wozniak và Ronald Wayne, không phải là bên tạo ra máy tính đầu tiên, nhưng là bên đầu tiên đưa máy tính ra một cách thương mại rộng rãi (tương tự như Edison, không phải là người đầu tiên phát minh bóng đèn, mà đưa được bóng đèn ra ở tầm đại chúng). 

Những trường hợp trên không phải là những SE, nhưng họ làm thế giới tốt hơn mà không chỉ tập trung vào thay đổi hay ích lợi xã hội. Và trên thực tế, những SE ở vị trí một trong hai thái cực trên rất hiếm, và thường kết hợp lợi thế hai phía phù hợp từng hoàn cảnh. Chúng ta có thể quan sát tới hai trường hợp: Grameen Bank với Yunus từ Bangladesh và UIDAI Project Aadhaar với Nilekani từ Ấn Độ.

Yunus sau khi quan sát được sự kiện trong xã hội: những người phụ nữ cộng đồng phía Tây Nam Bangladesh gặp vấn đề với nguồn cung mây không có sẵn để làm ghế. Họ phải dựa vào bên trung gian cung cấp, ngược lại bên này sẽ quyết định giá sản phẩm và mua lại từ họ với cái giá rất rẻ. Yunus đến với cộng đồng, gặp một nhóm gồm 42 người phụ nữ như vậy, hỏi han, và biết rằng tổng số tiền để họ thoát được khỏi bên trung gian, và trở nên năng suất hơn, là vào khoảng $27. Yunus đã đưa số tiền này cho họ, không nghĩ tới việc họ có khả năng trả lại, nhưng không hề nghĩ đó là tiền từ thiện. Ông có quan điểm rằng từ thiện sẽ khiến chính bản thân rũ bỏ nhanh trách nhiệm với vấn đề xã hội, chú ý tới cuộc sống của chúng ta mà bỏ quên cuộc sống của những người nghèo. Vậy mà thời gian sau số tiền đó đã được họ trả đủ từng đồng. Trong khi đó, các ngân hàng truyền thống khi cho vay đều cần có điều kiện bảo đảm, Yunus đưa ra ý tưởng, sẽ cho vay các cá nhân thông qua thực thể là các nhóm tin cậy. Mô hình vi tín dụng này trở thành nền tảng cho Grameen Bank sau đó được nhân rộng rãi, thu hồi 96% các khoản vay và thu về 250 triệu đô lợi nhuận vào 2010. Ý tưởng này cùng Yunus đã được nhận giải Nobel Hòa Bình 2006.

UIDAI Project Aadhaar nhắm tới vấn đề giải quyết an toàn trong việc định danh. Cách đây hơn thập kỷ, hơn 400 triệu dân ở Ấn Độ không có giấy tờ định danh cá nhân chính thức, thứ tài liệu nền tảng cho quyền công dân tại một quốc gia, từ bằng lái, bầu cử tới các hoạt động pháp lý khác. Sau một năm từ khi xuất bản cuốn sách _Imagining India_, phân tích vấn đề và đề xuất giải quyết về Ấn Độ, của Nilekani, thủ tướng Ấn Độ đã yêu cầu ông tham gia vào một dự án hiện tại của UIDAI, Project Aadhaar (tiếng Hindi cho _nền tảng_). Ông nhìn thấy giải quyết vấn đề này, vừa lấy lại danh tính những người thuộc tầng lớp thấp, đưa họ gần hơn với các tiện ích công cũng như các khoản đầu tư của chính phủ trở nên hiệu quả hơn. Trong tình huống đa số những người này không hề có giấy tờ tùy thân, ông nhận thấy việc định danh sinh học: qua đồng tử của hai mắt và qua vân tay mười ngón, có thể lập được một chữ ký điện tử Aadhaar gồm 12 chữ số duy nhất trong hàng tỷ người, tiện hơn và cũng đi trước cả các nước phương Tây (cuối cùng cách thức này có độ chính xác tới 99.9%). Giải pháp đã có, cần là cách thức áp dụng nhanh, hiệu quả và nhân rộng được. Nilekani lập một hệ sinh thái các đối tác: các tổ chức và địa điểm có thể dùng phần mềm từ UIDAI để đăng ký: từ ngân hàng, bưu điện, ... Đỉnh điểm có tới 30.000 điểm đăng ký như vậy dọc Ấn Độ, với hơn 1.000.000 đăng ký một ngày. Đến năm 2014, khi Nilekani xuống chức giám đốc, đã có 720.000.000 người được đăng ký định danh cá nhân. Bên cạnh những lo ngại về quyền riêng tư thì không thể không phủ nhận khả năng ảnh hưởng từ dự án này: 720.000.000 cá nhân, liên kết với 6.000.000 tài khoản ngân hàng và hơn 100 tổ chức sử dụng phương thức định danh này.

