---
layout: post
title: Thủ tục hàng ngày
date: 2021-09-03T09:28:55+07:00
tags: [health, sleep, routine]
---

Sự xáo trộn về routine (thủ tục hàng ngày). 

Cảm nhận sự mất kết nối khi các giác quan bị nhồi quá nhiều thông tin từ sớm bên ngoài. Sự mất cảm giác về các suy nghĩ bản thân. Một hình dung như những mầm cây bị dẫm không thương tiếc trên nền đất của tụi nhỏ mất trí vậy. Tôi cũng đã nghĩ tới thời gian yên tĩnh trước khi mọi người thức giấc là lúc hay ho để dành thời gian cho bản thân, trong khi đó thì lại cần ngủ đủ để bản thân ít có tâm thế phản ứng (reactive).

Điều đó là một nghịch lý làm sao có thể sinh hoạt trong một môi trường như này. 

Điều này đáng ra dễ dàng hơn trong dịp giãn cách nhờ ít xe cộ đi lại trong nội thành hơn hẳn. Cái không khí thức dậy viết lách vào buổi trưa làm tôi như được sống lại địa điểm đó hơn chục năm về trước.

Cũng tiện là trên điện thoại có hẹn chế độ tắt bật nguồn điện thoại theo giờ buộc tôi ngắt kết nối, tiện cho việc vào giấc ngủ. Tôi chợt nghĩ có khi tôi hình thành khái niệm này khi đọc về revenge sleep procasination. Nhiều chỗ họ nói rằng khả năng kiểm soát bản thân càng về cuối ngày càng giảm, hay việc tiếp xúc với đồ điện tử công nghệ gia tăng. Tôi lại nghĩ hành vi này miêu tả về một thứ nhu cầu nào đó mà mình cần nhìn thấy chứ không đơn thuần chỉ là tắt máy. Có thể nó gợi tới việc lồng khoảng thời gian quan tâm bản thân trong ngày, thứ thật quan trọng như Zarathustra được nghe về đức hạnh với sự tỉnh thức cùng giấc ngủ.
