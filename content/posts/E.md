---
layout: post
title: Tránh đông
date: 2017-08-19T09:28:55+07:00
tags: [stories]
---

Tránh đông
Thỉnh thoảng tôi lại nghe được câu chuyện nào đó về các loài vật.

Khi con voi cảm nhận được cái chết đang cận kề, nó sẽ một mình bỏ đến một nơi thật xa, đến nơi không ai tìm được nó và chết đơn côi ở đó. Vịt con khi mới nở nếu thấy thứ gì trong suốt bảy đến tám tiếng sẽ bám theo thứ đó cho đến lúc chết. Đây hẳn là hiện tượng khắc ghi tình cảm vĩnh viễn với thứ đầu tiên nhìn thấy trong đời ở loài vịt. Còn chứng đãng trí của loài sóc thì nghiêm trọng đến mức khó tin. Chúng chăm chỉ gom hạt sồi chất đống một chỗ rồi quên bẵng luôn. Đôi khi nếu thấy một ụ hạt sồi dưới gốc cây nào trong núi thì hẳn đó chính là của đám sóc để dành định ăn mà quên mất. Nhưng đãng trí bậc nhất phải kể đến loài cá. Chúng thừa biết miếng mồi câu xuất hiện trước mắt là mối nguy hiểm không nên ăn, thế nhưng vừa quay đi đã quên béng mất nên cứ thế đớp lấy. Khó tin hơn cả là chuyện về bọn gà, một khi đã ngủ thì ngủ say như chết, đến mức có bị chuột moi ruột ăn hết cũng không hay biết gì.

Là thật hay đùa ư? Tôi không thể trả lời được. Tôi cũng chỉ nghe lại từ người khác thôi, chưa tận mắt chứng kiến bao giờ.

Tôi còn nghe chuyện về loài gấu nữa. Trước khi sang đông, những con gấu ăn no đến nứt bụng rồi leo lên cây. Từ trên cây chúng nhảy xuống đất và lăn tròn. Làm thế để nếu bị đau, chúng sẽ tiếp tục ăn. Cứ thế, chúng ăn cho đến khi lăn mà không thấy đau nữa. Để chuẩn bị tránh đông.

Nhìn một cô mèo dẫn ba nhóc mèo con bước chậm rãi ra sân trên nền tuyết tháng Một còn chưa tan, tôi chợt nhớ đến câu chuyện về loài gấu ai đó từng kể mình nghe. Lũ mèo có vẻ đói bụng lắm. Chúng vồ lấy rồi liếm thử cái túi rác bị gió thổi bay tới, đám mèo con thậm chí còn cúi xuống liếm cả những bông tuyết trắng, có lẽ là do khát nước. Giá mèo cũng ngủ đông được như gấu thì hay biết mấy. Trước khi mùa đông lạnh lẽo về khiến mọi thứ đông cứng lại, khi còn có thể ăn được thì cứ ăn để tích mỡ trong người, rồi chỉ việc cuộn tròn ngủ trong hang sâu cho đến hết mùa đông, Hình ảnh lũ mèo con bụng đói meo rón rén theo sau chân mẹ khiến tôi chợt nhớ đến cái hôm đi qua trạm thú y khu phố đã ghé vào mua một bao thức ăn hạt cho mèo mang về, bao lâu rồi ấy nhỉ?

Tôi mở chạn bát lấy ra ba cái đĩa không dùng đến. Tôi chia thức ăn khô vào hai cái và đổ nước vào cái còn lại, sau đó để cả ba ở nơi lũ mèo hay đi qua. Ngày đầu tiên, thức ăn vẫn còn nguyên. Hay là chúng không ăn hạt khô? Vừa nghĩ tôi vừa đi thay nước mới. Tôi nhớ đã đọc trong cuốn sách nào đó rằng, khó khăn lớn nhất mà mèo hoang gặp phải vào mùa đông lạnh cóng, hơn cả thiếu thức ăn, là không có nước sạch để uống. Vì thế, tôi vẫn thường xuyên thay nước sạch cho chúng dù thức ăn vẫn không vơi đi. Nhưng rồi một ngày nọ ,chỗ thức ăn chỉ còn lại một nửa. Tôi những muốn thốt lên, “Cuối cùng chúng cũng phát hiện ra thức ăn rồi.” Ngày hôm sau, đĩa thức ăn đã được chén hết sạch. “Ăn cũng khỏe đấy chứ,” tôi nghĩ rồi đổ thêm nhiều hạt khô hơn vào đĩa. Ngày hôm sau nhiều hơn một chút. Hôm sau nữa lại nhiều hơn chút nữa.

Cứ như thế, tôi đã trải qua mùa đông bằng cách cho mèo ăn mỗi ngày. Dù chỉ là mỗi ngày một lần, khi thức ăn vơi đi lại đổ đầy thêm, nhưng nói thế nào nhỉ, tôi đã rất hài lòng với dáng vẻ của mình khi đó, đổ thức ăn hạt cho mèo vào cái đĩa trống trơn ấy của mình. Cái dáng dốc công dốc sức vì người khác ấy của tôi thật không ngờ lại làm chính tôi yên lòng. Dù chỉ là đổ thức ăn hạt vào đĩa cho mèo mà thôi.

Thế nhưng, mấy ngày trước, thay vì mèo thì tôi lại thấy ác là trong sân. “A, có cả ác là nữa kìa,” mới đầu tôi đã vui mừng chào đón chúng. Thấy đám ác là mổ ăn hạt trong đĩa, tôi vừa nghiêm giọng mắng, “Đấy là thức ăn của mèo mà, có phải dành cho chúng mày đâu,” vừa rải thêm một nắm gạo kế bên đĩa hạt. Từ hôm đó, công việc mỗi ngày của tôi ngoài đổ thức ăn hạt cho mèo còn thêm cả rải gạo cho chim. Có điều, từ khi ác là xuất hiện lại không thấy lũ mèo đâu nữa, nhưng đồ ăn trong đĩa thì vẫn vơi dần. Có lẽ chúng đến ăn khi tôi không thấy chăng, nghĩ vậy nên tôi vẫn tiếp tục đổ hạt đầy đĩa. Một ngày nọ, tôi có việc phải về sớm, trước khi mở cổng tôi khẽ nhìn vào sân. Ôi trời! Bọn ác là đang bu đen kịt quanh đĩa đựng thức ăn hạt tôi đổ ra cho lũ mèo, ra sức mổ lấy mổ để. Không chỉ một, hai con mà là cả một đàn. Chúng cụng đầu, xô đẩy chen chúc nhau mổ thức ăn, và thật lòng mà nói thì cảnh này có hơi kinh dị. Chim ăn hạt dành cho mèo cũng thật là kỳ dị. Khi tôi mở cổng bước vào, lũ chim đồng loạt bay đi tán loạn, chỉ còn rải rác tầm mười con. Khi ấy tôi mới vỡ lẽ, thì ra đám ác là kia chính là thủ phạm đã giành hết thức ăn của mèo.

Sao lũ mèo lại không bảo về thức ăn của mình nhỉ?

Nếu không tận mắt chứng kiến cảnh tượng ban nãy thì có lẽ tôi cũng sẽ hỏi như thế. Nhưng quả là nếu bị cả đàn chim bu vào mổ thì mấy chú mèo chỉ còn cách dâng đĩa thức ăn cho chúng mà thôi. Thật kỳ quái. Lẽ ra chim phải mổ thóc, còn hạt khô để cho mèo ăn chứ, cứ chung sống hòa bình như vậy là được cơ mà.

Xét lại thì sự thật đúng là ác là đã cướp thức ăn của mèo. Không biết chúng làm thế nào, nhưng rõ ràng từ khi đàn ác là xuất hiện không còn thấy mấy chú mèo bén mảng quanh sân nữa. Ôi… không biết các bé mèo con đã uống nước ở đâu, nhưng dù sao thì ác là cũng cần phải sống mà… nghĩ thế nên tôi vẫn tiếp tục đổ thức ăn của mèo ra cho chim ác là ăn.

Rồi lúc chiều tà hôm nay, có một chuyện đã làm tôi giật mình xuýt ngã ngửa.

Đang ngồi trong phòng thì tôi nghe có tiếng ồn ào. Tiếng gì thế nhỉ, tôi bước về phía phát ra tiếng ồn ào. Tiếng gì thế nhỉ, tôi bước về phía phát ra tiếng ồn thì chẳng phải đâu khác, chính là chỗ đặt mấy cái đĩa đựng hạt thức ăn cho mèo. Ôi trời! Chuyện gì thế này? Bầy chim ác là đang lao vào mổ nhau kịch liệt. Tôi không hiểu tiếng chim nên không biết chúng cãi nhau vì cái gì, nhưng nhìn kỹ thì thấy rõ chúng đang chia làm hai phe tả xung hữu đột. Chúng bay bổ nhào vào nhau rồi bỏ chạy rồi mổ nhau, loạn hết cả lên… Thật không ngờ ác là lại như thế. Tất cả khởi sự bằng việc một đàn ác là giành lất thức ăn tôi để cho đám mèo hoang, rồi chẳng biết thế nào một đàn ác là từ khu khác cũng bay đến để tranh thức ăn với đàn ác là kia, có lẽ là vậy chăng?

Quan sát trận ẩu đả một hồi, tôi cảm thấy sợ nên đóng của quay vào nhà. Nhiều người đi ngang qua cũng tò mò dừng chân nghển cổ ngó vào, nhưng rồi có vẻ bị âm thanh nháo nhào ầm ĩ của lũ chim dọa chết khiếp, họ tròn mắt kinh ngạc rảo bước bỏ đi.

Mãi tới khi một đàn ác là bay đi chỗ khác thì sự náo loạn mới dịu xuống. Dõi theo cuộc chiến ấy qua khung cửa kính, trái tim tôi chùng hẳn xuống.

Trăng ơi! Trăng có biết rốt cuộc giữa bọn chúng đã xảy ra chuyện gì không?

Ngày hôm sau, tôi lặng lẽ mang ba chiếc đĩa cất lại vào nhà. Để cho lũ chim trở về với thế giới vốn dĩ của chúng. Để chúng trải qua mùa đông này bằng chính bản năng sinh tồn vốn có. Tôi đã can thiệp vào thế giới của lũ chim ác là, và để chấm dứt cuộc chiến này tôi không còn cách nào khác ngoài việc cất đi những đĩa thức ăn đã bày ra.

Thế nhưng mà trăng ơi, sao lại thấy lạc lõng thế này?

Chuyện kể trăng nghe – Shin Kyung Sook

*19/08/2017*
