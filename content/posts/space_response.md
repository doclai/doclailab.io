---
layout: post
title: Khoảng không phản hồi
date: 2021-09-02T09:28:55+07:00
tags: [learning, books, health]
---

Với những người Hoa, khủng hoảng là 危机, trong đó thì chữ 危 là hiểm nguy còn 机 nghĩa là cơ hội. Trích một miêu tả của nhà tâm thần học Victor Frankl với tinh thần tương tự, tôi dịch lại một cách gợi hình ở đây:

> Có khoảng không ở giữa sự kích thích và phản hồi. Ở đó cư ngụ sự tự do và nội lực quyết định sự phản hồi. Trong sự phản hồi chứa đựng sự lớn lên và hạnh phúc của chúng ta.

Nói một cách giản dị, khi nào thì bạn "buồn"? Nhớ lại khi bạn xem một bộ phim, kịch bản đến đoạn éo le cho nhân vật, hay khi bạn trải qua một sự mất mát, bạn buồn đúng không? Vậy mà khi kết thúc bộ phim, hoặc một thời gian sau sự mất mát, bạn cảm thấy sự sinh động trong tâm, cảm nhận gắn kết với nhân sinh, cuộc đời. Việc đi qua đầy đủ nỗi buồn là một điều lành mạnh cần thực hiện để trải nghiệm được dòng chảy cuộc sống, như một trong bốn sự thật cao quý của Phật Giáo:

> Life is suffering.
> Cuộc đời là đau khổ.

Không phải ai cũng có thói quen hoặc một sự hình dung như vậy. Không ít người họ có trải nghiệm buồn, nhưng lại học cách từ chối sự cảm nhận hoặc sự bộc lộ, để đi qua một cách đầy đủ. Họ không trực tiếp được chạm vào, cởi mở để chấp nhận sự thật và nhìn thấy những sự trợ giúp xung quanh. Những gì họ có được là một góc cuộc đời chưa được tô màu, hoặc tô màu nham nhở. Bức panorama cuộc đời mỗi người chúng ta xây dựng có điều đặc biệt, màu của chúng phai theo thời gian khi chúng ta bớt dần những trải nghiệm tại khu vực đó, mà cũng gần gũi, như việc thỉnh thoảng trên phố ta đi lại thấy vạch kẻ đường được tô mới vậy. Họ có thể hình dung chỗ này chỗ kia trên pano chưa tô màu, có vẻ là ổn đấy, nhưng khi khoảng không, như Victor Frankl nói, lấp đầy chủ đạo bởi thói quen này, trong tương lai không xa, những gì họ có trong tay là một bức tranh thiếu sức sống, xám xịt như những bước họ từng đi đầu tiên. Và đây mọi người gọi là "trầm cảm".

