---
layout: post
title: Không khí
date: 2021-11-20T09:28:55+07:00
tags: [air]
---

Giờ ngoài lúc viết mã với đọc sách báo thì mình nghĩ về không khí bị ô nhiễm bởi bụi mịn. 

Mình quan tâm tới chủ đề này cách đây hơn 1 năm, do mình nhớ cái lõi lọc của mình đã hết thời hạn sử dụng được mấy tháng rồi. Hồi đó mua cái máy lọc không khí bê về nhà, thấy cảm giác xa xỉ của một nhu cầu được hít thở không khí sạch. Thời gian đầu còn mang cái máy đó đi từng phòng trong nhà, cho nó chạy cái cảm biến đo chất lượng không khí, xem phòng này bao nhiêu, phòng kia bao nhiêu.

Đi cùng với lựa chọn đấy là việc thực hành mua rất nhiều khẩu trang PM2.5. Đi đâu mình cũng mang theo, còn mua cho người nhà.

Mình nghĩ do ở quen với môi trường ô nhiễm nên phải tới khi chênh lệch số trên cảm biến lên tới vài trăm, mình mới cảm nhận được rõ cái không khí nó ảnh hưởng tới sức khỏe và tinh thần như nào.

Hồi mình lang thang khắp các thư viện, mới có mỗi thư viện của Viện Goethe được đầu tư cái máy lọc không khí. Thấy bớt lạc lõng hơn hẳn.

Rồi mình cũng không đeo được khẩu trang mãi. Gặp gỡ mọi người rồi cũng phải gỡ khẩu trang, không phải chỗ nào không khí cũng sạch và càng không phải chỗ nào cũng có máy lọc không khí.

Đợt vừa rồi trên TV nghe loáng thoáng lại thấy họ nói về không khí ô nhiễm hơn.

Sáng sớm ngồi ở Hà Nội, ngửi thấy mùi khét trong không khí, nhưng vẫn mở cửa. Đường vẫn ướt, xem Visual Air mà mong quay về thời điểm trong Covid.
