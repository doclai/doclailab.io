---
layout: post
title: "Kỹ năng học tập suốt đời và sức khỏe tâm thần"
date: 2022-09-23T09:26:49+07:00
tags: [tlud]
draft: false
---

![](../018s.vi.jpg)

Sống ở những thành phố lớn khiến áp lực cuộc sống gia tăng và khiến chúng ta càng ngày càng cần phản hồi fight-or-flight (chiến đấu hay bỏ chạy). Một số nghiên cứu đã chỉ ra rằng, so với việc sống ở khu vực nông thôn, sinh sống ở thành phố  gia tăng các vấn đề về rối loạn lo âu, rối loạn trạng thái cảm xúc, cũng như các vấn đề sức khỏe tâm thần nghiêm trọng khác. Để đem lại sự cải thiện ít nhiều được hiện trạng này, chính những công dân thành thị, và những người làm quản lý, cần có nhìn nhận nghiêm túc về kỹ năng học tập suốt đời.

Trong mô hình phân loại tác động của việc học của Tom Schuller (2004), ta có thể chia dựa trên hai trục, về mức độ thay đổi, trục transforming-sustaining (chuyển hóa - duy trì), và về mức độ lan tỏa, trục individual - collective (cá nhân - cộng đồng). Dựa trên mô hình này, các tác động của việc học theo hướng duy trì thường khó nhìn thấy hơn các tác động theo hướng chuyển hóa, với nghĩa rằng, cần mất một thời gian đủ lâu để ta có thể nhìn thấy sự quan trọng khi mất đi việc học. Chẳng hạn như trong một ví dụ của ông, khi gỡ bỏ chương trình giáo dục cho người cao tuổi ở một địa phương, thời gian sau, mức độ trầm cảm tăng lên và tạo áp lực trở lại với hệ thống dịch vụ sức khỏe tâm thần.

Ở một bài viết khác của Cathie Hammond (2004) về tác động của việc học lên sự tự ổn thỏa, sức khỏe tâm thần, và cách đối phó hiệu quả, bà nhắc tới sự củng cố các “tác nhân tâm lý trung gian” ảnh hưởng tới sức khỏe tâm thần như: self-esteem (sự cảm nhận chung ít nhiều tích cực về bản thân, Emler 2001), self-efficacy (sự tự tin về năng lực tự giải quyết một vấn đề cụ thể), identity (bản dạng), purpose and future (cảm nhận về mục đích và tương lai), social integration (hòa nhập xã hội), communication and compentences (truyền thông và năng lực).

Trong buổi tối này, chúng ta cùng nhau tham gia thảo luận về kỹ năng học tập suốt đời ở Hà Nội, các thành phố lớn nói riêng, và những môi trường có tính áp lực cao nói chung.

Nguồn ảnh: Tác phẩm Learning to fly, của nghệ sĩ Phil Herbison