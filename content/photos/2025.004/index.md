+++
image = "2025.004.jpg"
date = "2025-02-13"
title = "Số 2025.004"
type = "gallery"
+++

Những thứ hay ho vài ngày qua

Hai bài này xuất hiện và đi song hành với nhau một cách đầy ngoạn mục

## [Họ nghĩ họ đã tự do](https://press.uchicago.edu/Misc/Chicago/511928.htm)

Đoạn đáng đọc này được trích từ đầu chương 13 của cuốn They Thought They Were Free: The Germans.

Nên trích lại nguyên văn một phần ở đây:

"You will understand me when I say that my Middle High German was my life. It was all I cared about. I was a scholar, a specialist. Then, suddenly, I was plunged into all the new activity, as the university was drawn into the new situation; meetings, conferences, interviews, ceremonies, and, above all, papers to be filled out, reports, bibliographies, lists, questionnaires. And on top of that were the demands in the community, the things in which one had to, was ‘expected to’ participate that had not been there or had not been important before. It was all rigmarole, of course, but it consumed all one’s energies, coming on top of the work one really wanted to do. You can see how easy it was, then, not to think about fundamental things. One had no time."

"Those," I said, "are the words of my friend the baker. ‘One had no time to think. There was so much going on.’"

"Your friend the baker was right," said my colleague. "The dictatorship, and the whole process of its coming into being, was above all diverting. It provided an excuse not to think for people who did not want to think anyway. I do not speak of your ‘little men,’ your baker and so on; I speak of my colleagues and myself, learned men, mind you. Most of us did not want to think about fundamental things and never had. There was no need to. Nazism gave us some dreadful, fundamental things to think about—we were decent people—and kept us so busy with continuous changes and ‘crises’ and so fascinated, yes, fascinated, by the machinations of the ‘national enemies,’ without and within, that we had no time to think about these dreadful things that were growing, little by little, all around us. Unconsciously, I suppose, we were grateful. Who wants to think?

"To live in this process is absolutely not to be able to notice it—please try to believe me—unless one has a much greater degree of political awareness, acuity, than most of us had ever had occasion to develop. Each step was so small, so inconsequential, so well explained or, on occasion, ‘regretted,’ that, unless one were detached from the whole process from the beginning, unless one understood what the whole thing was in principle, what all these ‘little measures’ that no ‘patriotic German’ could resent must some day lead to, one no more saw it developing from day to day than a farmer in his field sees the corn growing. One day it is over his head.

"How is this to be avoided, among ordinary men, even highly educated ordinary men? Frankly, I do not know. I do not see, even now. Many, many times since it all happened I have pondered that pair of great maxims, Principiis obsta and Finem respice—‘Resist the beginnings’ and ‘Consider the end.’ But one must foresee the end in order to resist, or even see, the beginnings. One must foresee the end clearly and certainly and how is this to be done, by ordinary men or even by extraordinary men? Things might have. And everyone counts on that might.

"Your ‘little men,’ your Nazi friends, were not against National Socialism in principle. Men like me, who were, are the greater offenders, not because we knew better (that would be too much to say) but because we sensed better. Pastor Niemöller spoke for the thousands and thousands of men like me when he spoke (too modestly of himself) and said that, when the Nazis attacked the Communists, he was a little uneasy, but, after all, he was not a Communist, and so he did nothing; and then they attacked the Socialists, and he was a little uneasier, but, still, he was not a Socialist, and he did nothing; and then the schools, the press, the Jews, and so on, and he was always uneasier, but still he did nothing. And then they attacked the Church, and he was a Churchman, and he did something—but then it was too late."

Bài này làm mình nhớ về chia sẻ của moral stance và moral courage trước đây từng ghi lại trên đây. Tất cả đều cần thực hành.

## [Phân loại học: về loài và phân loài](https://aeon.co/essays/the-case-for-subspecies-the-neglected-unit-of-conservation)

Mở đầu bằng câu chuyện chiếc thuyền của Noah, một cái nhìn chi tiết về những chuyển dịch tinh vi, qua việc nhìn ở khía cạnh phân loại học, và sau đó là tới cảm xúc, nhìn nhận của chúng ta bị ảnh hưởng, tác động tới đa dạng sinh học như nào.
Cụ thể hơn, thì việc mất mát, hay thậm chí tuyệt chủng, một số phân loài sẽ không gây được sự chú ý của ta khi đưa tin bằng sự mất mát của một loài. Chúng ta nghĩ những sự kiện đó có thể được cứu vãn "vào lần sau", vì nhìn từ lăng kính của "loài", thiệt hại đó "không đáng kể".
Và như chia sẻ của tác giả, việc dùng ngôn ngữ với phân loại học này của chúng ta, mang lại cảm giác thoải mái hơn với việc giảm thiểu vùng xám và sự bất định, nhưng vẫn gặp vấn đề, chúng có lỗ hổng.

Nhân bài này, mình có đọc một đoạn tin về sự kiện liên quan đến hệ sinh thái như này. Đầu tháng 9 năm 2024, một du khách vô tình đánh rơi một túi Cheetos bên trong Carlsbad Caverns, New Mexico, gây ra những hậu quả không ngờ cho hệ sinh thái mỏng manh của hang động. Cheetos, được làm mềm bởi độ ẩm của hang động, đã tạo ra môi trường lý tưởng cho vi sinh vật và nấm phát triển. Điều này đã thúc đẩy một mạng lưới thức ăn tạm thời bao gồm dế hang, ve, nhện và ruồi, phá vỡ sự cân bằng mong manh của hang động. Sự gián đoạn như vậy đặc biệt có tác động mạnh vì hang động là môi trường nhạy cảm, nơi các sinh vật thích nghi để tồn tại với lượng chất dinh dưỡng tối thiểu và việc đưa các yếu tố lạ vào có thể gây ra những thay đổi sinh thái đáng kể. Ngay cả những hành động nhỏ của con người cũng có thể có tác động đáng kể đến các hệ sinh thái này, khiến tác động của Cheetos lớn hơn nhiều so với tưởng tượng. Các nhân viên kiểm lâm của công viên đã dành 20 phút một cách tỉ mỉ để loại bỏ Cheetos và nấm mốc khỏi bề mặt hang động, nhấn mạnh tầm quan trọng của việc tuân theo các quy tắc của công viên, cấm tiêu thụ bất cứ thứ gì ngoại trừ nước thường trong hang động. Sự việc này nhấn mạnh thông điệp rộng lớn hơn của "Không để lại dấu vết", khuyến khích du khách giảm thiểu tác động của chúng ta đối với môi trường tự nhiên.

Dù ở mức phân loại học, thoạt nhìn, có vẻ "thụ động" hơn việc làm rơi Cheetos trong hang động, về mặt tác động, thì suy tư cuối cùng sâu thẳm, vẫn là, điều gì thực sự đang diễn ra, điều gì là thật, điều gì đáng kể, điều gì thực sự có nghĩa.

Xếp bài này cùng với link cuốn sách về câu chuyện lịch sử của Đức bên trên cho thấy chúng ta hay rơi vào tình huống của những con ếch luộc (the boiling frog) dễ dàng như nào. 
Những con ếch luộc ở tầm đa dạng sinh học và những con ếch luộc ở tầm chính trị.

