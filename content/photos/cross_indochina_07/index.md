+++
image = "cross_indochina_07.jpg"
date = "2025-01-10"
title = "Đạp xe xuyên Đông Dương - Phần 7"
type = "gallery"
+++

## Ngày 19

Bữa sáng no căng đét, nếu mà mình không kể là người Việt (chị hỏi lúc mình trả tiền) thì chị người Lào sẽ thối lại mình 40 thay vì 50.

Chiều chạy vào Pakse thấy đoàn này. Thường mình có ngó được mấy sư bé trong chùa, với một lúc có đoàn đi khất thực về mà mình chụp được ở Thakhek (ảnh trong phố), nay mới thấy một đoàn nhiều (4-5) sư bé đi khất thực dưới nắng như này.

Những ngày êm đềm thì có dịp mình nghĩ tiếp về vài chuyện.

Mình nghĩ chuyện này từ lúc sang Lào. Mỗi ngày mình đều chào và được những người trên đường chào, có những đám trẻ con mình không kịp chụp lại, không muốn chụp lại (có những thứ nên ghi vào ký ức hơn là hình ảnh), và chụp lại không xuể. Tưởng tượng như này cho đơn giản, rằng cứ mỗi km mình đi thì chào ít cũng 2 lần, nhiều thì hơn chục lần. Người thì gật đầu, người thì ngơ ngác bỏ qua, người thì trao cái hifive, đa số cười sảng khoái gào thêm những câu được dạy trong sách như "good morning", "what's your name?". Ngày tráng miệng độ đôi trăm cái chào trên đất Lào càng khiến mình nghĩ càng buồn về Việt Nam với câu hét to khiếm nhã trên đường lên cửa khẩu "Lô lô cái l*n" của học sinh Việt.

Thử nghĩ vài cách bao biện cho dân tộc mình mà không có bao biện được cách nào cho hợp lý.
