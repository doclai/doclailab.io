---
layout: post
title: Phong cách ông Điều
date: 2021-09-04T09:28:55+07:00
tags: [thoughts, movies]
---

Mở đầu bộ phim, ai nhắc đến chữ "tự do" bằng tiếng Việt, trong khung hình đen ngòm, khiến tôi bật hình dung ra cụm từ đứng giữa trong tiêu ngữ. Và vị trí trung tâm đó lại bất ngờ nhắc lại vài giây sau bởi nhận định _eccentric_ từ người vợ dành cho ông.

Có thể kể điều gì hay ho về ông? Nhận dạng ông dễ lắm: một người lười tắm, tóc dài qua tai, hay đội ngược cái mũ lưỡi trai ghi chữ "A Di Đà Phật" và viết thơ Pháp. Nghe đoạn những người ngoại quốc đọc thơ và nghe chia sẻ của ông, tôi lại lục được chia sẻ của hoàng đế Hirohito nói khi Nhật chấp nhận đầu hàng:

> The hardships and sufferings to which Our nation is to be subjected hereafter will be certainly great. We are keenly aware of the inmost feelings of all of you. Our subjects. However, it is according to the dictates of time and fate that We have resolved to pave the way for a grand peace for all the generations to come by __enduring the unendurable and suffering what is insufferable__.

Dù bối cảnh khác nhau, chúng đều đưa tới sự thay đổi. Hirohito mở ra trang mới tại Nhật, còn của ông, đơn giản là _réparer l'irréparable_.

Ngoài ra ông vẫn giữ lối sinh hoạt "đi cày", ít khi ra khỏi nhà mà vẫn tươi vui, nhắc nhở về trải nghiệm tự tại cho mọi người trong đợt giãn cách dịch. Ông có kiểu ngủ nghỉ như Balzac: đi ngủ từ sớm trước thời sự 1 tiếng và dậy từ 3 giờ đến 6 giờ sáng, cái giờ mà không tiếng ồn ông dành để dịch thuật và viết lách. Với lối sinh hoạt đó mang lại thu nhập gấp 4 lần cho ông so với kiểu làm việc 8 tiếng một ngày. Một lịch sinh hoạt, so ở thời điểm hiện tại với thế hệ công nghệ, thì thực sự rất trendy, như lối sống lành mạnh mới của những CEO, freelancer thế hệ 8x-9x. Có một câu mà ông nói, đại lọai là _wise with time_.

Một thói quen khác của ông, khi tôi thấy những trang sách ông dịch, là dùng hoàn toàn dùng bút và giấy, rồi xong xuôi mới gõ máy tính. Làm tôi nghĩ về thói quen dùng đồ công nghệ hiện tại, và sự dồi dào xúc giác của giấy bút. Khi di chuyển nhiều hơn, và cũng cần sự tự do trong giao diện văn bản, smartphone và giấy là một sự kết hợp hiệu quả, khắc phục điểm yếu về khả năng sắp xếp tuyến tính thời gian thứ tự sự kiện của công cụ. Cũng có một điều mà tôi nghĩ gần đây, về việc thấy thiếu _sự sẵn sàng_ cũng như _sự chân thật_ về những ý tưởng muốn chia sẻ. Thực hành sự mân mê chúng trước khi làm trò gì đó là cần thực hiện. Vậy nên quan sát nhiều hơn và viết.

_Truth are buried_ cùng những hạt cát của Nguyễn Khải lại thấy một bài học cho người trẻ, rằng khiêm tốn không bao giờ là thừa.
