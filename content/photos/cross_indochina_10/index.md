+++
image = "cross_indochina_10.jpg"
date = "2025-01-16"
title = "Đạp xe xuyên Đông Dương - Phần 10"
type = "gallery"
+++

## Ngày 25

Khi mà chim cất cánh còn bị thổi ngược, thì mình liên tục chửi thề không đếm được bao nhiêu lần cái gió khiến mỗi giờ mình đi được có 6 km. Xong còn quả đang đi vệ sinh thì xe bị thổi rơi xuống rãnh thoát nước (may là rãnh khô) và chổng ngược lên trời, rớt gần sạch cốc nước mía đang treo. Thêm 2 lần tuột xích nữa, ối giời, một tràng chửi thề =)).

Gió, kiểu, chiều qua giặt quần áo thì tối khô luôn, tiện thật, chứ gió thế này, chỉ muốn chửi thề.

Đến gần Snuol lấy ngay 1 lon nước tăng lực Krud để chiến bằng được về biên giới Việt Nam.
Và chửi thề thì cũng không quên chụp lại công viên tưởng niệm Pir Thnu trên quốc lộ 76.

Gần 11 tiếng đạp chỉ để qua hơn 100km về đến cửa khẩu Hoa Lư tỉnh Bình Phước.
