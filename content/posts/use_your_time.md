---
layout: post
title: Dùng thời gian bản thân
date: 2022-06-27T09:28:55+07:00
tags: [learning, time]
---

Viết lại cho tôi 6 năm trước. 

Một điều được bạn nên bắt đầu làm khi rời khỏi trường phổ thông, là hãy học cách coi trọng thời gian của bạn như một sự thật rằng bạn hoàn toàn quản lý chúng. Trường đại học, chỗ làm, gia đình, không một ai trong số đó sẽ quản lý thời gian của bạn được cho con đường tìm kiếm bản thân. Đã bước trên con đường này, khởi đầu một cách không chuẩn bị gì cả, một cách đầy mò mẫm và sau 6 năm tôi mới nhận ra những thứ quan trọng với bản thân mình, tôi nghĩ mình nên chia sẻ trải nghiệm cá nhân lại ở đây.

__Nghĩ, và viết__. Bất kể bạn có anti-social hay pro-social trong quãng thời gian nào của cuộc đời, thất nghiệp hay công việc ổn định phát triển, thăng tiến, thì việc bạn viết thường xuyên, giúp bạn có thói quen viết, nghĩ mạch lạc về những thứ bạn quan tâm và biết được giá trị bạn nắm giữ. Tuyệt vời hơn thì bạn cần lưu chúng xuống đâu đó, qua việc chăm sóc blog chẳng hạn. Những người đồng hành đến với bạn, qua việc thể hiện được con người bạn trong quá trình tìm ra các giá trị nắm giữ này, hoặc qua việc biết được hành trình của nhau nhờ blog. 

__Xây dựng các thói quen tốt__. Câu quen thuộc của J. Peterson, để bạn bớt phải đau khổ bởi những sự ngu xuẩn tự thân, mà dành sự đau khổ đó trải nghiệm với những thứ khác quan trọng hơn. Những thói quen, tưởng đơn giản, nhưng là phần lớn của cuộc đời bạn, không nhầm đâu, đơn giản vì chúng được xây dựng thường xuyên. Khi có kỹ năng hình thành thói quen tốt, các kỹ năng hữu ích khác sẽ được xây dựng trên nền tảng của những thói quen này.

__Thực hiện các hoạt động có tính self-expanding__. Điều này hết sức quan trọng khi bạn bắt đầu tự quản lý thời gian cá nhân. Khi bạn tự quản lý thời gian cá nhân, đồng nghĩa với vấn đề bạn lựa chọn để giải quyết ở mức độ cao hơn nhiều so với một người có thời gian được quản lý bởi một bên khác. Điều này dẫn tới sự thay đổi về cảm nhận của bạn về tương quan năng lực bản thân và các thử thách (self-efficacy). Để hỗ trợ củng cố self-efficacy, có hai hướng chính của các hoạt động có tính self-expanding: về mặt tương tác xã hội, hãy xây dựng các mối quan hệ có chiều sâu, tìm kiếm đồng đội, các hội nhóm; về trải nghiệm cá nhân, thử các trải nghiệm mới lạ, mang tính hấp dẫn, thách thức. 

p/s: Một bài viết về self-expanding sẽ lên blog này sớm thôi.