---
layout: post
title: "Đồ văn phòng phẩm"
date: 2020-03-31T09:28:55+07:00
tags: [stationery, learning, journaling, memories, photos]
---

Trong quá trình đọc cuốn Essential Study Skills của Linda Wong, nhận thấy khả năng tiếp nhận thông tin của bản thân thông qua thị giác và xúc giác nên là cần thay đổi thói quen của tôi. Sử dụng giấy và sổ nhiều hơn, bên cạnh việc gõ bàn phím.

Sử dụng cách dùng máy tính/kindle kết hợp ghi note thì nội dung mới có được tương tác xúc giác/vị trí (tactile memory). Thực tế là có quá nhiều thứ mà nếu in hết ra thì cuối cùng vẫn phải note, nên hãy thực hiện luôn ý tưởng này.

Và dạo quanh trên mạng tìm hiểu về màu mực, và cũng là tôi không muốn sử dụng mực đen liên tục trong các ghi chép cá nhân. Nhiều người nghĩ là sử dụng khác màu, đặc biệt màu xanh dương hoặc đỏ thì mức độ ghi nhớ thông tin cao hơn, nhưng thực tế là do những màu đó ít được sử dụng, chứ một người sử dụng một màu mực xuyên suốt thì khác biệt hiệu quả ghi nhớ không rõ rệt. 
Tôi tìm thứ gì đó khác hơn. Nếu mà xài bút máy thì đúng là tự chọn, tự pha màu mực theo RGB được đó, cơ mà để sự tỉ mỉ đó lại khi khác.
Không có điều kiện đầu tư bút máy với lọ mực hẳn hoi nên kiếm bút đi nét. Hiện dùng bút Micron 05 Burgundy. Cảm thấy ưng ý, nhìn xa thì không thấy sự khác biệt lắm đâu.
![burgundy vs black](/images/IMG_20200331_102632.jpg)
