---
layout: post
title: Giai đoạn bế tắc
date: 2022-09-19T05:30:55+07:00
tags: [deadlock]
---

Sau một thời gian dài tôi tạm dừng việc đọc và tự học, đồng nghĩa với việc tạm dừng xây dựng bản dạng một người viết, tôi khởi đầu lại, tôi định viết viết về triệu chứng âm avolition và cách tự chăm sóc bản thân khi bạn thấy bản thân đang trong một tình trạng như vậy, tuy nhiên, viết như vậy sẽ xa rời trải nghiệm thực tế của tôi. Tôi để bài viết này ở mục Nghĩ ngợi thay vì ở mục Tâm lý.

Tôi còn nhớ một câu nói đọc được trong lúc dựng con bot danh ngôn từ GoodReads, rằng sự khó khăn, đau đớn không phải là sự thật, mà sự thật sẽ lộ diện sau khi đi qua được trải nghiệm đấy. Nhờ việc hành thiền cùng đồng môn hàng ngày mà tôi vẫn giữ được cấu trúc lịch sinh hoạt hàng ngày ổn định và có sức khỏe tinh thần tốt hơn. Tôi biết giai đoạn này của tôi có nhiều những thay đổi lớn trong mọi mặt đời sống, có thể những quyết định đấy cũng nhỏ với những người bình thường khác thôi, nhưng tôi cần luôn tự nhắc bản thân hãy tập trung và bình tĩnh đi qua chúng. Thực hành sự từ bi với bản thân khi những sự cố luôn có thể xảy ra, và nhẹ nhàng gửi thông điệp tới những người xung quanh tôi vào thời điểm đấy: "mọi chuyện rồi cũng ổn thôi mà".

Chần chừ rất nhiều lần để đưa được những suy nghĩ này xuống mà không nhất thiết tôi phải đọc vài bài báo nghiên cứu. Cảm thấy nhẹ nhõm hơn nhiều.
