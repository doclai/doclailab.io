---
title: "Nỗi sợ"
date: 2023-04-02T20:53:13+07:00
tags: ["fear"]
toc: "false"
draft: false
---

Sáng nhân dịp thảo luận với một anh bạn về giáo dục, có vài dòng ghi chép về đoạn thảo luận nỗi sợ, mà quên mất mạch câu chuyện nào đã dẫn chúng tôi tới đó. Tôi nhận thấy việc thiếu vắng nỗi sợ nói chung liên quan tới sự thiếu vắng mục tiêu, cơ bản là nếu một người có bản đồ (map) hình dung về những gì anh ấy/cô ấy muốn tiến tới trong cuộc sống này thì những trục trặc cản trở khả thi sẽ luôn gây ít nhiều nỗi sợ. Anh ta lấy ví dụ về câu chuyện đánh trận, một vị tướng mà không có bất kỳ nỗi sợ nào có thể dễ dàng phung phí nhân lực và tài nguyên đến nhường nào. Một cách thực tế, vị tướng đấy vẫn chứa nỗi sợ, nhưng không bị chi phối bởi nỗi sợ.

Buổi trưa ăn bún, anh ta kể về câu chuyện lâm sàng sắp sửa thảo luận buổi chiều, nhân chủ đề câu chuyện về bệnh tật và điều trị, tôi chia sẻ với anh ta về "màn đêm của tâm hồn". Chiều tới đoạn tôi nhớ được một đọan rằng những trẻ được đi qua cơn ốm một cách chậm rãi, không phải dùng những tác động mạnh từ bên ngoài (ví dụ: khánh sinh), sẽ có sự thông minh hơn. Tôi cũng nghĩ như vậy với sự thông minh hiện thân và sự khít khớp thân thể và tâm trí.
