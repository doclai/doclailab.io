---
layout: post
title: "Tiết lộ bản thân (self-disclosure) trong mối quan hệ"
date: 2022-02-24T09:28:55+07:00
tags: [tlud]
---

Tiết lộ bản thân (self-disclosure) trong mối quan hệ: cởi mở hay kín đáo? (24/02/2022)

Khi làm quen với một người mới, liệu bạn có hay lập tức cởi mở chia sẻ những thông tin hay câu chuyện riêng tư của mình không? Hay bạn là người kín đáo, và chỉ chịu chia sẻ những thông tin cá nhân với một số ít người mà bạn cảm thấy thực sự tin tưởng?

Trong tiếng Anh, self-disclosure (sự tiết lộ bản thân) trỏ cho việc chia sẻ, vô tình hay hữu ý, những thông tin cá nhân của bạn như cảm xúc, suy nghĩ, kỷ niệm,…

Việc bạn chọn ai để chia sẻ thông tin cá nhân và thông tin nào bạn lựa chọn để chia sẻ phản ánh mức độ thân mật trong quan hệ của bạn với người ấy. Nhiều khi bạn dễ thấy bản thân có nhu cầu muốn đưa ra chia sẻ cá nhân hơn khi được nghe một câu chuyện riêng tư nào đó từ phía đối diện. Những chia sẻ này có thể khiến phía đối diện kia trở nên đáng tin tưởng và quan trọng hơn với bạn. Hay cũng nhiều khi bạn lại thấy một điều ngược lại, những điều riêng tư của phía đối diện kia bắt đầu có vẻ tạo áp lực thêm cho bạn, bạn không biết làm gì với những thông tin mới này của họ. Trong một bối cảnh khác, bạn có lúc sẽ dừng lại sau một thời gian chia sẻ và bất chợt bật lên câu hỏi trong suy nghĩ như “Liệu mình làm như thế này có cởi mở quá không?” hay “Vì sao họ không có chia sẻ gì mấy với mình?”.

Tiết lộ bản thân không đơn thuần chỉ qua lời nói, mà có thể qua những cách thể hiện khác: Liệu bạn có nên để lộ hình xăm với người nhà, hay như chuyện, bạn có nên diện một bộ trang phục cá tính ở công ty? Điều tưởng chừng như hiển nhiên trong tương tác xã hội này có thể khiến cho mối quan hệ lạnh nhạt, ngộp thở, hay ấm áp. 