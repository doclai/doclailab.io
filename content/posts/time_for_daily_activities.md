---
title: "Dành thời gian cá nhân hàng ngày"
date: 2023-04-20T12:57:35+07:00
tags: ["daily", "activities"]
toc: "false"
draft: false
---

Hôm trước ngồi cafe với một ông bạn, nghe câu chuyện dành thời gian cho các dự án cá nhân sau một ngày dài làm việc của đôi vợ chồng này đang gặp khó khăn. Họ cũng đang xây dựng lối sống mới sau hôn nhân

Câu chuyện về chuyện ngoại ngữ, ngôn ngữ cần cộng đồng, cần nhóm để sinh trưởng. Và gần như mọi thứ mình làm và phát triển mạnh đều nhờ cộng đồng. Việt Nam mặc dù có những cộng đồng như vậy nhưng không có những cộng đồng rộng và mạnh như các quốc gia khác phía Âu Mỹ.

Và khi dành thời gian cho việc viết blog update hàng ngày, mình mới nhận ra mình cần thêm độ 2 tiếng đồng hồ thì mới có thể đọc hết các tài liệu cần đọc trong ngày.
