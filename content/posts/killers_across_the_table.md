---
layout: post
title: Người phía bên kia bàn
date: 2022-01-19T09:28:55+07:00
tags: [mind, crime]
---

Một cuốn sách viết về các sự kiện có thật, không dễ đọc lắm. Mình mới hoàn thành được phần 1 của cuốn sách, để gọi là có một hình dung đầy đủ cách tác giả phân tích một trường hợp được nêu ra. 

Phần 1 của cuốn sách kể về một vụ án vào năm 1973. Đó là một vụ án ấu dâm giết người bởi một giảng viên 27 tuổi Joseph McGowan, có bằng tiến sĩ khoa học, với một cô bé 7 tuổi Joan D'Alessandro. Giám định pháp y thời điểm đó, Frederick Zugibe, nhận định là một trong những vụ án tàn khốc nhất từng tham gia: rạn nứt cổ, bị thắt cổ, vai phải bị chật, vết bầm sâu, rách cằm và môi trên, rạn nứt hộp sọ trước, rạn nứt xương lá mía, xưng phồng mặt, mất ba chiếc răng, tụ máu não, tổn thương phổi và gan, rách màng trinh.

"Một người hay một con quái vật như nào mới để lại thứ kinh khủng như này với một cô bé 7 tuổi?"

"Một vài tiếng trước, anh ta còn đứng lớp dạy môn hóa học cho học sinh. Điều gì đã dẫn anh ta từ điểm đó tới điểm này?"

Bất ngờ đầu tiên ở tên tiêu đề, thể hiện sự bình tĩnh khi có thể ngồi lại với một kẻ giết người, trên đây là những câu hỏi mà tác giả đặt ra trên quá trình đọc lại hồ sơ vụ án, chuẩn bị cho việc gặp mặt trực tiếp McGowan.

Vụ án này, nhờ sự vận động không ngơi nghỉ của mẹ Joan, bà Rosemarie, đạo luật mang tên con gái bà, Joan's law (tù chung thân cho tội giết và tấn công tình dục trẻ em dưới 14 tuổi) đã được ký tại New Jersey vào 1997 và một phiên bản liên bang vào 1998.

Về tác giả, ông là cựu nhân viên FBI, với sự nghiệp 25 năm ở cục Khoa học hành vi, một trong những chuyên gia hàng đầu về nghiên cứu tiểu sử tâm lý tội phạm và là người tiên phong trong phân tích điều tra tội phạm hiện đại. Cuốn sách này là nguồn cảm hứng cho các phim dài tập thu hút đông đảo sự chú ý: Criminal Mind và Mindhunter.
