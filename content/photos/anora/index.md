+++
image = "anora.jpg"
date = "2025-02-15"
title = "Anora"
type = "gallery"
+++

Tôi đến với Anora như xé túi mù của Cannes 2024.

Dù là định bàn về chia sẻ của Xiang Biao về "những điều gần gũi", thì hơn cả vẫn là khả năng tiếp cận và thể hiện sự thân mật được. Tôi bắt đầu thấy muốn bớt đi thời gian với các  bài phân tích.

Anora với những đoạn hội thoại chậm, một cách hết sức chân thật, làm sống lại từng dòng viết chung cách đây 3 năm, mà hiện tại, tôi mới chỉ đủ dũng cảm đọc lại với tất cả sự dè dặt cảm xúc.
