---
layout: post
title: "Ngược đãi bản thân"
date: 2019-05-31T09:28:55+07:00
tags: [mistreat]
---

Một tuần nhúng mình với những trải nghiệm mới, tôi đã thấy bản thân chậm chạp trong việc nắm bắt được quá trình học tập cũ, và giờ đưa ra lựa chọn là sẽ viết về những gì tôi còn cảm nhận lại từ các buổi nói chuyện. Ý tưởng là những đứa con tinh thần. Đẻ ra rồi mà không chăm sóc được chúng hoặc không đưa cho ai đó có thể chăm sóc, là một tội ác.

