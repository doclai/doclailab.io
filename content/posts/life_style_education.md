---
title: "Về lối sống, về tuổi trẻ"
date: 2023-08-01T11:42:36+07:00
tags: []
toc: "false"
draft: false
---

Khi thấy tuổi trẻ bị tiêu tốn cho việc kiếm tiền, mình thấy việc dành nhiều thời gian với mấy đứa cháu trở thành điều quan trọng để xây dựng lối sống của mình. Thực sự lo lắng khi thời gian vừa qua mình còn chẳng có đầu tư thời gian cho việc đọc sách nữa. Bắt đầu đặt những câu hỏi khác nữa khi mình 

Mình muốn dành thời gian 5 năm tới đầu tư thực sự nhiều trong giáo dục cho những đứa cháu nhỏ ở nhà, tránh chuyện hôn nhân trong 5 năm tới để thực hiện các dự án giáo dục này, đồng thời hình thành lối sống từ freelancer. Tầm nhìn xa là mình có khả năng và phẩm chất để thực hiện được ý tưởng homeschooling với môi trường gần gũi thiên nhiên hơn là duy trì ở trung tâm Hà Nội.

Hiện giờ, mình đã làm vé tháng xe buýt được cho gần hết những đứa nhỏ ở nhà. Tấm vé tháng này sẽ, đầu tiên là cho chúng được vận động trên đôi chân nhiều hơn, và tiếp theo là trải nghiệm được đa dạng hơn không gian tại Hà Nội, hơn là chỉ tham gia giao thông thụ động trong ô tô hay trên xe máy được người khác chở.

Mình bắt đầu nghĩ về những câu hỏi, những ưu tiên về nghiên cứu hay cả về xây dựng sự nghiệp. Câu chuyện vẫn là việc mình xác định mục tiêu sống là gì và thể hiện mong muốn đó xuyên suốt đời sống này. Chung quy quay về chữ "giới" khi tiết chế năng lượng của mình.
