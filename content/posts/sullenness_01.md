---
title: "Sự ủ rũ 01"
date: 2022-12-18T08:59:29+07:00
tags: ["sullinness"]
toc: "false"
draft: false
---

Viết về sự ủ rũ.

Mồm nói là không biết làm gì trong cuộc đời nhưng lại bị mất tập trung bởi các thiết bị đa phương tiện. Vậy nên tôi thiết kế hệ thống mạng tự động ngắt kết nối theo thời gian, để rơi đủ sâu vào thứ mà tôi luôn lẩn tránh bằng những sự kiện, những cuộc gặp, hẹn hò.

Nhưng rồi tôi tìm được trong tất cả những khoảng không đó là tìm thấy những gì chôn vùi từ lâu: một chút văn học Nga, Zarathustra, và Charles D. Hayes. Mừng là chỗ ở của tôi đủ heo hút vắng vẻ, mong ít nhà hơn.
