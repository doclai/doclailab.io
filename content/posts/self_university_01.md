+++
title = "Đại học tự học (self-university)"
date = "2025-01-29"
+++

Nhờ những người trăn trở tôi lượm đọc được trên feed, như những miếng bùi nhùi đầu tiên, nhóm lên lại ngọn lửa mới cho một kế hoạch dài hơi mà tôi đã tắt từ lâu. Mở đầu năm mới, tôi lên kế hoạch theo đuổi lại một cách bài bản những mảng mà tôi bỏ dở cách đây vài năm. Đó là về xã hội và triết học, với cách bám theo lộ trình cử nhân từ một số nơi tôi hay ghé qua như UC Berkeley, đại học Hồng Kông, OCW... Cũng dễ hiểu vì sao tôi đã dừng trước kia, phần nhiều tôi không thấy sự tương tác văn hóa sống động trong con người tôi, như sau hành trình đạp xe vừa rồi. Như tôi có chia sẻ trên Facebook cá nhân, và cũng như khi cafe với một người bạn kể về động lực trước chuyến đi khi đã trở về, thì tôi vẫn còn nhiệt để đi tiếp lắm, cơ mà trong hành trình tôi thử viết, ngẫm, đạp để có dịp suy tư và chạm tới vài thứ quan trọng, nên cũng giúp tôi biết cần điều tiết lại năng lượng và mở ra những hành trình khác.

Blog tôi sẽ trở nên sôi động hơn, nhờ hàng tuần sau này, ngoài những bài viết tâm sự góc nhìn, review sách, tôi sẽ còn đăng các bài thu hoạch ở những thứ học được bên trên nữa. 

Ban đầu, ai cũng sẽ nghĩ sẽ cần sắp xếp tâm trí và thời gian gắt gao lắm cho những kế hoạch kiểu này, cơ mà tôi đang làm quen với chuyện đọc và viết trong thời gian rảnh. Rất nhiều sự thay đổi sẽ nằm ở chỗ, một người làm được gì trong thời gian này.

Bắt đầu với một khóa nhân học mới [này](https://ocw.mit.edu/courses/21a-00-introduction-to-anthropology-spring-2013/) và trở lại khóa triết cũ [này](https://ocw.mit.edu/courses/24-00-problems-of-philosophy-fall-2019/). Mở ra một cuốn nằm trong tủ sách Oxford World's Classics (gồm hơn 700 đầu sách) mới thấy con đường tới mênh mông như nào. Mỗi tuần một session của một khóa, chạy song song, mong tầm nửa năm là thấm chút ít được tinh hoa của những ý tưởng này. Với mục tiêu như vậy, tôi đang tập làm quen với chuyện mỗi ngày có khả năng đọc một khối lượng khoảng tầm 5-10 nghìn từ, và phần lớn thời gian còn lại để viết. Những ngày đầu trải nghiệm với tầm 2-3 nghìn từ, xong ngồi viết, tôi đã cảm thấy tăng sự hứng thú và ưu tiên với kênh thông tin chữ viết hơn vượt trội so với kênh đa phương tiện.
