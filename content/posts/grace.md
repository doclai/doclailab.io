---
layout: post
title: Grace
date: 2021-12-27T09:28:55+07:00
tags: []
---

Cảm ơn đất trời nâng đỡ cho con được đững vững để nhìn và nghe thấu những chuyện hỷ nộ ái ố cùng một trong ngũ giới của nhân gian. 

Vẫn còn có trục trặc ngay lúc hạ bút được dòng này. 

Biết lần sau đi xa, tới được nơi an toàn thì có thể bỏ điện thoại đi, để tập trung vào những gì xảy ra ở hiện tại. Giúp cho những người xung quanh được độc lập hơn để tập trung tâm trí.

Biết mỗi người có những khả năng cần sự đầu tư thời gian và sức lực khác hẳn nhau.
