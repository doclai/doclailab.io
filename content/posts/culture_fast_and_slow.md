---
title: "Trải nghiệm văn hóa. Chuyện nhanh hay chậm"
date: 2023-05-05T14:55:42+07:00
tags: []
toc: "false"
draft: false
---

Một người có thể nói với bạn rằng bạn là một người chậm chạp. Trước thời điểm đi thiền và sau đấy, mình từng thấy những người khác bị nhắc là "làm chậm như rùa bò", "mối xông chân" nhưng rồi mình nghĩ sâu xa hơn về câu chuyện đó. Nhân dịp nay ngồi cùng một anh bạn lớn hơn mình độ gần một giáp, ngồi nghe anh ấy kể chuyện sau ốm dậy là chính chứ cũng không có nhu cầu muốn chia sẻ gì, anh ấy chia sẻ rằng mình cần "trở nên nhanh hơn", "năng động hơn". Câu chuyện nhanh hay chậm mình từng nghĩ một thời gian lâu trước kia, giờ tiện có dịp viết lại đôi chút ở đây.

Đứng từ một vị trí từng nhìn thấy người khác nhanh hơn mình cũng như ở vị trí nhìn thấy mình nhanh hơn người khác, câu chuyện liệu có đơn giản là người khác có thể nói bạn cần "nhanh hơn"? Điều gì khiến cho những người nhanh hơn kia họ, vô tình hay hữu ý, bỏ qua sự trù phú với đủ các hình thái (kiểu như, vì sao những người "phản ứng chậm" họ vẫn ở lại, chưa muốn kết thúc một tác vụ nào đó) mà lại đặt câu hỏi về thời lượng của một trải nghiệm nội tâm?

Cũng có phần thú vị mình ngẫm lại sau khi anh ấy kể về những trải nghiệm ăn uống cần có, lựa chọn đồ dùng cá nhân tinh tế như nào, và khuyên mình nên đi trải nghiệm nhiều hơn, tôi bắt đầu cảm thấy hơi ngợp và chóng mặt về những danh sách lựa chọn anh ấy kể ra. Sau cùng thì, tôi nghĩ vẫn sẽ tự cho bản thân những trải nghiệm như vậy nếu có dịp.
