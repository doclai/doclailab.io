---
layout: post
title: Wright Mills, cô bán rau và sự trợ giúp
date: 2021-08-22T09:28:55+07:00
tags: [thoughts, learning, computer]
---

Nhân một câu hỏi khi rẽ ngang một post trên Facebook của người bạn chia sẻ podcast của VnExpress phỏng vấn một trường hợp gặp khó khăn ở Sài Gòn. Chị bán rau nuôi sống gia đình từ trước khi diễn ra sự phong tỏa toàn thành phố, sinh sống qua đợt này nhờ tiền tích cóp, ... Một comment tôi có đọc được bên dưới bài chia sẻ: _"Sao họ không để lại liên hệ để hỗ trợ anh nhỉ?"_. Câu hỏi có một sự thôi thúc hành động, một sự sẵn sàng, và nhanh chóng có thể mường tượng ra một thứ hút sức lực chúng ta vào. Những câu hỏi hiện ra song song, rằng liệu _"Đội podcast đã quên điều đó khi tác nghiệp?"_, _"Còn điều gì ta có thể làm được tiếp không?"_, ...

Tôi nghĩ câu hỏi chạm được tới điều mà Wright Mills phân biệt giữa vấn đề xã hội _(issue)_ và khó khăn cá nhân _(trouble)_. Cảm nhận thôi thúc chúng ta để lại liên hệ hỗ trợ có thể nhìn thấy là một cách thức quen thuộc ở các chương trình tương tự như _"Tấm lòng nhân ái"_. Ở đó, họ tìm thấy đa dạng các cá nhân đặc biệt khó khăn, trên từng số phát sóng, kết nối sự giúp đỡ từ các nhà hảo tâm trên cả nước với hoàn cảnh đó. Việc để lại thông tin cá nhân trên tập podcast sẽ hướng sự giúp đỡ vào một cá nhân không phải là một sự kết nối hiệu quả mà cộng đồng người Sài Gòn cần khi có rất nhiều trường hợp khó khăn như vậy.
