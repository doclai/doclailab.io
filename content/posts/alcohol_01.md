---
layout: post
title: Ảnh hưởng của cồn
date: 2024-10-15T10:28:55+07:00
tags: [thoughts, alcohol]
---

Nhân sự kiện gần đây của nồng độ cồn ảnh hưởng tới khả năng định vị các chi của tôi trong thế giới thực (impaired coordination) dẫn đến chuyện tôi đã ngã xe mà không có khả năng bật ra khỏi xe như nhiều những lần trước (đúng vậy, kiểu như tích tắc trước khi xe đổ, tôi có thể bật đứng dậy và chỉ nhìn xe trôi đi). Trải nghiệm của cú ngã xe là một trải nghiệm thụ động, và có chút tận hưởng giây phút nằm xuống mặt đất. 

Trải nghiệm hệ thần kinh của tôi có thể dẫn ra từ ảnh hưởng tới khu vực tiểu não (cerebellum, khu vực phía sau gáy theo vị trí giải phẫu). Trải nghiệm mấy ngày gần đây khi tôi gia tăng dùng bia cũng có cảm nhận về sự "vô hiệu hóa" khu vực gáy này, khi các khu vực khác có thể cảm nhận được bằng cách đưa sự chú ý.

Xây xát người và (có vẻ như chỉ là) chấn thương cơ một bên cổ, đi kèm chiếc balo rách, và một ý thức vẫn còn nguyên vẹn, nhắc nhở về đặc tính của cơ thể tôi.
