+++
image = "2025.001.jpg"
date = "2025-01-31"
title = "Số 2025.001"
type = "gallery"
+++

Những thứ hay ho vài ngày qua

- [Vũ điệu đá cổ Pavie](https://antg.cand.com.vn/Kinh-te-Van-hoa-The-Thao/vu-dieu-da-co-pavie-i756138/): đường đá cổ Pavie, chưa lên đây, nhưng hẹn sẽ có dịp
- [Đời sống tàu điện ngầm New York](https://www.newyorker.com/news/our-columnists/my-life-on-the-subway): mình có viết một bài ngắn ở [đây](https://blog.doclai.com/posts/new_york_subway/)
- [Trolley problem và đạo đức thực tế](https://psyche.co/ideas/what-a-real-life-trolley-problem-reveals-about-morality): mình có viết một bài ngắn ở [đây](https://blog.doclai.com/posts/real_life_morality/)
- [Ý tưởng về hòn đảo](https://www.themarginalian.org/2025/01/21/islands/):
Lối ẩn dụ những hòn đảo xuất hiện khi mình đang nghĩ về ý tưởng trải rộng nỗi niềm trong lòng của Rilke. Tác giả còn kể tới chuyện lối ẩn dụ này xuất hiện trong nhiều mô hình khác: mô hình vũ trụ, mô hình hiểu biết, mô hình mối quan hệ. Một chùm các bài viết cho những mô hình đó:

[Vực thẳm giữa chúng ta](https://www.themarginalian.org/2025/01/16/abyss/)

Tác giả của tập thơ __The Wren, The Wren__ có viết:

> We don’t walk down the same street as the person walking beside us. All we can do is tell the other person what we see. We can point at things and try to name them. If we do this well, our friend can look at the world in a new way. We can meet.

Đoạn này hay quá nên muốn dịch:

> Ta không đi cùng một con phố với người bên cạnh mình. Tất cả những gì ta có thể làm, là kể cho người kia biết những gì ta thấy. Ta có thể chỉ vào các thứ và cố gắng gọi tên chúng. Nếu ta làm tốt điều này, bạn của ta có thể nhìn thế giới theo một cách mới. Chúng ta có thể gặp nhau.

```
a friend
is a chosen path of light
into the dusky hollow
of a person
a tender return
to the abandon parts within
that can render
the old field of torment
newly ploughed with love
```

[Giấc mơ tới một thiên hà khác](https://www.themarginalian.org/2021/02/16/thomas-wright-original-theory/)

[Song đề con nhím (porcupine dilemma)](https://www.themarginalian.org/2023/08/03/porcupine-dilemma/)

Song đề quen thuộc mà trong tâm lý học hay dùng để kể về sự cân bằng giữa sự gần gũi và sự độc lập trong mối quan hệ. Tác giả có kể về một cuốn của Rilke, _Rilke on Love and Other Difficulties_, tổng hợp bởi một tác giả khác, và một cuốn thơ của Gibran. Mình để lại vài dòng ở đây để dành đọc tiếp.

```
Let there be spaces in your togetherness,
And let the winds of the heavens dance between you.

Love one another but make not a bond of love:
Let it rather be a moving sea between the shores of your souls.
Fill each other’s cup but drink not from one cup.
Give one another of your bread but eat not from the same loaf.
Sing and dance together and be joyous, but let each one of you be alone,
Even as the strings of a lute are alone though they quiver with the same music.

Give your hearts, but not into each other’s keeping.
For only the hand of Life can contain your hearts.
And stand together, yet not too near together:
For the pillars of the temple stand apart,
And the oak tree and the cypress grow not in each other’s shadow.

The Prophet, Kahlil Gibran
```

Rilke chỉ ra điều này, không chỉ đúng với hôn nhân, mà với cả những mối quan hệ thân mật muốn được nuôi dưỡng dài lâu:

> I hold this to be the highest task of a bond between two people: that each should stand guard over the solitude of the other. For, if it lies in the nature of indifference and of the crowd to recognize no solitude, then love and friendship are there for the purpose of continually providing the opportunity for solitude. And only those are the true sharings which rhythmically interrupt periods of deep isolation. (Rilke)

> All companionship can consist only in the strengthening of two neighboring solitudes, whereas everything that one is wont to call giving oneself is by nature harmful to companionship: for when a person abandons himself, he is no longer anything, and when two people both give themselves up in order to come close to each other, there is no longer any ground beneath them and their being together is a continual falling… Once there is disunity between them, the confusion grows with every day; neither of the two has anything unbroken, pure, and unspoiled about him any longer… They who wanted to do each other good are now handling one another in an imperious and intolerant manner, and in the struggle somehow to get out of their untenable and unbearable state of confusion, they commit the greatest fault that can happen to human relationships: they become impatient. They hurry to a conclusion; to come, as they believe, to a final decision, they try once and for all to establish their relationship, whose surprising changes have frightened them, in order to remain the same now and forever (as they say). (Rilke)

[Hòn đảo ẩn dụ cho sự hiểu biết](https://www.themarginalian.org/2015/02/02/the-island-of-knowledge-marcelo-gleiser/)

[Hòn đảo ẩn dụ cho sự tự hiểu biết](https://www.themarginalian.org/2021/09/09/aldous-huxley-island-universes/)

Những ý tưởng này không quá mới lạ với tôi, nhưng mà cách các tác giả diễn đạt về ý tưởng khiến mình nghĩ về chuyện vượt qua được sự khô cằn của ngôn ngữ thông thường để đưa được sự tươi mới của đời sống. Tôi đã có dịp viết lại về sự cần thiết của việc diễn đạt lại những ý tưởng. Thơ là một trong số những cách như vậy.

- [Tiểu thuyết về Chân dung bệnh mất trí nhớ](https://www.theguardian.com/books/2025/jan/31/you-must-remember-this-by-sean-wilson-review-a-beautiful-terrifying-portrait-of-dementia): chương sách bị đánh số sắp xếp lộn xộn nè, hứa hẹn sẽ là một trải nghiệm thú vị về cuốn sách
- [Phim Asura - Netflix](https://www.theguardian.com/tv-and-radio/2025/jan/29/asura-netflix-drama-hirokazu-kore-eda): ảnh nữ diễn viên xinh, với bối cảnh phim kể về sự nỗ lực của phụ nữ với những giới hạn đặt ra ở thời đại đó
- [Review phim No More Bets](https://www.theguardian.com/film/2023/sep/04/no-more-bets-review-ao-shen-chinese-thriller): phim No More Bets lại bị so sánh với phim The Wolf of Wall Street (?)

