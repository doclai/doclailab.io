+++
image = "cross_indochina_03.jpg"
date = "2025-01-04"
title = "Đạp xe xuyên Đông Dương - Phần 3"
type = "gallery"
+++

## Ngày 12

Trưa gặp và nghe anh chủ quán phở tâm sự đôi điều về kinh doanh tới tình yêu hôn nhân. Quán anh mới mở 4 tháng, nằm ở gần ngân hàng trong trung tâm Thakhek.

Đền thờ + tượng Naga ở Thakhek. Phía sau đền thờ là con sông Mekong ranh giới tự nhiên giữa hai quốc gia. Bên này là đất Lào và bên kia sông là đất Thái Lan.

## Ngày 13

Ban đầu là mình dự định hôm nay đạp xuôi phía Nam luôn, cơ mà 2h sáng đúng lúc dorm có thêm khách vào, tỉnh giấc, lên ngay combo nôn mửa với tào tháo đuổi hơn tiếng rưỡi ra hết được bữa tối. Thakhek lại giữ chân mình thêm 1 ngày.

May mắn là gặp combo ở trung tâm thị xã nên là có đủ hỗ trợ. Cơ mà hiệu thuốc bình thường mở cửa muộn lắm. Được người dân ở đó họ hỗ trợ chở ra cổng bệnh viện có nhà thuốc mở sớm.
Dự là ngày thứ 15 mình sẽ quay lại hành trình.

Do là sự cố không biết trước về sức khoẻ, nên là mình đã phải thực hiện hai việc: ở lại Thakhek, và đặt chỗ nghỉ mới tại đây. Bên này không có quán cạo gió giác hơi, nên mình có ghé qua một quán massage ở đây. Massage xong thấy cũng hồn xác gắn kết lại khá nhiều. Tới ngày thứ 15 chắc mới lên đường.

Ảnh mình chụp một trong số những người quản lý của nhà nghỉ cũ. Họ là một gia đình nhỏ. Người bố rất thích xem bóng đá, và hay nhắc mình về hai trận giữa Việt Nam và Thái Lan. Cậu em con trai này đang du học ở Huế, nói tiếng Huế ổn lắm cơ, dễ mà khiến mình nhầm là người Việt Nam. Mọi người rất thân thiện.

Chị chủ ở nhà nghỉ mới thì có bà ngoại là người Việt, nên là cũng dùng được tiếng Việt giọng miền Nam ổn lắm.

Một tuần của mình đã khép lại như vậy đấy.

