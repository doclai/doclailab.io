---
title: "Độ nhạy cảm từ chối. Hiện diện ở thực tại. Người ăn chay với trang trại bò. Câu chuyện Columbia."
date: 2023-09-18T06:05:11+07:00
tags: [daily, rejection, sensitivity, present, columbia, vegan, assault]
draft: false
---

Một thảo luận về [hiện diện ở thực tại](https://www.psychologytoday.com/intl/blog/anxiety-in-high-achievers/202309/being-present-takes-practice), các cách thực hành để trở nên tỉnh thức ở hiện tại hơn có thể kể tới như: 
- thường xuyên kiểm tra chú ý tới các cảm nhận trên thân, theo dõi hơi thở 
- thực hành tỉnh thức: chẳng hạn việc thực hành thiền tập.
- những khoảng sống chậm và tận hưởng trong ngày: đi những cung đường lâu hơn xíu nhưng được ngắm cảnh, ăn một bữa chậm rãi hơn, dành thời gian trò chuyện với đồng nghiệp.
- ngắt kết nối với công nghệ: khi điện thoại thông minh với sự sẵn có "một chạm", ta có xu hướng sẵn sàng tách khỏi thực tại hơn.
- dành thời gian với thiên nhiên.

Một thảo luận khác về [độ nhạy cảm từ chối](https://www.psychologytoday.com/intl/blog/meet-catch-and-keep/202309/the-10-hidden-relationship-costs-of-being-too-sensitive), thì chính những người nhạy cảm về chuyện từ chối tìm kiếm các chỉ báo cho chuyện đổ vỡ mối quan hệ tạo ra các bối cảnh để chuyện đổ vỡ đó dễ xảy ra hơn, từ việc thể hiện sự lo lắng, diễn giải các hành vi của đối phương theo xu hướng đổ vỡ, cho tới chuyện thể hiện các xung hấn hay thái độ thù địch. Trong nghiên cứu của Mishra & Allen (2023), ngoài các biểu hiện dương, thì các biểu hiện âm liên hệ với độ nhạy cảm từ chối có thể kể tới như tự-im-lặng (self-silencing), hay cảm thấy ít năng lượng hơn. Một gợi ý để can thiệp giảm bớt xu hướng nhạy cảm từ chối này là tạo nhiều khoảng lặng trong mối nối giữa góc nhìn với sự phản ứng, ở những người tự ý thức được vấn đề này của bản thân. Một cách khác, trong khi cảm thấy sự lo lắng bị từ chối xuất hiện, hiện diện ở đó và làm chậm chuyện phản ứng lại (có thể nhờ hỗ trợ của thảo luận hiện diện ở thực tại nêu trên), là một cách hữu hiệu để cải thiện mối quan hệ với những người có đặc điểm tính cách này.

Một câu chuyện khác ở tỉnh Derbyshire tại Anh Quốc tái hiện trong [bộ phim tài liệu ngắn 73 Cows (2018)](https://vimeo.com/293352305), sự khó khăn mang tính đạo đức trong việc kế thừa di sản trang trại bò của Jay Wilde, một người ăn chay, đã đưa họ tới quyết định thay đổi hoàn toàn mô hình trang trại bất chấp sự dè bỉu và tấn công của những người xung quanh. Câu chuyện của người trong cuộc chia sẻ: với hy vọng "ai đó ở ngoài kia có thể thấy trân trọng những gì họ đang làm". Như chia sẻ đầu của bộ phim tài liệu, qua chia sẻ về người cha với một cái "bóng" của "một kỹ sư ở Roll-Royce", người cha đã mất có tầm ảnh hưởng tới những quyết định của Jay Wilde từ nhỏ, khiến cho việc lựa chọn thay đổi này khó khăn tới chừng nào.

Trong khi đó, tại Việt Nam, nhiều vụ cháy diễn ra gần đây ở những khu chung cư. Những câu chuyện xoay quanh vụ cháy ở phố Khương Hạ vẫn đang được xã hội bàn luận. Sài Gòn ngập sau mưa lớn. Số ca mắc sốt xuất huyết ở Hà Nội tăng gấp đôi trong tuần vừa qua.

Gần đây lại có một thảo luận về một trường hợp bị tố cáo sàm sỡ khi chụp X-quang ở Việt Nam, trong khi bên tố cáo liên tục yêu cầu việc bồi thường tài chính, cuối cùng khi đưa ra làm việc cùng với phía công an thì phía tố cáo không hề có xuất hiện. Tiện chuyện nhà y, có câu chuyện trong lịch sử bang Columbia, Hoa Kỳ, bàn về các vụ tấn công tình dục của bác sĩ khoa sản Robert Hadden với những chi tiết cụ thể của những nhân chứng xin được đọc tại [đây](https://www.propublica.org/article/columbia-obgyn-sexually-assaulted-patients-for-20-years?src=longreads).

