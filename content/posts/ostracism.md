---
layout: post
title: Tẩy chay (ostracism)
date: 2022-07-11T09:28:55+07:00
tags: [tlud]
---

![](../015s.vi.jpg)
&copy; Siloto

"Bố tôi từng giữ im lặng với tôi mỗi khi ông ấy khó chịu với tôi từ lúc tôi còn 12 tuổi. Giờ tôi đã 40 và chúng tôi đã không nói chuyện trong suốt 6 tháng qua. Gần đây, ông đã ở trong bệnh viện và tôi được thông báo ông có thể mất. Tôi đã quyết định phải gặp ông, kể cả khi chúng tôi không nói chuyện. Tôi bước đến ông, nắm lấy tay và nói "Ôi bố, đừng rời con mà đi". Ông nhìn tôi, nước mắt lưng tròng, rồi quay mặt đi. Ông vẫn không nói chuyện với tôi... Sự ra đi của ông có lẽ là sự im lặng cuối cùng" - Trích từ một cuộc phỏng vấn có cấu trúc với một phụ nữ 40 tuổi, diễn ra vào 12/03/1998.

Khi đa phần những gì chúng ta biết được về khái niệm tẩy chay (ostracism) trên phương tiện truyền thông Việt như một khái niệm mang tính xã hội, thực tế việc thực hiện hành vi loại trừ, hay lờ đi này có thể xuất hiện ở giữa các cá nhân, hoặc/và các nhóm. Tẩy chay có hình thức thể hiện đa dạng từ việc xóa bỏ hoàn toàn một cá nhân khỏi một cộng đồng, tới những biểu hiện tinh tế hơn của việc thể hiện sự không chú ý (ví dụ như giảm giao tiếp bằng mắt hay không phản hồi bằng lời nói). 

Sự tẩy chay xuất hiện ở nhiều loài. Nhiều các hình thức tẩy chay khác nhau đã được ghi nhận ở các động vật linh trưởng, bao gồm cả việc tẩy chay các thành viên thể hiện bệnh tật, ốm đau, các thành viên biểu đạt các hành vi bất thường, các thành viên thất bại trong việc lấy vị trí lãnh đạo. Hành vi này có thể có lợi trong nhiều nhóm động vật khác nhau vì nó giúp giảm nhu cầu về tài nguyên khan hiếm, và giảm nguy cơ cận huyết. Tuy nhiên, sự loại bỏ khỏi nhóm, cũng như là sự bao bọc của các thành viên khác, thường là bước đầu tiên dẫn tới sự đói khát và cái chết cho thành viên bị tẩy chay (Gooodall, 1986).

Sự tẩy chay xuất hiện từ rất sớm với trẻ em (trong nghiên cứu Barner-Barry 1986, trẻ mầm non biết cách tẩy chay một cách có hệ thống với kẻ bắt nạt trong nhóm mà không có sự thúc đẩy của người lớn) đưa tới nhận định rằng sử dụng sự tẩy chay hiệu quả này, chỉ ra rằng cách kiểm soát hành vi của người khác thông qua cách loại trừ, là vừa bẩm sinh và được học. Khi chúng ta lớn hơn, chúng ta tham gia nhiều hơn ở cả hai vị trí mục tiêu và nguồn của sự tẩy chay. Đặc biệt khi chúng ta già đi, chúng ta ngày càng bị loại bỏ khỏi nhiều mặt của cuộc sống. Khi chúng ta bước tới giai đoạn nghỉ hưu, chúng ta bước dần ra khỏi lực lượng lao động, hay khi chúng ta vào trại dưỡng lão, gia đình cũng ít gặp chúng ta, và chúng ta bị loại bỏ dần ra khỏi việc tương tác với một xã hội lớn hơn. Trong một khảo sát về người già (Madey và Williams 1999), những người trải qua sự tẩy chay cao hơn trong công việc, gia đình, và xã hội, thể hiện mức độ hài lòng với cuộc sống của họ thấp hơn.

Buổi tối này chúng ta sẽ cùng chia sẻ các góc nhìn khác nhau cũng như các trải nghiệm cá nhân về sự tẩy chay, cũng như là phân tích các ảnh hưởng của nó tới đời sống tâm lý và xã hội.

Tham khảo: _Kipling D. Williams, Lisa Zadro. Ostracism: On Being Ignored, Excluded, and Rejected_.