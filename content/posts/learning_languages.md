---
title: "Viết để học ngôn ngữ"
date: 2022-08-05T08:58:41+07:00
tags: [learning, languages]
toc: "false"
draft: false
---

Nếu bạn là một người thích viết, thì một cách rất người để duy trì các ngôn ngữ đã từng có trải nghiệm là đưa chúng vào cùng với việc viết lách. Hãy viết một blog đa ngôn ngữ. Từ lúc chuyển sang Hugo, mình lục thấy có tính năng `i18n`, dùng để sắp xếp cấu trúc một trang web khi tác giả có nhu cầu phục vụ độc giả với nhiều ngôn ngữ hơn.   

Có một điểm mạnh nữa khi dùng Hugo là tốc độ xuất trang tĩnh siêu nhanh của nó cực kỳ hữu ích khi có một số lượng lớn các bài viết. Điều này rất dễ xảy ra khi bạn viết một blog với đa ngôn ngữ, số lượng bài của bạn sẽ tăng rất nhanh, do được nhân lên cùng với số lượng ngôn ngữ mà bạn duy trì.

Tuy nhiên trên thực tế thì nhiều khi không phải lúc nào duy trì một trang blog đa ngôn ngữ cũng dễ dàng, nhất là ngay khi việc viết lách đã là một sự khó khăn.
