+++
title = "Đời sống tàu điện ngầm New York"
date = "2025-01-29"
+++

Nhân chuyện nghe tin Hoa Kỳ bắt đầu rục rịch khởi động các dự án phát triển đường sắt, trong bối cảnh Hoa Kỳ với một nền kinh tế hàng đầu thế giới thì mức độ phát triển của đường sắt lại cực kỳ mờ nhạt so với các khu vực như châu Âu, hay không nói đâu xa, người hàng xóm Trung Quốc đang đứng đầu thế giới về công nghệ này. Lười chưa có dịp tìm hiểu về chuyện đường sắt, thì nay có dịp nghe một [tâm sự](https://www.newyorker.com/news/our-columnists/my-life-on-the-subway) về đời sống gắn với tàu điện ngầm từ một công dân ở New York. 

Đi qua những dấu mốc trong kỷ niệm 25 năm với tàu điện ngầm New York, loáng thoáng qua những đoạn đầu, tôi vẫn cảm thấy khoảng cách rất lớn với dòng chia sẻ của tác giả "nếu hệ thống tàu điện ngầm sụp đổ, thành phố sẽ ra đi cùng với nó" (if the subway collapses, the city will go with it). Tôi bắt đầu nối lại chia sẻ của tác giả, đầu tiên về nghiên cứu tâm thần từ năm 1992 về bản dạng và tư duy của những người đẩy những người khác xuống đường ray, rồi chuyện gian lận doanh thu tàu điện từ những năm 90, tiếp nối với 5 sự cố lớn trong 2 năm gần đây đã đẩy mối quan hệ giữa người dân và chính quyền tới mức độ giống như "song đề tù nhân" (prisoner's dilemma). Tên gọi này, khi tôi lục lại, có thể gọi như "hàn gắn trong một tình huống buông tay là dễ dàng hơn". Góc nhìn của tác giả với tôi trở nên rõ ràng, và câu chuyện về xung đột được tóm lại bởi một chia sẻ:

> Tàu điện ngầm, Lieber vẫn nói, là một mô hình thu nhỏ của thành phố. Những gì xảy ra bên dưới chỉ là sự phản ánh của những gì xảy ra ở trên. Nếu chúng ta đạt được tiến bộ về nhà ở giá rẻ và dịch vụ sức khỏe tâm thần, tàu điện ngầm sẽ được cải thiện. (The subway, Lieber keeps saying, is a microcosm of the city. What happens below is merely a reflection of what happens above. If we make progress on affordable housing and mental-health services, the subways will improve.)
