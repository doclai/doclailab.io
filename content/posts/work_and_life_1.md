---
layout: post
title: Công việc và đời sống (1)
date: 2022-06-23T09:28:55+07:00
tags: [work, career]
---

Trò chuyện với một người bạn, tôi nhận ra về sự khác biệt trong suy nghĩ của chúng tôi về lựa chọn công việc. Cô ấy mang trong bản thân một năng lượng hừng hực cháy, những mong ước với startup. Có vẻ như tôi từng có một thời gian như vậy, nhưng rồi tôi nhận ra, cho dù cùng mơ về startup, động cơ trước đây của tôi khác với cô ấy. Trước đây tôi nghĩ về ý tưởng kinh doanh, sản phẩm nổi bật khác lạ, còn giờ cô ấy tìm môi trường năng động và các kết nối mới. Cũng cùng một câu chuyện đấy, tôi kể với một người bạn khác thì thấy một điểm chung trong quá khứ khác. Chúng tôi cũng nghĩ tới chuyện kiếm việc chỉ để yên ổn tài chính rồi đi đây đó vòng quanh thế giới, nhưng câu chuyện của cô ấy rẽ hướng. Cô ấy muốn cống hiến nhiều hơn cho cuộc đời. Bất chợt nhờ các sự khác biệt này, tôi có dịp lần mò đọc về vị trí công việc với đời sống.

Lần này tôi cho rằng tôi viết theo một cách khác. Khi bắt đầu đọc các tài liệu, tôi thấy những thói quen cũ từ việc chắp nhặt câu chữ khi thấy một số ý tưởng không được địa phương hóa (một bài viết về sự không mạch lạc tôi từng viết vào năm 2020) [^5]. Thấy được các ý tưởng được viết ra sống động trong đời sống là một điều quan trọng. Viết đi viết lại một bản nháp như một cách đẽo gọt các câu chữ và ở lại lâu hơn với ý tưởng.

Qua một bài viết [^1], lợi ích công việc có thể mang tới là: cung cấp các mối quan hệ bạn bè; tạo cảm giác ổn định khi cuộc đời bất trắc; cung cấp các thử thách trí tuệ, giữ bản dạng và giá trị bản thân tích cực; cung cấp tài chính để làm những thứ bạn thích; giúp bạn hiểu về thế giới, về những người khác, về bản thân bạn; cống hiến cho cộng đồng. Những ý kiến khác, hoặc phớt lờ những lợi ích nêu trên, hoặc nhắm nhiều hơn về câu chuyện đơn thuần tài chính, đưa tới một quan điểm tách riêng sự phát triển đời sống cá nhân và đời sống làm việc. Hiện tượng này thực tế chính là cơ chế phòng vệ chia thành ngăn (compartmentalization) [^2], liên hệ với bất hòa nhận thức (cognitive dissonance) mà tôi đã viết tháng trước [^3]. Plato từng bàn về chuyện làm việc dưới góc độ triết học, như là một cách hiểu về thế giới, sự thuần thục, lão luyện về một số phần của thực tại, như cách mà thợ mộc hiểu về gỗ đưa ông đấy khả năng sử dụng gỗ trong nhiều hoàn cảnh khác nhau [^4]. Làm việc định hình tính cách của chúng ta. Ý tương quan giữa bản thân và môi trường xung quanh này cũng xuất hiện ở một ý “clean your room” của J. Peterson trong bài giảng chủ nghĩa hiện sinh trong khóa học về tâm lý học tính cách. Việc điều trị cho các thân chủ của ông nhiều khi khởi đầu bằng việc dọn phòng: căn phòng có phải chỉ đơn thuần là căn phòng hay là một biểu hiện khác của tâm trí bản thân ra ngoài đời thực? Đời sống vốn tự thân đầy rẫy sự đau khổ rồi, việc chúng ta là bớt việc phải chịu đựng những đau khổ do sự ngu ngốc của bản thân gây ra. Và chúng ta bắt đầu sửa chữa những thứ nằm trong khả năng của ta mà đời sống đã và đang gợi mở.

Tham khảo:

[^1]: [What Psychological Benefits Do You Get From Work? - PsychologyToday](https://www.psychologytoday.com/us/blog/in-practice/201908/what-psychological-benefits-do-you-get-work)
[^2]: [Why Work Is More Than Just a Job - PsychologyToday](https://www.psychologytoday.com/us/blog/the-new-employee-experience/202110/why-work-is-more-just-job)
[^3]: [Từ bất hòa nhận thức, đi tìm mục đích cuộc đời - Sonarypt Sona viết](../psy/cognitive_dissonance)
[^4]: [Philosophies of Work: Ideas From the Platonic Tradition - PsychologyToday](https://www.psychologytoday.com/sg/blog/human-flourishing/202205/philosophies-work-ideas-the-platonic-tradition)
[^5]: [Sự không mạch lạc (2) - Sonarypt Sona viết](../books/david_bohm_02)