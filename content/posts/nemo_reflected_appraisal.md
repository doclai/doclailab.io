---
layout: post
title: Từ Nemo tới the reflected appraisal process
date: 2022-06-23T09:28:55+07:00
tags: [tlud]
---

Yêu thương bản thân: Từ Nemo tới The reflected appraisal process

![](../013s.jpg)

Những cô bé, cậu bé của thời “Đi tìm Nemo” ngày nào giờ đã trở thành những sĩ tử đại học. Cách đây không lâu khi xã hội nổi lên chuyện sự bất mãn đã để người con đi quá xa tới khi cha mẹ chẳng thể bao giờ tìm lại được, thì gần đây cộng đồng mạng xôn xao về hình ảnh người mẹ mang theo một bó hoa đặc biệt khi đính toàn ảnh idol K-Pop - thần tượng của con kèm một tấm biển ghi lời nhắn: "Bất kể con thi đạt kết quả thế nào, bố mẹ đều yêu con". Từ bộ phim tới đời thực, rộng ra, chúng đều nhắc chúng ta tới một quá trình về cách mà chúng ta nhìn nhận về việc người khác nhìn về chúng ta, và là cách chúng ta tự nhìn về bản thân (the reflected appraisal process).