---
title: "Câu chuyện mục đích ở giảng đường đại học"
date: 2023-04-20T12:57:35+07:00
tags: ["purpose"]
toc: "false"
draft: false
---

Đêm hôm kia trước cày nốt dự án lập trình đến những phút cuối cùng. Tôi vẫn thản nhiên trả lời nhóm bạn rằng "tao chưa xong đâu, tối nay rồi tao sẽ làm nốt". Dự án được giao cho trong 1 tháng để hoàn thành, nhóm đã hoàn thành gần hết khung xương chính của dự án ngay ở tuần đầu tiên, nhưng thời gian vài tuần tiếp theo đấy nhóm không có động vào code của dự án này. Ngay trước hôm thuyết trình, trên đường về nhà đã nghĩ thời lượng để tôi hoàn thành tính bằng giờ, một cảm giác hồi hộp chờ đợi tôi trước khi chạm vào bàn phím để viết như hồi tôi còn làm sản phẩm cho một khách ở US. Một buổi tối như hackathon để hoàn thiện các tính năng còn lại trước buổi thuyết trình ngày mai. Sáng hôm sau tôi hẹn cả nhóm đến thật sớm để thảo luận và kiểm tra một số khâu vận hành. Vài phút trước khi bước lên thuyết trình tôi vẫn còn hồ hởi tìm ra bug để fix. Vui là mọi thứ cứ như lần đầu tôi có cảm tình với việc lập trình.

Nhân dịp một buổi học, giáo viên nói về trải nghiệm, rằng một thứ trong việc học ở trường có quan trọng hơn nhiều điểm số kết quả. Điều đó tôi cảm thấy được qua việc tôi lựa chọn dành thời gian của tôi như thế nào với những gì mà nhiều người khác cho rằng là nhàm chán.
