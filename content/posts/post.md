---
layout: post
title: Chảo rán, máy cổ và tuổi thơ
date: 2022-11-22T09:28:55+07:00
tags: [childhood]
---

Cái chảo rán không san đều nước dùng nổi bong bóng lúc làm mình liên tưởng tới sự hưởng ứng. Cái nhiệt có sinh ra nhưng mỗi chỗ của cái chảo nó lại thể hiện một cách khác nhau.

Cảm giác hoài cổ trong góc nhà bên cạnh cái máy tính tuổi đời gần thập kỷ viết lên những dòng này. Sự tương phản trong cái mùa đông Hà Nội khi ngoài cánh cửa, tiếng bước chân thình thịch đều đều ngoài cầu thang, còn đây, mình chỉ còn nghe tiếng quạt máy tính thỉnh thoảng phe phẩy do hệ điều hành chạy siêu nhẹ đã được tỉ mỉ tối ưu cho cân bằng giữa cái sự cổ kính cùng với sự thoải mái trong sử dụng hàng ngày. 

Dạo qua một câu hỏi của Reddit: kỹ năng gì bạn có hồi còn là một đứa trẻ nhưng đánh mất khi là người lớn? Có những bình luận nêu lên kỹ năng kết bạn, kỹ năng ngủ trên sofa nhưng dậy ở trên giường, kỹ năng trở nên cực kỳ năng suất trong lúc cực kỳ không năng suất (ví dụ về việc hồi nhỏ họ từng chơi trong giờ, bị giáo viên gọi và vẫn trả lời được câu hỏi của giáo viên khiến giáo viên bối rối) hay kỹ năng giả giọng người thân trong gia đình.
