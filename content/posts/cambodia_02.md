+++
title = "Campuchia lần 2"
date = "2025-01-20"
+++

Hết hành trình tôi bắt đầu bị lười. Vội nhắn tạm biệt chú người Đài Loan cùng phòng và tôi lên đường. Với tôi thì ký ức với Campuchia là con người, văn hoá, thiên nhiên, và đồ ăn, nhưng do là đã hẹn mọi người có quà của Campuchia, và hành trình đạp xe vừa qua của tôi không có ghé qua các thành phố lớn của Campuchia, nên là sau khi về Sài Gòn vài hôm gặp gỡ nhóm của tôi thì lên đường tiếp lại Phnom Penh qua đêm để kiếm vài đồ thú vị cho mọi người. Sáng nay tôi đã đặt phòng và kịp nhắn tin check thông tin với quản lý của cửa hàng lưu niệm ở gần đó. Cửa hàng của họ là một trong những cửa hàng ít ỏi mở muộn.

Mọi thứ lại cập rập trong buổi sáng từ việc dọn đồ checkout, rút tiền mặt khi thẻ bị khoá (sau đó tôi mới dùng tới tính năng rút qua mã QR thay vì phải vào quầy), tới việc kiếm chỗ gửi xe qua đêm gần nhà xe khách. Rồi thì tôi tới nhà xe sát giờ, hơi hụt hẫng, thì có chú xe ôm của nhà xe chở tôi ra bắt kịp xe ở điểm đón tiếp theo. Ngồi trên xe, tôi lần lượt trả lời lại những tin nhắn nhủ ấm áp của chú cho chuyến đi bình an.

Đi trên những con phố tại Phnom Penh, thì tôi thấy sự tương phản giàu nghèo một cách rõ rệt. Có thể kể ra khi tôi bước ra từ cửa hàng đồ lưu niệm thì có một bé níu áo tôi trỏ trỏ vào cửa hàng đồ lưu niệm rồi chắp hai tay cầu xin tôi. Không như những người trên đường khác, mái tóc của em không hề gọn gàng, điều tương tự cũng xuất hiện trên quần áo của em. Những em đấy cũng xuất hiện ở cửa ra vào của một siêu thị Big C mini gần đó khi tôi kiếm đồ uống cho bữa tối. Cảm giác tương phản một cách lạc lõng này tôi không thấy nó xuất hiện ở Sài Gòn, nơi mà tôi mới chỉ có đôi lần qua lại. Nhân dịp có được trải nghiệm như này khiến tôi có dịp để đọc những ý tưởng của Edward Glaeser.

Nửa đêm 20, rạng sáng 21, tỉnh giấc rón rén đi vệ sinh, rồi sau đó lướt Facebook thì ở bên kia bán cầu, Donald Trump đang nhậm chức tổng thống Hoa Kỳ thứ 47 và có những chia sẻ trước công chúng.

