---
layout: post
title: "Kết nối hai bán cầu não"
date: 2024-07-01T02:56:49+07:00
tags: [psychology, neuroscience, philosophy]
draft: false
---

Bắt đầu với [một thảo luận thú vị](https://www.facebook.com/nguyenleanh2007/posts/10230412983005696) của một giảng viên với chuyện đưa giải Nobel 1981 của Roger Wolcott Sperry ra làm lý do để phủ định sự tồn tại của thế giới tâm linh, khẳng định ý thức của con người có tính cá thể độc lập thay vì có sự tham gia của những thực thể khác. 

## Bài viết gốc

Khái niệm Linh Hồn. 

Linh hồn, hay ma, hay thần thức đều là các khái niệm có bắt đầu là ao ước về cuộc sống sau cái chết. Chúng được hiểu là các đối tượng tạo ra thế giới tâm linh. Một trong số các câu hỏi đặt ra, khái niệm về thế giới tâm linh xuất hiện thế nào, vì sao chúng ta có khái niệm về những thứ không có thực, chứng minh tâm linh không tồn tại thế nào?

Chúng ta bắt đầu từ những con vi khuẩn, vi trùng. Ngày nay chúng ta nhìn thấy chúng nhờ kính hiển vi điện tử phóng đại lên nhiều nghìn lần. Tuy nhiên trước đây khi con người còn sống thành bộ lạc họ chưa thể biết được vi khuẩn, vi trùng, nhưng họ biết được sự kiện khi ở gần các xác chết sẽ bị (lây nhiễm bệnh mà) chết theo. Như thế tất cả các bộ lạc đều có nhận thức về sự khác biệt giữa người còn sống và người đã chết. Tình mẫu tử níu kéo người thân, không muốn rời xa. Đấy là nguyên nhân dẫn loài người tới khái niệm cuộc sống sau cái chết, mà chúng ta vẫn gọi là linh hồn. 

Theo như định nghĩa thì không thể nhận thấy bất luận một liên hệ gì giữa linh hồn với người sống, bởi nếu có thì họ đã sống. Mọi liên tưởng đều được thực hiện dựa trên sự hình dung của những người   thân. Người ta cho rằng các linh hồn vẫn sống và sống theo quy tắc "trần sao âm vậy". Người ta xây dựng cho linh hồn các cấu trúc như vua chúa và coi cái thế giới tâm linh được điều hành bởi một vị hoặc một hệ thống các vị quan chức rất lớn là Ngọc Hoàng, hay các Bồ Tát. Dần dần sự kiện nhân sinh quan ấy chuyển hóa thành thế giới quan, theo đó người ta coi con người gồm hai phần, phần thể xác và linh hồn. Phần linh hồn tồn tại vĩnh viễn và nhập vào thể xác tạo ra sự sống. Sau khi chết thì linh hồn thoát ra khỏi thể xác mà bay về trời hay bị đầy xuống địa ngục. Chế độ chiếm hữu nô lệ sử dụng cấu trúc thể xác và linh hồn ấy để duy trì sự thống trị. Con người ngay từ khi còn nhỏ đã bị nhồi nhét khái niệm linh hồn, kiếp số, địa ngục, thiên đàng... Đấy là các phạm trù được dùng để kiến tạo ra sự thống trị dựa trên khái niệm tâm linh.
Sự giao tiếp giữa thế giới vật chất có thật với thế giới tâm linh được cho là thực hiện thông qua các nhà sư hướng dẫn. Cúng dường cho nhà sư được cho là để thỏa mãn yêu cầu của vong (tức linh hồn của ai đấy), nếu không thì vong sẽ bắt (tức bị chết). Cúng dường cho nhà sư được coi là phước lành, cúng càng nhiều thì phước càng lớn, tức cơ hội thăng tiến càng lớn. Những lập luận hoang tưởng này đã và đang thống trị nhận thức của con người trong xã hội Việt Nam nhiều nghìn năm qua. Mọi sự chống đối Phật giáo đều bị quy cho là tạo nghiệp ác, và phải xám hối và cúng. 

Như chúng ta đã hiểu, Phật giáo sử dụng các thủ pháp vô thức, tức nhồi nhét vào đầu trẻ em khi chúng chưa có nhận thức về các phạm trù như ma quỷ, số kiếp, linh hồn... Vào chùa các bạn có thể nhận ra đầy dãy các hình tượng dọa nạt về sự bị đày đọa sau khi chết. Các ban cũng nhìn thấy các tượng Thích ca màu Ni (vua của cõi tâm linh) ngồi đè đít lên đầu vua. Những tượng thế này làm xuất hiện trong lòng các tín đồ nhận thức về quyền lực của Phật giáo là mạnh mẽ hơn các vua chúa, tức mạnh hơn các lãnh đạo cấp cao như chủ tịch nước hay thủ tướng chính phủ. Phật giáo nhanh chóng chiếm đoạt nhân cách của những người nổi tiếng. Ví dụ như việc khẳng định Einstein là tín đồ phật giáo. Ashoka cũng chịu chung số phận bị Phật giáo chiếm đoạt nhân cách. Ngày nay Quang sư, một kẻ tâm thần hoang tưởng, tự cho mình là cùng có máu mủ họ hàng với Hồ Chủ Tịch. Quang sư đang nhét vào mồm Hồ Chủ Tịch những thứ như "Hồ Chủ Tịch phát biểu ra tiên đề tâm linh".

Hiện Quang sư được trường đại học Luật Việt Nam cho bảo vệ thành công luận án và công nhận là tiến sĩ Luật. Trường đại học Luật Việt Nam -- cơ quan có chức năng kiến tạo ra nền công hòa -- đang bị tâm thần nặng.  

Mấu chốt của tôn giáo là khái niệm về thế giới tâm linh, trong đó thể xác con người được linh hồn (khả năng tư duy, khả năng tạo ra ý thức) nhập vào. Như thế linh hồn là chủ của thể xác, nó vận hành (như chúng ta lái chiếc xe) thể xác theo ý muốn của nó dưới dạng nhận thức và hành động. Vào năm 1981 Roger Wolcott Sperry đã nhận được giải thưởng Nobel trong thí nghiệm về nhận thức. Thí nghiệm cho thấy nhận thức không phải là một thứ trọn vẹn theo linh hồn mà là do cấu trúc của thể xác tạo ra. 

Roger nối dây thần kinh vận động ở chân chuột nên dây thần kinh bên trái điều khiển chân phải và ngược lại. Sau đó, Roger đặt những con chuột vào một chiếc lồng bên dưới có lưới điện. Lưới được chia thành 4 phần phân tách nhau và có điện. Mỗi chân của con chuột được đặt vào ở một trong bốn phần của lưới điện. Các cú sốc điện được thực hiện đối với các chân của chuột. Mỗi lần bị điện giật vào chân trái chuột sẽ nhấc chân phải lên và ngược lại. 

Sau nhiều lần kiểm tra, Roger phát hiện ra những con chuột không bao giờ học được cách nhấc đúng bàn chân lên. Điều này dẫn đến kết luận rằng "Không thể có một thứ linh hồn nào điều khiển thân thể". Tất cả phụ thuộc vào cấu trúc vật chất của cơ thể. Như thế ý thức của chúng ta không phải là hồn thứ từ thế giới tâm linh nhập vào tạo ra mà là sản phẩn của cấu trúc vật chất của bản thân cơ thể. 

Giải thưởng Nobel được trao cho các thí nghiệm của Roger thực hiện trên người. 

Não được chia thành hai bán cầu, bán cầu não trái và phải, được kết nối ở giữa bởi một phần não gọi là thể chai. Căn bệnh động kinh gây ra các cơn co giật dữ dội và dai dẳng. Các cơn co giật bắt đầu ở một bán cầu não và tiếp tục lan sang bán cầu não còn lại. Việc cắt thể chai giúp ngăn chặn cơn động kinh di chuyển từ bán cầu não này sang bán cầu não kia, từ đó ngăn ngừa cơn động kinh xảy ra, do đó cho phép bệnh nhân hoạt động bình thường thay vì phải chịu đựng những cơn động kinh liên tục.

Ở những bệnh nhân thể chai đã bị cắt đứt thì "não bị chia đôi". 

Ông yêu cầu bệnh nhân "não bị chia đôi" đặt tay trái của họ vào một khay đầy các đồ vật nằm dưới một vách ngăn để bệnh nhân không thể nhìn thấy các đồ vật đó. Giữa mắt phải và mắt trái có vách ngăn để chúng không đồng thời nhìn thấy cùng một vật.

Sau đó, một từ được hiển thị ra cho mắt bên phải đọc, thông tin được xử lý bởi bán cầu não phải. Từ này mô tả một trong những đồ vật trong khay. Não phải lệnh cho tay trái của bệnh nhân cầm đồ vật tương ứng với từ đó. 

Tuy nhiên khi được hỏi về tên gọi đồ vật trong tay trái họ không thể trả lời được. Bán cầu não phải đã nhận ra từ đó và bảo tay trái cầm nó lên, nhưng vì bán cầu não phải không thể nói và bán cầu não trái chưa nhìn thấy từ đó nên bệnh nhân không thể diễn đạt những gì họ đã nhìn thấy.

Như thế "Không thể có một thứ linh hồn nào điều khiển thân thể". Tất cả phụ thuộc vào cấu trúc vật chất của cơ thể. Như thế ý thức của chúng ta không phải là hồn, thứ từ thế giới tâm linh nhập vào tạo ra, mà là sản phẩn của cấu trúc vật chất của bản thân cơ thể. 

Hồn -- nền tảng cơ bản tạo ra tôn giáo đã không còn. 

Tài liệu dẫn

https://www.facebook.com/nguyenleanh2007/posts/10222334427366854

## Comment 1

Bài viết này từ thầy thú vị. Nó khiến em chịu khó viết vài dòng lại để phản biện xíu.

Cảm nhận đầu tiên về lập luận nửa dưới của bài từ thầy là có vẻ muốn đưa reductionism (chủ nghĩa rút gọn) với worst case vào một điểm trọng yếu của hệ thống và coi cả hệ thống chỉ là điểm trọng yếu đó. Ngoài ra thì "tâm linh" là một khái niệm chúng ta gặp khó khăn về tầm nhận thức (tức là mô tả như thế nào), chứ chúng ta đều chỉ đang loay hoay chứng minh "cách hiểu về nó" sai hơn là "phủ nhận" nó.

Cách thức lập luận reductionism với worst case này có thể nhìn vào qua một ví dụ thú vị, và có vẻ hơi nực cười sau. Ta có thể dựng lên một lập luận như này, bằng cách tạo ra thí nghiệm tưởng tượng (nhưng thực tế cũng có kết quả như vậy) cho một nhóm người sống trong điều kiện thiếu thốn vật chất, quan sát, và khẳng định rằng những người này có nhu cầu nhiều về vật chất, gần như không có nhu cầu về tinh thần, sau đó khẳng định rằng mục đích cao nhất của đời người là đạt được mục tiêu về điều kiện vật chất. Cơ mà, một người có hiểu biết cơ bản thì thấy rõ ràng là khi đủ cái ăn thì mọi người sẽ không chỉ đi tìm cái ăn nữa, và lập luận không còn đúng với những cá nhân đã đủ ăn. 

Nếu ví dụ trên chưa đạt được mức độ sâu sắc về mặt tâm sinh lý, thì ta có thể tiếp cận một ví dụ khác sâu hơn, gần hơn cho thầy phân tích thì là về những giấc mơ. Nó sẽ gần gũi hơn cho mọi người hơn là nói về "tâm linh" như một ý niệm trừu tượng.

## Reply 1

Trước hết bạn nên dùng từ ý thức thay cho tinh thần.
Lập luận của bạn không chuẩn.
Trong lập luận
"một nhóm người sống trong điều kiện thiếu thốn vật chất, quan sát, và khẳng định rằng những người này có nhu cầu nhiều về vật chất, gần như không có nhu cầu về tinh thần, sau đó khẳng định rằng mục đích cao nhất của đời người là đạt được mục tiêu về điều kiện vật chất. Cơ mà, một người có hiểu biết cơ bản thì thấy rõ ràng là khi đủ cái ăn thì mọi người sẽ không chỉ đi tìm cái ăn nữa, và lập luận không còn đúng với những cá nhân đã đủ ăn."
Bạn sai.
- Thứ nhất khẳng định "một người có hiểu biết cơ bản..." là võ đoán.
- Thứ hai vấn đề "vật chất" quyết định ý thức (tinh thần) không hàm ý cái ăn. Cái ăn chỉ là một phần. "Vật chất" quyết định ý thức là hàm ý nói tới nguyên lý tư duy đúng sai thế nào. Nếu nguyên lý tư duy phải dựa vào thực tế khách quan thì tức là vật chất quyết định ý thức.

Bạn đã hoàn toàn không hiểu toàn bộ hay chỉ một phần rất nhỏ của tút. Cái tút này không phải là reductionism. Ở đây chúng ta nói đến sự thống trị của ý thức lên vật chất là không thể. Thí nghiệm cho thấy là bạn có thể rút kinh nghiệm thế nào đi chăng nữa, tức ý thức được toàn quyền chủ động, thì vẫn không thể trả lời đúng được thí nghiệm. Cái này là do cấu trúc cơ thể đã khác.

Bạn nên đọc và hiểu cho kỹ trước khi phê phán.

## Comment 2

Em hiểu tút này không phải reductionism, mà em nói về cách dùng reductionism của thầy với giải Nobel 1981 ra làm ví dụ ấy. Em nói về lập luận nửa dưới, em không nói về nửa trên, và cũng hiểu về bối cảnh thầy đưa ra để phủ nhận sự thống trị hoàn toàn của ý thức.

Đoạn "một người có hiểu biết cơ bản" thì có thể bỏ ra, nó không ảnh hưởng đến đại ý của em. Ví dụ số 1 này là nhắm tới reductionism, không nhắm tới mối quan hệ "vật chất - ý thức".

Edited thêm: Cụ thể hơn thì khẳng định của thầy suy ra từ Nobel 1981 đang là "Không thể có một thứ linh hồn nào điều khiển thân thể" dựa vào worst case.

## Reply 2

Thí nghiệm của Roger được mô tả rất rõ ràng. Cái chính ở đây là không thể có một "người điều khiển cơ thể" tức là ý thức. Nếu ý thức có, tồn tại độc lập với cơ thể, nhưng điều khiển cơ thể, thì nó sẽ phải trả lời được vật đang nắm ở tay trái là gì. Đằng này ý thức đã không thể trả lời được vật đang nắm trong tay trái tên là gì. Như thế ý thức phụ thuộc vào cấu trúc của cơ thể, tức ý thức do vật chất quyết định.

## Comment 3

Em không tham gia vào cuộc chiến giữa "vật chất - ý thức" nên sẽ không đại diện và không thảo luận về câu chuyện "vật chất - ý thức".

Cái chúng ta đang thảo luận và phản biện nằm ở cách diễn giải kết quả về khoa học thần kinh sang câu chuyện về nhận thức. 

Em xin nhắc lại là bản thân Nobel 1981 không phải là chuyện ta phản biện ở đây, đó là câu chuyện của quá khứ, nhưng cách ta diễn giải về kết quả này.
 
Nobel 1981 nói về vùng corpus callosum và mối quan hệ các phía của não. Tuy nhiên, cách đưa corpus callosum lên nói về sự tồn tại/vận hành của "ý thức" qua một khía cạnh cấu trúc cơ thể thì cũng không chứng minh được thời điểm đấy ý thức không làm gì khác.

## Reply 3

Cậu nên đánh giá "tài năng" của tôi khi sử dụng công sức của người khác. Ông Roger làm thí nghiệm về một sự kiện, tôi dùng đúng thí nghiệm ấy cho phạm trù khác.

Cậu nghĩ là tôi chỉ có khả năng nhắc lại và copy paste thôi sao? Tôi làm khá hơn thế nhiều. Chính cậu bây giờ mới hiểu ra, cái chính làm nên tầm vóc của một con người nằm ở chỗ sử dụng được công sức của người khác.

Tôi lội ngược dòng lịch sử, tôi đâu có phán vu vơ. Tôi sử dụng công sức của hàng nghìn bài báo, họ đều là những nhà nghiên cứu rất giỏi và làm mất rất nhiều công sức và tiền bạc. Tôi đứng trên vai người khổng lồ bạn nhé.

## Comment 4

Em không công kích cá nhân ở đây đâu.

Em có ghi ở ngay comment đầu tiên, nếu thầy đọc kỹ, còn có ví dụ thứ 2 thầy có thể phân tích là về các giấc mơ.

Và hơn hết, những người khổng lồ cũng vẫn luôn có những cuộc thảo luận xuyên thế kỷ, thầy ạ.

## Reply 4

tôi dùng kết quả của Roger là rất chuẩn.

