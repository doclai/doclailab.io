---
layout: post
title: "Nghĩ về nắm bắt và biểu đạt cảm xúc của người trẻ"
date: 2022-03-17T09:28:55+07:00
tags: [tlud]
---

“Tôi có thấy những gì mình và người khác cảm?": Nghĩ về nắm bắt và biểu đạt cảm xúc của người trẻ (17/03/2022)

"Phải mất một thời gian để tôi nhận ra, một cách không nhẹ nhàng lắm, rằng tôi chẳng hiểu về hay lắng nghe được những sự thầm thì của thế giới người ấy. Tôi từng nghĩ là tôi hiểu. Xong khi tôi chịu khó viết xuống (mà hình thành được hành động viết xuống này cũng cần nhiều sự khiêm tốn và chuẩn bị), và xâu chuỗi chúng lại, mới thấy, mình không có hiểu gì cả. Toàn là tưởng tượng của bản thân. Tôi bắt đầu kể với người ấy về những gì tìm ra trong hành trình này.

Người ấy đã có một thời gian bắt chuyện nhẫn nại, chậm rãi đặt ra những câu hỏi để tôi bớt dùng sự lý tính đi, giúp tôi nói về những thứ mang tính cá nhân bên dưới. Lúc mà tôi đang đưa những thứ bên dưới đó lên, thoáng đã có ý nghĩ trốn chạy khỏi cái bối cảnh lúc đấy, người ấy đã nâng niu những thứ đó và giúp tôi ngồi lại với chúng.

Cảm ơn người ấy.

Giờ khi tôi thấy ổn hơn, tôi nghĩ, có lẽ, sẽ có những người khác, đang trên hành trình cảm xúc của bản thân, cảm thấy được chia sẻ về câu chuyện."

Trong buổi tối này, chúng ta sẽ cùng thảo luận về sự suy giảm trí thông minh cảm xúc, về sự ổn thoả và khả năng nắm bắt lẫn biểu đạt cảm xúc của người trẻ.