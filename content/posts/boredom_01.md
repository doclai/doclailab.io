---
title: "Tẻ nhạt"
date: 2023-03-19T10:45:50+07:00
tags: []
toc: "false"
draft: false
---

Bớt càng nhiều càng tốt thời gian làm văn bản văn phòng đi. Làm càng nhiều nó sẽ cản trở việc mình đối diện với câu hỏi mình cần tìm kiếm điều gì. Nhưng thì công việc nào rồi cũng sẽ tẻ nhạt. Buổi sáng đi bộ với cháu của mình, mình cũng thấy tẻ nhạt.

Một ngày tẻ nhạt như vậy. Vào siêu thị mình đã nghĩ tới việc kiếm rượu. Nhưng rồi thôi, cơ bản cũng không phải lần đầu mình rơi vào tâm trạng này. Thay đổi lựa chọn, mình mua thật nhiều bánh mỳ với bánh bao cho mọi người ở nhà. Muốn mình ăn nhiều, cả nhà cũng ăn nhiều, thì mua thật nhiều vào.

Đi gặp mọi người để host buổi trò chuyện như đã thông báo trước. Nhận ra càng về sau mình khó nói chuyện, khó diễn đạt, cảm giác muốn ngồi một mình hơn.

Trong lúc trò chuyện với mọi người, mình hình dung ra, có những mốc thời gian trong đời mình cần làm ngay khi mình có còn hứng thú, còn không thì như câu chuyện dịch chuyển, mình cũng không còn có hứng thú chu du đây đó như cách đây 5 năm nữa.

