---
layout: post
title: Trưa hè nghĩ về tính cá nhân
date: 2022-06-25T09:28:55+07:00
tags: [individuality]
---

Được một người bạn đồng niên quen đã lâu gửi một bài blog mới của cô ấy, tôi nghĩ về câu hỏi mà cô ấy đặt ra "có phải mọi người cần thành thật hơn?". Tôi chợt chìm mất vào cảm giác ngộp thở của thời giữa cấp ba, khung cửa sắt, bốn bức tường, sơ vin, sổ ghi đầu bài. Thời này là thời mà tôi bắt đầu đọc đâu đó, hoặc nhìn thấy, những bất mãn trong đời sống mình. Đời sống cá nhân của tôi từ lúc đó đã có những lựa chọn khác đa số, người ngoài nhìn thấy sự lòng vòng quanh co nhưng giờ tôi thấy tôi đang đi sâu hơn và đi tìm những câu hỏi khác. Tôi rẽ từ một hồ sơ "ổn áp" để đến những nơi cần đến, gặp những người cần gặp, tạo ra những trải nghiệm có tính chân thật nhất. Tôi nhận ra rằng, những người sống chân thật với nhau không có nghĩa họ sẽ đạt được kỳ vọng đặt ra cho mối quan hệ, hay cho kỳ vọng gì đó chung. Điều này không như cô ấy hình dung trong bài blog đấy, rằng, đại loại, cứ đưa những thứ thô ráp nhất ra và chúng ta có thể sống yên ổn với nhau.

Đi theo dòng thời gian như này, tôi mới thoát được ra cảm giác ngộp thở đó, và bắt đầu hình dung ra vì sao chúng tôi có sự quan tâm hiện tại khác nhau như vậy. Liệu cô ấy sẽ làm gì sau khi đặt ra câu hỏi đó? Điều gì khiến cô ấy xoáy sâu vào câu hỏi đó để rồi cho rằng ngôn ngữ là vấn đề của điều cô ấy đang quan tâm?

Nhân chuyện này, cùng dịp trò chuyện với một đồng môn, khi nghe mối quan tâm của anh ấy trong chuyện lập trình một dự án riêng. Anh ấy bàn tới những ý tưởng hết sức vĩ mô, về tầm quốc gia, khu vực. Sau một hồi, tôi mới hỏi anh ấy, rằng anh có ý tưởng nào là thực sự mang tính cá nhân hơn không, thì thấy ý tưởng này không được kể tới gì nhiều lắm.