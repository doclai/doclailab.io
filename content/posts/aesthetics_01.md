---
layout: post
title: Sau cái ta thấy
date: 2024-10-12T05:45:55+07:00
tags: [aesthetics]
---

Ngồi hý hoáy viết bài này vào lúc gần 6h sáng, thời điểm mình đang có các phòng chống về bức xạ điện từ, nghe nhạc Duy Mạnh, và đang tập Yoga. 

Không dám ra ngoài hiên tập yoga như mấy hôm trước trong cái thời điểm không khí ngoài trời cứ có chỉ số PM2.5 trên 170, tiện lướt Reddit bắt gặp một bài chia sẻ tiêu đề "đi du lịch một mình đã hủy hoại cuộc đời tôi như nào". Xong rồi thì đọc được từ một bình luận bên dưới rằng, có thể viết lại tiêu đề là "đi du lịch một mình hé mở nững thứ về bản thân và lựa chọn cá nhân và bằng cách nào cuộc đời tôi có thể khác đi".



