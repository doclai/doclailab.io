---
layout: post
title: không đề
date: 2017-01-20T09:28:55+07:00
tags: [poetry]
---

Người ta đã cùng bạn uống say thì không thể đưa bạn về nhà được.

Người đến trước có duyên không phận, kẻ đến sau có phận không duyên.

Chuyện đời lắt léo, chẳng thể cưỡng cầu.
