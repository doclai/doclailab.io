---
layout: post
title: Động năng
date: 2022-01-13T09:28:55+07:00
tags: [mind]
---

Những ý tưởng nhảy tới trong tâm trí, làm xao lãng tập trung vào những thứ cơ bản để sinh tồn. Lâu lắm cái cảm giác sợ trí tưởng tượng nó mới ghê gớm như này. Và những lúc này thường xuất hiện ở thời điểm chẳng chăm chút gì cho tâm hồn bản thân. Mọi thứ thật rời rạc và 

Chuyển sang blog mới, đặt lại mọi thứ cho gọn gàng.

Nhìn thấy bản thân đi tìm kiếm thứ gì đó trên mạng. Vơ vội những kết quả thu được từ một vài từ khóa mà nghĩ là nó năng suất. Nhận ra mình cần cắt đi mấy thứ đó. Mình đang tiêu thụ quá nhiều thứ. Và bắt đầu nghĩ về những suy nghĩ của mình. Một cái lối suy nghĩ quen thuộc trước đây, rằng khi đạt đến mức độ kiến tạo giá trị rồi, mình cần tiêu thụ thêm để có những thứ "sáng tạo" hơn. 

Nhớ đến tập thơ "Mùa Dã Cổ", cái lạnh tràn xuống từ cực Bắc chạm tới những thứ rác tái chế. Nhớ về sáng tinh khôi trên không trung Việt Nam, trắng trong của bìa tập thơ và cái nắm tay rời Đà Nẵng. 

Trong những sự kiện gần đây, mình được tưởng thưởng nhiều hơn bởi những lần tái chế.