---
title: "Sự kiệt sức. Tính cách dễ chịu"
date: 2023-09-17T15:45:11+07:00
tags: [daily, burnout, agreeableness]
draft: false
---

[Câu chuyện về burn-out](https://www.psychologytoday.com/intl/blog/romantically-attached/202309/what-does-burnout-feel-like) chia sẻ về mối tương quan giữa công việc và sức khỏe tinh thần, nhắc nhở về chuyện rằng phần lớn thời gian cuộc sống ta dành cho công việc, hay nói cách khác là _phần lớn của bản thân ta_ là công việc, nên đừng chỉ nhìn và cho là đó chỉ là chuyện kiếm tiền và tránh những va chạm sâu thẳm về giá trị. Đây còn nhắc tới câu chuyện tổng quát hơn: bạn quan sát thấy điều gì khi bản thân làm những việc mà không khớp với giá trị cốt lõi của mình?

Ở một thảo luận khác không liên quan về burn out, [một thảo luận](https://www.psychologytoday.com/intl/blog/fulfillment-at-any-age/202309/why-its-so-easy-to-trust-a-nice-person) về tính cách liên nhân nào ảnh hưởng tới chuyện đáng tin cậy của một người. Nghiên cứu chỉ ra hai yếu tố: một trong số đó là tính cách dễ chịu (agreeableness: kiểu tính cách có khả năng đặt như cầu của người khác nên trước nhu cầu bản thân) được coi như là người đáng tin cậy. Một tính cách khác là tính cách hướng ngoại (extraversion) được đề cập tới nhưng không ổn định như là yếu tố agreeableness. Năm 2023, Stavrova và cộng sự thử các bài test, ba bài test đầu là dựng môi trường có sẵn và để cho những người tham gia lựa chọn người đáng tin cậy, và chỉ báo agreeableness chứ không phải extraversion quyết định sự đáng tin cậy. Bài test thứ tư hết sức thú vị: những người tham gia được kiểm tra xem liệu họ có tìm cách thể hiện mình là một người dễ chịu để tăng mức độ tin cậy không thôn qua một trò chơi về đầu tư kinh tế. Với cơ hội được thử thuyết phục đồng đội rằng họ đáng tin cậy để được đầu tư, các thành viên tham gia thể hiện bản thân mang tính cách dễ chịu hơn là mang tính cách hướng ngoại.
