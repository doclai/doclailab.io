---
layout: post
title: "Những thế hệ xê dịch"
date: 2019-06-01T09:28:55+07:00
tags: [generations]
---

Lần trò chuyện này với anh D, tôi nhận ra rằng, đã có sự khác biệt trong suy nghĩ của anh D và những người khác. 
Trong khi đa số mọi người đều đưa ra rằng, dựa trên tự do cá nhân, thì việc đưa ra được những lựa chọn về vẻ đẹp dao kéo đều là những thứ có thể chấp nhận được, như một thứ thời trang, một thứ giao diện, rằng thực ra không có chuẩn đạo đức nào cả.
Nhưng hình như điều mà mọi người quên không đặt câu hỏi, đó là những thứ _"tự do"_ đó, được nhân rộng ra, cuối cùng sau một vài thế hệ sẽ đem lại một thứ xã hội gì? Chẳng hạn với câu chuyện privacy ở phương Tây, khi mỗi người đều thấy ổn khi để chính phủ kiểm soát được dữ liệu cá nhân, rằng họ nghĩ rằng _nếu tôi không làm gì sai thì chẳng có gì phải che giấu cả_. Nhưng bạn nghĩ sao khi có một (nhóm) người liên tục ghi lại những lời bạn nói, sau đó sử dụng tài liệu đó _đúng thời điểm_ để chống lại chính bạn và những người xung quanh bạn? Và với những thứ _"tự do"_ tương tự mà bạn nghĩ rằng điều đó chấp nhận được thì hãy nghĩ xem ai sẽ nhận được kết quả từ sự _"tự do"_ này?
Những thế hệ sau được _"thừa hưởng"_ sự _"tự do"_ theo cách hiểu của thế hệ trước.

<br>

Khi điều kiện sống được cải thiện tốt hơn, những thế hệ sau này có khả năng thực hiện những chuyến đi xa hơn và thường xuyên hơn cha ông của họ. Nhiều người cho rằng sự xê dịch sẽ là liều thuốc cho những vấn đề trong cuộc sống, một thứ không thể thiếu để có được một cuộc sống viên mãn và thú vị. Họ đọc một số bài viết trên blog nào đó, theo dõi một số video, rồi cảm thấy như được truyền cảm hứng, họ muốn sở hữu một "trải nghiệm thay đổi tâm hồn". Một cái gopro, bạn sẵn sàng ghi lại bất kỳ khoảnh khắc nào của chuyến đi. Và sau đó là quá trình dựng phim, bạn đưa những đoạn nhạc _"hợp lý"_ vào và cuối cùng, thu được một chuyến đi thật "sâu sắc" và "độc đáo". Họ mang theo thói quen và lối sống cũ đi xung quanh. Những chuyến đi như thứ công cụ thể hiện sự can đảm họ không có ở thế giới tinh thần, và cần thứ gì đó có thể chứng nhận. Điều gì đã khiến họ cảm thấy bất ổn nội tâm và lùng kiếm từ thế giới bên ngoài?

Những thế hệ xê dịch.

_Quốc tế thiếu nhi 2019_

