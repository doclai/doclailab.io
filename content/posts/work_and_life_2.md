---
title: "Công việc và đời sống (2)"
date: 2022-09-26T15:08:02+07:00
tags: [work, job, career, vocation]
toc: "false"
draft: false
---

Đây sẽ là một bài viết về chuyên mục công việc và đời sống, mang tính thực tiễn hơn, và ban đầu tôi nghĩ sẽ được để tại mục Nghĩ ngợi, thay vì mục Tâm lý như bài viết đầu tiên trong chuyên mục. Update lại cuối cùng thì phát hiện ra nên để tại mục Tâm lý.

Bài viết này xuất hiện trong thời điểm gặp sự nhập nhằng trong khái niệm của bản thân về định hướng nghề nghiệp, một phần vì lối sinh hoạt gần đây không được lành mạnh, ảnh hưởng tới sự tự ổn thỏa, phần vì chưa từng thực sự làm rõ về những ý tưởng này.

Trong bài viết này, chúng ta sẽ nhắc lại 4 khái niệm trong [mô hình](https://www.youtube.com/watch?v=0g7ARarFNnw) của Elizabeth Gilbert về hobbies, job, career, và vocation. Hobbies là thứ bạn làm theo sở thích, đơn giản bạn thích, không cần tạo ra thu nhập. Jobs là thứ bắt buộc có để sống trong xã hội này, trang trải các chi phí sinh hoạt hàng ngày, không cần phải là một thứ gì đó cực kỳ tuyệt vời. Careers cao hơn job ở chỗ bạn có đam mê, sẵn sàng làm thêm giờ bởi tin vào sứ mệnh. Vocations nó là tiếng gọi của bạn. Hobbies thì không nhất thiết cần có. Với jobs, bạn có thể thấy không được thích thú lắm, hoặc ghét nó, nhưng với careers, bạn nhất định là hoặc không có, hoặc thực sự yêu thích nó. Nếu mà bạn đặt tên cho một thứ bạn không thực sự yêu thích lắm là career thì vấn đề nảy sinh. Trong khi đấy, jobs cũng là một phần đời sống của bạn, nên như bài viết đầu tiên trong chuyên mục, nó sẽ ảnh hưởng nhiều đến tâm lý và sức khỏe tâm thần của bạn. 

Hãy chọn lựa một (hay một vài) jobs mà bạn cảm thấy tương đối ổn thỏa, không cần nhất thiết phải thỏa mãn toàn bộ các nhu cầu cá nhân, và dành thời gian còn lại cho hobbies hay vocation. Nhiều khi jobs sẽ chiếm hết thời gian cá nhân, tuy nhiên, lúc đấy, chúng ta cần xác lập ranh giới rõ ràng để theo đuổi chúng.

Sự tự ổn thoả trong đời sống ảnh hưởng tới cảm nhận chung về những việc làm hàng ngày. Hiểu những nhu cầu gì cấu thành nên sự tự ổn thoả sẽ giúp ta cải thiện được đời sống mình thay vì tìm kiếm sự tự ổn thoả từ một nguồn. Ta có thể kể tới [7 chiều kích](https://students.wlu.ca/wellness-and-recreation/health-and-wellness/wellness-education/dimensions.html) của sự tự ổn thỏa, đây tôi phân chia thành trục từ cá nhân tới cộng đồng: mental, spiritual, physical, environmental, financial, vocational, social, nhằm quan sát được dễ dàng hơn những thứ chúng ta hiện đang thiếu hụt và không tìm kiếm chúng ở những nơi không phù hợp. Tiện đây cũng ghi lại một ý tại buổi thảo luận tâm lý tuần vừa rồi với nhóm tại Hà Nội về tháp nhu cầu Maslow, rằng các nhu cầu đòi hỏi bậc thấp tỉ lệ nghịch với sự thỏa mãn sẵn có, trong khi các nhu cầu bậc cao thì lại tỉ lệ thuận.

Nhân nghĩ về một lời tư vấn, rằng cần tiết kiệm thời gian và công sức với con đường tối ưu nhằm tìm kiếm câu trả lời cho định hướng nghề nghiệp, của một người chị làm khoa học vân tay, trong khi lướt qua một bài viết Facebook với danh ngôn, đơn giản là, bạn bỏ hay không bỏ một người vì họ chú ý hay không chú ý gì tới bản thân mình, thì rốt cuộc các quyết định trong đời sống này đều mang tính hết sức cá nhân đi cùng với sự từ bi trên con đường tìm câu trả lời cho bản thân, cùng với những người đi qua ta trong đời sống này.