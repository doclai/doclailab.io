+++
image = "cross_indochina_04.jpg"
date = "2025-01-06"
title = "Đạp xe xuyên Đông Dương - Phần 4"
type = "gallery"
+++

Hẳn một bài dài đáng để viết cho hai ngày vừa trải qua.

## Ngày 14

Massage dầu nóng ở Lào không ăn thua, hệ thống chống rung thị giác vẫn không ổn, nên là tối lọ mọ lên con bạch mã lang thang trong thị trấn hỏi hết nhà này sang nhà khác xem có quán Đông Y nào không. E chừng là nếu đêm nay không tìm ra thì lại phải lội độ chục km sang bên phía bên kia biên giới, tức Thái Lan, kiếm chỗ. Đâu ngờ là đến quán thứ ba mình rẽ vào, chủ quán giới thiệu một bạn du học sinh Úc gốc Hongkong. Hai anh em bắn tiếng Anh một lúc, thì, nói lại khoe, cậu đấy bảo bất ngờ khi thấy có khuôn mặt châu Á mà nói tiếng Anh tự tin vậy á. Và sau khi mình cho xem hình ảnh cái mà mình muốn mô tả cho "liệu pháp thảo dược Trung Hoa" thì cậu đấy mới ồ lên bảo "bá quan", nếu mà bảo "bá quan" là cậu ta hiểu ngay. Cậu ta cũng bảo bên phía Thái Lan nhiều quán như này hơn, và cậu ta dạy tiếng Anh ở bên đấy, đang sang Thakhek này chơi. Anh bạn đi cùng cậu ta có vẻ là người ở đây, nên là chỉ cho mình một quán Đông Y trong thị trấn. Và mình tiếp tục lọ mọ tìm tiếp.

À thì rồi mình tìm ra được quán Đông Y đó. Nó ở ngoài đường chính nhưng không quá nổi bật, chỉ toàn chữ tiếng Trung và tiếng Lào, dễ khiến mình bỏ qua ngay khi vào thị trấn. Ngồi đợi nửa tiếng trước quán Đông Y đang sáng đèn thì bác chủ quán đi ăn tối về. Làm thủ tục bắt mạch, xông thuốc và bóp thuốc xong xuôi tới 9:30. Bác có dặn đi bộ về chứ đừng đạp xe, tránh hàn, tối đừng tắm, và sáng mai nhớ qua lại. Hệ thống chống rung thị giác thấy ổn hơn hẳn. Lọ mọ dắt con bạch mã đi bộ tầm 10h thì về tới nhà nghỉ.

## Ngày 15

Mình ăn sáng, làm thủ tục checkout, đi đổi tiền, rồi rẽ qua quán của bác ngay. Mình nài nỉ bác giảm giá cho do là lượt điều trị hôm qua xịn quá bị vượt budget mất hơn tuần của mình. Bác đồng ý rồi cho mình vào xông lượt nữa. Mình hỏi thì bác có kể bác từng mở quán từ 1983, hồi ở Vientiane, cơ mà do vấn đề tài chính, (và hỏi thêm thì bác kể tiếp rằng) cũng như là bác thích phong cảnh ở đây, nên đã rời tới Thakhek. Mình có để lại sự khâm phục khi người Lào họ không có trân trọng Đông Y Trung Hoa đâu, mà bác dũng cảm mở ở đây từ lâu vậy. Bác kể về sự tự tin với đôi bàn tay của bác với rất nhiều bệnh. Bác cũng có kể tiếp, rằng bác trọng y đức nên vẫn chữa cho mình sáng nay không có lãi. Bác cũng bảo thời tiết thế này rất tốt cho việc đạp xe, và làm đầy đủ thủ tục như này là ok rồi.

Xíu nữa ổn thì chạy xuôi xuống phía nam một xíu rồi hạ trại nghỉ ngơi.

