---
title: "Chuyển động. Về một vài chuyển động"
date: 2023-05-20T16:11:31+07:00
tags: ["apology", "movement"]
toc: "false"
draft: false
---

Nhân chuyện một lời mời tập thể hình (có cách gọi khác là tập _gym_, khác với tập _calisthenics_ chủ yếu sử dụng cân nặng cơ thể, hay là khác với với tập _gymnastics_ sử dụng xà và vòng treo) xuất hiện trong thời gian xáo trộn sinh hoạt hàng ngày, tôi quay về với bao cát, với tinh thần chuyển động cơ thể khởi xướng bởi Ido Portal. Những người có mong muốn cảm nhận cơ thể và di chuyển như tôi chắc hẳn sẽ thấy sự thiếu tự nhiên và sự tẻ nhạt trong cốt tủy triết lý lặp lại của tập thể hình, cũng muốn được ở lâu hơn với cơ thể này, chăm chút một cách hài hòa hơn, và sẽ tìm kiếm những cách để chuyển điều đó thành lối sống của bản thân.

Và câu chuyện chúng tôi bàn gần đây về lời xin lỗi, cũng như là tinh thần chuyển động cơ thể, gần lắm với những hình dung mơ hồ khi tôi thảo luận về chuyện cầm vô lăng trong khi đi trên đường. Hình dung này xuất phát từ góc nhìn khác nhau khi người kia kể ra quy tắc khi đi ô tô cần cách một khoảng khỏi mép đường, nhằm tránh những phương tiện từ trong ngõ đi ra. Điều gợn lên trong tôi là, vì sao họ lại nghĩ nó là một quy tắc, kỹ thuật riêng, trong khi có thể nhìn đơn giản hơn, là nhìn những con đường đó với sự căng tràn sức sống ở hai bên. Bất chợt tôi nhận ra người ấy hiện có vẻ không chú tâm đến sự sống đấy lắm, nên có thể hiểu về chuyện có những ngoại lệ, những quy tắc, cho từng tình huống.

Ngoài ra trong buổi thảo luận vừa rồi, sử dụng mô hình ở buổi, nếu nhìn mô hình dưới dạng hình thoi mối quan hệ giữa transgressor đối diện victim trên hàng ngang, với lời xin lỗi là điểm ở trên, chuyển động của một lời xin lỗi hiệu quả có thể nhìn thấy từ vòng tròn bắt đầu từ việc nói lên lời xin lỗi; sau đó cá nhân hóa bằng cách đi vào điểm nằm giữa (mối quan hệ) của transgressor và victim; tiếp thể hiện sự ý thức về tầm ảnh hưởng bản thân, đi qua điểm của transgressor; vòng lên trên một điểm mới ngay bên trên đó, là về giải pháp tương lai. Nếu nhìn như này, ta cũng có thể hình dung một cách đơn giản rằng chuyển động của một lời cảm ơn hiệu quả là một vòng tròn đối diện còn lại.
