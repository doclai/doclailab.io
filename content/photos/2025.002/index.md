+++
image = "2025.002.jpg"
date = "2025-02-04"
title = "Số 2025.002"
type = "gallery"
+++

Những thứ hay ho vài ngày qua

## Đọc

- [Gordon Lam Ka-tung](https://www.scmp.com/lifestyle/entertainment/article/3296870/who-gordon-lam-ka-tung-hong-kong-actor-and-producer-who-took-long-road-success): chú diễn viên này lớn lên ở khu Kowloon Walled City (KWC), xong tôi đọc [một bài](https://www.scmp.com/news/hong-kong/article/3263596/hong-kongs-kowloon-walled-city-best-south-china-morning-posts-past-coverage) về khu KWC khét tiếng này của Hong Kong, dẫn tới một cuốn sách tên  [City of Darkness](https://www.amazon.com/City-Darkness-Life-Kowloon-Walled/dp/1873200137) như tên gọi trong tiếng Quảng Đông cho thành phố này.
- [Nguồn gốc nem rán](https://www.scmp.com/lifestyle/food-drink/article/3296829/origin-spring-rolls-and-why-chinese-people-eat-them-celebrate-lunar-new-year): để so thử các trang mạng trong nước phân tích như nào.
- [Chuyện đi ra thế giới của đồ ăn Malaysia](https://www.scmp.com/lifestyle/chinese-culture/article/3296812/why-malaysian-and-singaporean-food-hasnt-gone-global-chinese-food-has): ở đây, chưa nói đến ẩm thực Trung Hoa, họ đang so sánh với ẩm thực Đông Nam Á, và có nêu tên Việt Nam nè.
- [Tiểu thuyết người lớn Onyx Storm](https://www.nytimes.com/2025/01/30/books/rebecca-yarros-onyx-storm.html): lập đỉnh kỷ lục mới trong tuần bán đầu tiên với 2.7 triệu bản, bán nhanh nhất trong lịch sử 20 năm của tiểu thuyết người lớn.
- [Những phim không được chiếu ở Malaysia](https://www.nytimes.com/2025/01/31/movies/malaysia-film-censorship.html): ví dụ trường hợp đạo diễn và nhà sản xuất phim "Mentega Terbang" (2021) (tiêu đề có nghĩa "Butterfly") bị luận tội báng bổ. Đây là một phim độc lập, kể về một cô bé Hồi giáo tò mò về những nền văn hóa khác. Ngoài ra có một bộ phim khác, không được lên sóng ở rạp, cũng được thảo luận. Bộ phim [Pendatang](https://www.kumanpictures.com/pendatang) về xung đột sắc tộc ở Malaysia, gọi vốn từ cộng đồng, và [chiếu miễn phí không quảng cáo](https://www.youtube.com/watch?v=yI50AwTDpiU) trên nền tảng YouTube.
- [Tội phạm trên Tiktok](https://www.nytimes.com/2025/01/31/arts/true-crime-streaming-tiktok.html): Có các series phim giới thiệu về những vụ này ([Dancing for the Devil](https://youtu.be/-CCG5RXbtwc), [TikTok Star Murders](https://youtu.be/m31uNmVp7hc), [Deadly Influence: The Social Media Murders](https://youtu.be/EAlm4iupmss), [Who TF Did I Marry?](https://www.tiktok.com/@reesamteesa))
- [No other Land](https://www.nytimes.com/2025/01/31/movies/no-other-land-documentary-gaza-west-bank.html): Bộ phim tài liệu về xung đột giữa Israel-Palestine, thắng hai giải ở liên hoan phim quốc tế Berlin, được đề cử Oscar, và vẫn chưa tìm được nhà phát hành tại Hoa Kỳ. Ở [một chia sẻ](https://www.nytimes.com/2024/12/18/movies/no-other-land-documentaries-distribution.html) trước đó hơn một tháng thì "[...] những nhà đài lớn không hứng thú với những phim tài liệu về vấn đề xã hội".
- [Hôn nhân và thị trường](https://aeon.co/essays/on-class-and-the-perils-of-the-new-norms-of-intimacy): tìm ra từ tường của một người bạn trên Facebook, câu chuyện tân tự do tác động tới tiêu chuẩn thân mật, và sau đó là hôn nhân.
- [Góc nhìn của Michael Crichton về công nghệ](https://www.newyorker.com/culture/infinite-scroll/what-michael-crichton-reveals-about-big-tech-and-ai): Cal Newport nổi từ lâu rồi, bài này lấy tương phản giữa Frankenstein với Jurrasic Park hay phết. Khả năng liên tục ý thức về medium của Cal vẫn đẳng cấp.
- [Synchrony-versus-asynchrony](https://www.newyorker.com/tech/annals-of-technology/was-e-mail-a-mistake): câu chuyện thay đổi hệ thống tube ở văn phòng CIA cho tới câu chuyện tính toán những khó khăn mới sau khi chuyển qua async work. Có link đến [một bài](https://blog.idonethis.com/asynchronous-communication/) về nghệ thuật async work. 
- [RFK Jr. lên bộ trưởng bộ Y Tế](https://www.newyorker.com/news/the-lede/the-junk-science-of-robert-f-kennedy-jr): có nhiều chi tiết của tinh thần của RFK Jr. về y tế cần tìm hiểu, cơ mà tinh thần lớn nhất của ông là phòng bệnh hơn chữa bệnh, giải quyết từ chế độ ăn, là bước tiến lớn. Cơ mà tác giả của bài báo này có đưa lên thông tin chia sẻ của em gái về thú chơi chim diều hâu trong quá khứ để nâng cao quan điểm về RFK Jr. như "một kẻ săn mồi". Và rõ ràng việc mang quá khứ một người lên thế này là một ứng xử tệ, được comment ngay đầu của [video Youtube](https://youtu.be/VFw24zVO_3E?list=RDNSVFw24zVO_3E). Tác giả của bài viết còn dành tới 100 từ cuối bài để trích dẫn thông tin này từ bà em gái thì càng tệ hơn. Sau đấy thì mình tìm tới bài [Ngụy biện trong Thượng nghị Viện Mỹ](https://www.facebook.com/share/p/1B9ybLq5Qp/) của chú Nguyễn Tuấn, trích lại đầy đủ chia sẻ tinh thần của RFK Jr.:
> Trong một podcast, khi được hỏi có vaccine nào an toàn không, ông ấy nói một số vaccine là an toàn, rồi thêm rằng không có vaccine nào an toàn và có hiệu quả cho mọi người: "Some of the live virus vaccines are. [...] There are no vaccines that are safe and effective ... for every person." Vậy mà một ông thượng nghị sĩ khẳng định rằng ông ấy nói "No vaccine is safe and effective." (không có vaccine nào là an toàn và có hiệu quả). Trích dẫn kiểu này rất ... ác độc. 
- [Gov info crisis](https://freegovinfo.info/node/14747/): mối liên hệ quan trọng giữa archive với democracy, nhá hàng cho cuốn sách mới ra của các tác giả _Preserving Government Information: Past, Present, and Future_. [Dữ liệu biến mất trên data.gov](https://www.404media.co/archivists-work-to-identify-and-save-the-thousands-of-datasets-disappearing-from-data-gov/).
- [DeepSeek hex prompt injection](https://substack.com/home/post/p-156004330): test hex bằng thông tin của Thiên An Môn luôn
- [Smartphone khiến ta cảm thấy không còn sự gợi cảm](https://catherineshannon.substack.com/p/your-phone-is-why-you-dont-feel-sexy): những thứ sexy thường phù du, hiện diện ngay hiện tại, nhưng điện thoại thông minh thì không có tinh thần như vậy. Tác giả có ý tưởng thú vị.
- [Tác động tốt của thư viện](https://lithub.com/its-official-research-has-found-that-libraries-make-everything-better/): Nghiên cứu "thư viện và sự ổn thỏa" của Thư viện công New York khảo sát với 1974 người dùng thư viện. Mà câu chuyện là giờ không gian online xuất hiện nhiều hơn thì ta có thể hiểu như nào về sự chuyển dịch này?
- [Discovery coding](https://jimmyhmiller.github.io/discovery-coding): coder viết theo phong cách khám phá. Cơ mà có lưu ý khác biệt giữa quá trình kiến tạo và sản phẩm cuối.
- [Rời khỏi New York Times](https://contrarian.substack.com/p/departing-the-new-york-times): kể lại hành trình 24 năm của một cây viết từng công tác ở tạp chí này. có blog dưới tên của 
- [Quảng cáo liên app](https://timsh.org/tracking-myself-down-through-in-app-ads/): dữ liệu gửi cho bên thứ ba từ IP tới cả tọa độ.
- [Sử dụng hàu giám sát nước](https://www.awa.asn.au/resources/latest-news/technology/innovation/polish-city-using-mussels-monitor-water-quality): mấy con hàu được kết nối vào cùng với một máy để giám sát chất lượng nước ở khu vực từng ô nhiễm tại Phần Lan
- [Tủ sách Nguyễn Văn Linh](https://thanhnien.vn/nho-tu-sach-nguyen-van-linh-trong-rung-gia-185250202145150722.htm): những năm trước 1975 mà trong tủ của các cụ đã có Dostoievski, J.P.Sartre, Anbert Camus, Herman Hess trong một tủ sách giữa rừng. Cụ Linh chọn mua sách từ Sài Gòn, "chỉ định mua, toàn là sách kinh điển, sách dịch, nghĩa là sách rất quý". Đẳng cấp thật.
- [Đội ngũ Elon Musk triển khai chính phủ AI-first](https://www.wired.com/story/elon-musk-lieutenant-gsa-ai-agency/): ngoài chuyện redflags nêu lên bởi một chuyên gia trong bài, cùng với chỉ ra khác biệt giữa chính phủ với xe tự lái, thì trong tài liệu training nội bộ của IBM vào 1979 [có ghi](https://simonwillison.net/2025/Feb/3/a-computer-can-never-be-held-accountable/) vài dòng "A COMPUTER CAN NEVER BE HELD ACCOUNTABLE. THEREFORE A COMPUTER MUST NEVER MAKE A MANAGEMENT DECISION."  

## Nghe

- [Dax - "Lonely Dirt Road"](https://youtu.be/UxANRMspilI)

## Nhìn

- [Phim tài liệu về cô bé với hijab](https://www.newyorker.com/culture/the-new-yorker-documentary/a-young-girl-questions-wearing-a-head-scarf-in-rizoo): ngoài chuyện hay nghe Max Amini, thì vẫn thấy phụ nữ Iran đẹp nha, và đây là câu chuyện thú vị về hijab của cô bé 8 tuổi đi chụp ảnh thẻ.
- [Josh Waitzkin: The Art of Learning & Living Life](https://youtu.be/wAnDWfEIwoE)
- [How the IMF & World Bank Exploit Poor Countries with Alex Gladstein](https://youtu.be/DnHOxZgvdWM)
- [Why We NEED Africa Poor (Documentary)](https://youtu.be/Ov_jAE-jUmw): dẫn nhập mối quan hệ IMF và châu Phi
- [Why Men Are Becoming Weaker](https://youtu.be/z4dhrrDbjlw): dẫn nhập câu chuyện vaping, vài từ khóa: propylene glycol làm dịu, popcorn lung, nicotine & testosterone, và policy 
- [Escape the middle class](https://youtu.be/H4dkwGBb3I4): về đồng tiền đang mất giá ngày càng nhiều, có ví dụ về đồng Việt Nam, hãy nghĩ về lạm phát.
