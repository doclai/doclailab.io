+++
image = "cross_indochina_08.jpg"
date = "2025-01-13"
title = "Đạp xe xuyên Đông Dương - Phần 8"
type = "gallery"
+++

## Ngày 20

Sáng sớm vào Pakse thấy mọi người hay đứng ở chỗ này checkin, gọi là Pakse Landmark, ngay bên cạnh chùa Wat Luang.

Đi qua một đoạn thì có đền (mình gọi là đền vì không có sư) của người Hoa với kiến trúc ba tầng. Ở cửa ra vào có ghi lại nguồn gốc xây dựng với mục tiêu có chỗ cho những con người gốc Hoa xa xứ có nơi thực hành đầy đủ nghi lễ tín ngưỡng trong các đợt tang lễ. Đền được xây từ hồi 2001.

Kết thúc ngày 20 ở Ban Nongsim

## Ngày 22

Giang hồ không hẹn mà gặp (phần 3)

Mình gặp đôi này ngay trước khi tới biên giới Lào - Campuchia. Họ khởi hành từ Malaysia, đạp về hướng Bắc.

Hồi còn ở Thakhek, mình từng nghe kể từ một cô gái Uzbekistan về chuyện qua cửa khẩu Nong Nok Khiene phải đợi rất lâu, giờ mới tận mắt thấy. Mình đi một mình nên nhanh, tổng cộng 30 phút qua hết các thủ tục.

Đồng Riel thì to hơn đồng Kip của Lào (1 Riel = 5 Kip = 6 Đồng), cơ mà 2025 rồi, đi hơn chục cây từ cửa khẩu mà đường vẫn toàn đá dăm. Nước bạn Campuchia làm đường chán quá, như này đi chậm đi gần một nửa tốc độ.

Tiện trên đất Cam, mình có dịp đọc thêm khi lướt qua đoạn bình luận của luật sư bào chữa cho cựu tổng tư lệnh Khmer Đỏ vào năm 2000. Nội dung đoạn đó như sau:
"Tất cả những người nước ngoài liên quan phải được triệu tập tới tòa, và không có ngoại lệ nào cả. Những Madeleine Albright (Bộ trưởng Bộ Ngoại giao Mỹ), Margaret Thatcher (Thủ tướng Anh), Henry Kissinger (Ngoại trưởng Mỹ), Jimmy Carter, Ronald Reagan và George Bush (3 tổng thống Mỹ)... chúng tôi sẽ mời họ đến để họ giải thích với thế giới lý do vì sao họ ủng hộ Khmer Đỏ"

À thì vào Campuchia cũng không suôn sẻ gì từ hải quan lâu tới chất liệu cái mặt đường =)) (Campuchia đang tổng sửa chữa quốc lộ 7, chưa biết đoạn không sửa thì như nào). Với cả dân camping kháo nhau liệu liệu camp phải mìn, chứ không lo lắm về an ninh như mấy anh em người Việt mình kể, nên cũng cảnh giác hơn với cần làm phiền dân địa phương một xíu.

Cơ mà thỉnh thoảng hành lý mình cũng "đổ xúc xắc" giữa đường. Ừ thì nó bảo mình dừng ở đâu, thì dừng lại thử. Mình tính vào xin nghỉ chỗ mình rớt đồ, thì có cơ hội gặp gia đình một bác trai bộ đội nghỉ hưu. Thấy mình bảo là người Việt, bác dùng tiếng Việt ngay, và bảo "Việt Nam - Lào - Campuchia, cứ vào đây". Bác dành cho mình chỗ sạch nhất còn trống để hạ lều. Bác dùng tiếng Việt giới thiệu gia đình bác, kể về trải nghiệm bộ đội từ 1979, rồi thời Pol Pot đội của bác từng phải vòng qua Pleiku để vào Phnom Penh do bị chặn, mất hẳn 7 ngày thay vì 3 ngày. 30 năm vừa rồi, bác công tác ở biên giới Campuchia - Lào, giờ sõi tiếng Lào hơn tiếng Việt. Bác mới nghỉ hưu, cũng là 30 năm rồi không dùng tiếng Việt, nên giờ dùng không còn tốt.
Con giai bác mời nước thốt nốt ngọt ghê. Bác có giới thiệu sáng mai vào Krong Stung Treng thử cafe. Ảnh kia là mình chụp với mấy đứa cháu đã lớn tướng, không phải con của bác =))

Cho tới hiện tại thì anh em nào còn giữ quốc tịch Việt Nam thì nên ghé qua thử Campuchia nhé, khỏi phải đóng Visa gần 1 củ tiền Việt.
