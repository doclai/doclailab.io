+++
image = "cross_indochina_02.jpg"
date = "2025-01-02"
title = "Đạp xe xuyên Đông Dương - Phần 2"
type = "gallery"
+++

## Ngày 8

Trở về từ Đà Nẵng, Hà Tĩnh đã gửi tới cái lạnh thấu xương. Trước 9h sương mù đường tới mức cách độ 200m phương tiện mất dấu hẳn. Và cuối ngày Hà Tĩnh cũng tiễn mình rời Việt Nam bằng sương mù.

Note: leo Cầu Treo bằng leo 3 con dốc Cun, lên liên tục

Đạp xe trong đêm ở Lào làm mình hiểu được ô nhiễm ánh sáng đã khiến mình lãng quên bầu trời đẹp như thế nào. Mình dùng suy nghĩ rằng ít ra là vẫn có những chùm sao (rõ cả chùm luôn, đẹp một cách hùng vĩ và ghê rợn) để duy trì động lực trong đêm tối giữa đại ngàn, cùng ánh đèn thỉnh thoảng lướt qua của mấy xe tải, đi kèm với mấy con chó Lào gợi đòn muốn nếm mùi lợi hại của gậy hiking mình mang theo.

Dù sao thì game đỉnh cao thể lực không dành cho tất cả mọi người. Vì game thể lực thực tế là game về hệ thần kinh. Với mình thì hành trình chứa rất nhiều những cuộc đối thoại với nội tâm, với những negative inner voice. Với những người khác, có thể còn hơn thế nữa.

Chúc ngủ ngon cả nhà từ Lak Sao, Laos.

## Ngày 9

Lak Sao gần giống vị trí Lào Cai, lạnh hơn Hà Nội, có phần do gần núi
Đón giao thừa ở chợ đêm Lak Sao

## Ngày 10

Tượng phật Naga, một vị ngồi thiền dưới 7 đầu rắn Mucalinda, thể hiện niềm tin về sự bảo vệ cho tỉnh giác bởi hoàng đế các loài rắn.

Mình bắt gặp tượng này khi vừa đi hết vườn quốc gia Nakai Nam Theun.

Dưới là view chỗ mình hạ trại, nhìn ra khu rừng ngập Nam Theun.

## Ngày 11

Mình ăn tạp và gặp các vị ăn chay đang đi ngược lên núi trong lúc đi xuống. Có ba vị tất cả, bộ hành cách nhau độ vài chục mét. Ảnh mình chụp vị chốt đoàn.

Note: ngày hôm nay đã chính thức có bạn thanh niên nhận nhầm mình thành người Lào sau khi Sabaidee khi ngồi ăn trưa ở chợ Nakai.

Chiêù, gặp chị chủ dân Quảng Trị, tặng mình cả lon nước ngọt với chai nước 1L, kín hết chỗ phải chằng ra sau. Quán chị ở gần Nhommarath.

Bỗng nhạc nhảy trong đầu khi dọn lều xong, cái cây cao lên xíu nữa làm poster phim được:

"Có chàng trai đỗ xe đây

Lều bung ra ngay dưới đấy

Sẵn đồ anh ấy mang theo

Đồ ăn mua cô gái ấy"

