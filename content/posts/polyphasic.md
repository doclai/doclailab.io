---
layout: post
title: Polyphasic in COVID era (1)
date: 2020-04-04T09:28:55+07:00
tags: [sleep, routine]
---
Trong thời gian làm việc thời kỳ covid, ở một môi trường liên tục bị quấy rầy, mình chắc chắn cần sử dụng một chiến lược khác, với việc điều chỉnh đồng hồ sinh học cá nhân.
Nhà có mấy đứa cháu nhỏ, nên là làm việc vào buổi sáng cực kỳ ồn, mà chẳng hiệu quả. Các thời gian yên tĩnh trong ngày là những lúc đa số mọi người nghỉ trưa, vào hơn 12PM tới gần 3PM và giờ ngủ của mọi người vào ban đêm là từ 11.30PM tới 5.30AM. Tận dụng được những khoảng thời gian siêu yên tĩnh này là lý tưởng nhất.
Và còn một số hoạt động khác như nấu nướng, cần thực hiện vào ban ngày, nên rõ ràng là vẫn cần những khoảng thức vào những thời điểm này.
Chiến lược sẽ là polyphasic sleep. Thực tế mình vẫn có thực hành một kiểu ngủ polyphasic là ngủ trưa (siesta), nhưng không tận dụng được thời gian siêu yên tĩnh kia.
Trong [link này](
https://www.mattressnerd.com/polyphasic-sleep/beginners/), có một nhược điểm của polyphasic cần cải thiện là tăng light sleep.
Và mình chọn được một chế độ bắt đầu lý tưởng, dành cho những người đã quen có siesta là ngủ kiểu DC1 (DC1 sleep). DC1 là một sự kết hợp nhẹ nhàng giữa lợi thế ngủ trưa/chiều (afternoon nap), và có ngủ ngắt quãng (segmented night).
Đây là một ví dụ cấu hình cho việc ngủ DC1:
![DC1](/images/dc1.png)

Thay vì chỉ có tổng cộng hơn 5 tiếng ngủ như biểu đồ thì mình sẽ đưa giới hạn cho bản thân là cần 6.5 tiếng, được chia thành các khoảng:

| partition | first core | second core | nap |
| ------------- | :-------------: | :-------------: | :---------------: |
| time | 4 hours | 2 hours | 20 minutes |
| range (covid) | 10PM - 2AM | 9AM - 11AM | 5PM - 5.20PM |
| range (normal) | 8PM - 12AM | 4AM - 6AM | 12PM - 12.20PM |

Các update mới hơn sẽ được đăng tiếp vào những bài viết sau.

