---
title: "Tri Âm - Mỹ Tâm 2023"
date: 2023-04-08T21:45:59+07:00
tags: ["music", "chance"]
toc: "false"
draft: false
---

Vừa mới trở về rạp chiếu phim. 

Mình không phải fan ruột của Mỹ Tâm. Mọi thứ đến với mình cũng thật là tình cờ khi đặt từ khóa "My Tam" lên tìm kiếm youtube khi kiếm nhạc vào hôm qua. Trailer của bộ phim hiện lên và thông báo ngày mai là ngày công chiếu.

Sáng nay bê loa bluetooth mở đi mở lại "Hẹn ước từ hư vô". Mình đặt vé trước khi chiếu có 3 tiếng. Ngồi trong rạp, mình cứ nổi da gà suốt khi nghe câu chuyện của Mỹ Tâm với những giai điệu gần bằng tuổi mình. Mỹ Tâm là một con người sống bằng cảm xúc, như cô ấy đã nói trong phim, đã đi qua tất cả câu chuyện như vậy, khiến mình tự đặt câu hỏi về sự dũng cảm để cảm nhận được đầy đủ những trải nghiệm đi qua hàng ngày. Nó gợi mình nghĩ về sự cứng nhắc, về câu chuyện thiền tập hàng ngày của mình, mình đang đóng khung hay là đang quan sát thật sự.

Mình đến với bộ phim, đến với câu chuyện của Mỹ Tâm, như là "mình không tìm nhau", "để gặp nhau chỉ khi vừa ngày giờ", cảm ơn đã được "bước chung đoạn đường đời".


