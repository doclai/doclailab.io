---
layout: post
title: Chuyện trở về với đời sống một tôi (1)
date: 2024-04-11T09:28:55+07:00
tags: [living, reflection]
---

_(english below)_

Cần một thời gian tương đối dài để tôi chịu đưa ra quyết định rằng việc chuyển ra ở riêng là một việc quan trọng. Và cũng chẳng định viết gì cho sự kiện tôi rời ra ở riêng. Cơ mà chợt nhận thấy, gần đây khi tôi gửi blog cũ cho những người bạn mới, cũng trùng hợp trên Facebook có những người lại bất ngờ với comment cách đây 3 năm của tôi (bản thân đọc comment cũng thấy sự khác lạ trong cách hành văn với cách suy nghĩ thời điểm đó nữa), nên lại thấy tôi càng cần ghi lại.

Có thể bắt đầu với chuyện thói quen, khi nghĩ về chuyện gặp trục trặc với gia đình khi tôi tách ra ở riêng. Hoàn cảnh nhìn qua, thì có vẻ là không cho phép thật, nhưng đặt lên bàn tất cả, thì thấy dù sớm hay muộn, hành trình đơn độc tôi vẫn cần trải qua. Và nhờ hiểu sức ảnh hưởng không hề nhỏ của những lời than phiền nhẹ nhàng từ phía gia đình tới tôi, tôi càng thấy việc tách ra ở riêng là một chuyện cần làm, để tôi nhìn thấy và xây dựng con người thật của tôi. Nhu cầu tìm một nơi là "nhà" len lỏi trong những chuyến đi xa vào rừng, lên núi, và rồi khi tôi đặt chân tới một nơi riêng của tôi, hiện tại là một căn phòng riêng đủ xa gia đình, và trong thời gian đủ dài, tôi đã hiểu được điều cốt lõi vì sao tôi đã từng xách ba lô lên và đi thường xuyên như vậy.

Đi tiếp chuyện thói quen thì là về sự ổn thỏa. Có lẽ câu chuyện mâu thuẫn giữa người với người vẫn là như thế nào thì ổn thỏa. Với những người nhà tôi thì họ hình dung tới chuyện, và cũng như chia sẻ về hiểu lầm quen thuộc của người Việt ta mà gần đây tôi được nghe, rằng sự phát triển là một lần và mãi mãi. Điều này dẫn đến những câu chuyện kể quanh năm về "sự hy sinh", "sự đền đáp" khi một cá nhân đã thấy sự trống rỗng, họ từ lâu không còn chăm sóc bản thân nữa, và đã có thói quen đưa thời gian đó đầu tư hết vào những chuyện khác bên ngoài.

Thời gian tới sẽ dành thời gian đọc và chiêm nghiệm nhiều hơn, như cách đây 7 tháng. Thấy văn viết tiếng Việt của tôi cũng uyển chuyển thật.

