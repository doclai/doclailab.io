---
layout: post
title: Off the grid
date: 2022-02-03T09:28:55+07:00
tags: [books, lifestyle]
---

<img style="float: right" src="../assets/off_the_grid.jpg">

Tiện có dịp đón Tết một mình, trong lúc lang thang trên mạng tìm chính xác các thiết bị cho một cuộc sống xanh trong tưởng tượng của mình, mình kiếm được cuốn này. Thời gian trước, tác giả ở một căn nhà rất rộng, chi phí duy trì lên tới 3500 đô một tháng, mà thấy thừa thãi. Tác giả muốn một cuộc sống đơn giản hơn, tiêu dùng ít hơn. Trong cuốn sách, tác giả từng chia sẻ trải nghiệm trước đó: sống độc thân, cùng hai con chó, trong căn hộ 157 m2, và hàng đêm thường mất ngủ để nghĩ cách duy trì chi phí của nó. Tác giả từng thử những cách khác nhau như đi thuê nhà, sống ở những cabin bé, nhưng rồi cách rẻ nhất như tác giả là sống off-the-grid. Nghĩa gốc của off-the-grid là cuộc sống ngắt kết nối khỏi lưới điện chung, nhưng rồi có thể rộng hơn là các tiện ích công như nước sạch, gas, đường dẫn thải. Cuốn này mô tả sơ qua các bước để thay đổi lối sống, để xây dựng một ngôi nhà off-the-grid, và về những trải nghiệm và bài học khi tìm kiếm địa điểm, tìm kiếm thông tin, làm việc với con người trong quá trình xây dựng lên căn nhà hiệu quả, tiết kiệm như ý muốn.