---
layout: post
title: Thức giấc
date: 2021-09-04T09:28:55+07:00
tags: [health, sleep]
---

Tôi từ chối các buổi đi xuyên đêm mà biết là sáng hôm sau tôi sẽ không thức dậy bắt đầu ngày mới tĩnh lặng.

Một cách nhẹ nhàng, từ tối hôm trước sẽ tắt nguồn điện thoại, để nó ở cách giường ít nhất 2m. Nó sẽ tự động bật lên nửa đêm và báo thức vào sáng hôm sau. 

Và những phút đầu tiên trong ngày, ngoài dành thời gian vệ sinh cá nhân, tôi tự bảo bản thân hãy làm việc gì khiến bản thân có cảm giác tạo tác nguyên bản, mang tính người nhất. Điều này cốt khởi động lại sự linh hoạt vốn có của khả năng quan sát nhân văn, chẳng hạn dành thời gian đọc viết, hoặc ngày khác tôi sẽ nghe một miêu tả nào đó về thế giới, như một sự chăm sóc bản thân. 
Bên cạnh đó là thực hành từ chối mở các ứng dụng mạng xã hội của smartphone cho tới 9h sáng.

Có một thói quen trở nên hữu dụng hơn trong thời gian giãn cách là ngắm trời. Trong thế giới ngày càng nhỏ bé chật hẹp của nhà cao tầng và những bận bịu, mất kết nối nhiều hơn với thiên nhiên thì bầu trời là thứ vẫn hiện diện trên đầu chúng ta. Và luôn luôn, trong cái đầu nhỏ bé của chúng ta, dễ dàng nhất là có những hình mẫu (pattern) nhắc nhở chúng ta về một cấu trúc tính người toàn vẹn, chẳng hạn như bầu trời.

Trong khi làm với thủ tục hàng ngày, điều quan sát được là tôi dễ bị chuyển từ việc nhắm tới việc biến kế hoạch thành sự thật, hay sự toàn vẹn (wholeness) sang nhắm tới sự hoàn hảo (perfection) khi đạt mức độ nào đó. Nhìn thấy đây là vấn đề của tôi trong suốt thập kỷ qua. Nó cắm rễ ở cái tâm lý học sinh chuyên chọn, kiểu "một nghề cho chín còn hơn chín nghề" bê vào đời sống nội tâm một cách vụng về. Khi đó, tôi lại giật mình thấy, sự tự đánh đồng perfection và mastery, dù vẫn đang làm nhiều hoạt động lĩnh vực khác nhau, tôi vẫn có những thứ chưa hề chạm tới.

Khi tạo dựng bao nhiêu ở thế giới bên ngoài, bản thân cũng cần đầu tư tương tự ở thế giới nội tâm.
