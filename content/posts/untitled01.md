---
layout: post
title: không đề
date: 2016-12-31T09:28:55+07:00
tags: [poetry]
---

Tôi đi nhầm qua một đoạn phố đông

Trông người ta bước cạnh nhau cười cười nói nói

Tôi cũng cười vu vơ cho một niềm mong mỏi

Phố sẽ đủ dài cho mấy kẻ yêu nhau.

Tôi đưa lòng mình lên tận một tầng cao

Thấy thành phố một màu dẫu về đêm ngàn đèn xanh, đỏ

Cũng gắng mắt định hình những mờ mờ tỏ tỏ

Cũng nghĩ rằng sẽ có một người đợi mình

Đâu đó

Dưới kia.

Tôi chạy vào một con hẻm về khuya

Ngước nhìn lên những cửa sổ khép hờ chẳng đóng

Chỉ thêm mủi lòng khi trên kia hai chiếc bóng

Đành dụi mắt mình

Đường cô độc

Dừng chân.

Chợt thấy dáng ai đó đến bên cạnh, rất gần

Ôm lấy tôi giữa ban mai như tất cả vốn là điều thân thuộc

Là thật chứ chẳng phải tôi đang lạc mình trong giấc mơ lem luốc

Khi vòng tay người ấm quá hóa an nhiên.

Rồi giật mình giữa một chốc bình yên

Tự mở mắt thấy riêng mình giữa Sài Gòn chưa kịp sáng

-Ngọc Hoài Nhân-
