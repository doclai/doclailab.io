+++
image = "cross_indochina_05.jpg"
date = "2025-01-07"
title = "Đạp xe xuyên Đông Dương - Phần 5"
type = "gallery"
+++

## Ngày 16

Giang hồ không hẹn mà gặp (phần 1+2)

Phần 1 thì mình chưa đề cập, cơ mà ở trên ảnh cover 7 ngày đầu tiên vẫn lang thang trên đất Việt Nam trước khi qua Cầu Treo, thì mình đã gặp một anh giai người Đức trên đường. Tiện hôm nay đăng lại.

Bạn có thể đếm mấy trăm km chạy qua trong ngày để tránh sự tẻ nhạt trên đường (như một người anh chơi phân khối lớn đã khoe và gạ gẫm mình trước và trong chuyến đi) nhưng làm sao kiếm được cái cảm giác kết nối với từng mét và từng sinh vật gặp được trên đường trong suốt hành trình như xe đạp.

Giang hồ nhận nhau dễ lắm, nhìn kiểu xe "thồ" là bắt sóng nhau ngay.

Anh đầu tiên mình gặp ở Nghệ An, đạp cùng chiều với mình xuống phía Nam. Hành trình (cho tới hiện tại đã 7 tháng) của anh bắt đầu từ Đức đi qua Kazakhstan với vùng Tây Bắc Trung Quốc xuôi xuống Việt Nam với đích đến là Úc. Chia tay nhau ở Nghệ An, anh ấy đi Cầu Treo, và mình tiếp tục đi Hà Tĩnh (ban đầu kế hoạch mình định vào tận Lao Bảo sang Lào, nhưng sau đó thì quyết định quay trở lại gửi xe ở Hồng Lĩnh, gần AH15 sang Cầu Treo, vào ngày thứ 7 của hành trình)

Và hôm nay, trước khi tiến vào Xeno, thì gặp một anh người Pháp chạy ngược lên phía Bắc. Khác với anh trên thì anh này khởi hành từ Auckland, New Zealand để trở về quê hương. Hành trình của anh này đã đến ngày thứ 352. Nghe được mình đang hướng xuống Campuchia, anh có kể gần đây gặp một cô gái đạp solo đang đi hướng đó.

Chúc ngủ ngon từ Xeno, Laos

