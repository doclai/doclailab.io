---
layout: post
title: Mười ngày im lặng
date: 2022-06-18T09:28:55+07:00
tags: [vipassana]
---

Mấy ngày sau khi về với thế giới thường nhật mình mới bắt đầu viết về chuyến đi này. Bắt đầu ghép lại các ký ức  Một trong những điều mà ban quản lý khóa thiền dán ở trên tường rằng “xin quý vị thiền sinh không viết hay ghi chú trong suốt khóa thiền”. Tôi thì không mang theo giấy bút để ghi chép, đúng như đã cam kết, nhưng tôi có mang theo một con dao nhỏ và đã thử khắc lên trên cái cốc nhựa của tôi những suy nghĩ mình tâm đắc. Rồi tôi mới bắt đầu hiểu vì sao tôi không nên làm như vậy. Ghi chép bắt đầu mớm thức ăn cho cái tâm đi lang thang một cách, hoặc mạnh mẽ hơn để ghi chép được nhiều hơn, hoặc tinh tế hơn nhằm kiếm được những ý tinh túy hơn để ghi chép. Không biết với những người khác trải nghiệm ra sao nhưng lúc thiền tự bản thân sẽ thấy rằng đơn giản việc khắc chữ như vậy cũng khiến cho tâm tôi đi lang thang một cách mạnh mẽ hơn, có khi dẫn tới việc ngưng việc thiền giữa chừng khi bộ nhớ tạm không còn đủ chỗ cho những ý có thể viết xuống.

Mười ngày ở đó tôi có cơ hội được ngồi trong yên lặng. Tôi quen với sự im lặng tới khi quay trở về với đời sống thường nhật, tôi thấy rõ sự bình thản của tôi khi tương tác với người khác.

Những ký ức hiện về. Ngay cả những chuyện xảy ra với cách thực hành thiền cũng như câu chuyện tôi đi học thời phổ thông: cách học bài, những năm tháng ốm đau.

Những người ngủ ngáy gần bạn. Nếu bạn thấy khó chịu những ngày đầu khi làm quen với việc ngủ cạnh người ngáy, bạn có thể mang theo bịt tai. Tôi thấy một sự kiện xảy ra là càng về những ngày cuối, tâm trí bản thân có thể thay đổi sự tập trung. Có thể nhờ khả năng quan sát này mà tôi thấy chuyện ngáy của những người khác không diễn ra lâu và khó chịu nữa, hoặc có một sự chuyển biến nào đó thực sự đã xảy ra về mặt sinh học với họ.

Một chuyện vui sau đó nữa là có thể thuyết phục được gia đình cho mẹ tôi cũng tham gia được một khóa. Thời điểm bước chân vào cuộc thảo luận của gia đình, bắt đầu chạm tới những câu chuyện tương đối nhạy cảm của những người lớn tuổi, mới thấy sự bình tĩnh học nhờ việc hành thiền được thử thách. Lúc viết này tôi lại nhớ tới một short video của JulienHimself với nội dung đại loại rằng, bạn đứng lên cho giá trị gì thì mọi người xung quanh sẽ thử thách giá trị đó.