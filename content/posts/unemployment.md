---
layout: post
title: Thất nghiệp lành mạnh
date: 2022-06-23T09:28:55+07:00
tags: [tlud]
---


![](../014s.vi.jpg)
&copy; Unemployed, Art Painting by George Grosz

Trong nghiên cứu về cuộc sống thất nghiệp người Úc vào năm 1993 của Sue Jackson và Mary Crooks "Existing but not living" (Tồn tại nhưng không sống), những người thất nghiệp họ trải qua một vài tháng đầu với cảm giác khá lạc quan, tự tin với khả năng kiếm được việc, đặc biệt là người trẻ, khi họ miêu tả quãng thời gian này như "thời gian đi nghỉ". Quá trình tương tác xã hội của họ vẫn diễn ra, như trước khi họ thất nghiệp, cho tới khi họ gặp hiện tượng co cụm về đời sống xã hội, khi họ bắt đầu không đủ khả năng chi trả cho các sự kiện, và khiến họ cô lập hơn với mọi người xung quanh. Một anh chàng 24 tuổi ở thành phố Ballarat của Úc, chia sẻ:

"Bạn không có đời sống xã hội. Ở nhà, cố gắng cắt giảm hóa đơn. Kiểu như sau khi trong chương trình trợ cấp thất nghiệp quá lâu, hơn hai năm, chúng tôi sắp xếp để duy trì tài chính được trong ngần ấy thời gian, nhưng sẽ sinh hoạt dưới mức trợ cấp ngày qua ngày. Và mức trợ cấp đó rất thấp."

Vấn đề cũng bắt đầu xuất hiện ở trong mối quan hệ gia đình, khi những người làm kinh tế chính không có đủ tài chính cho trẻ đi học, hay những người con gặp vấn đề khi chia sẻ với bố mẹ. Sự lạc quan ban đầu đó dần thay thế bởi nỗi chán trường, trầm cảm, mất tự trọng. Đời sống cá nhân của họ thay đổi rõ rệt. Họ ngày càng khó duy trì động lực, và trở nên dễ dãi hơn về tiêu chí việc làm. Những vấn đề về sức khỏe, thể chất và tâm lý, cũng lộ diện. Một bác trên 50 tuổi ở thành phố Ballarat của Úc, chia sẻ:

"12 tháng qua tôi đã bị loét - không vui vẻ lắm. Bạn cứ lo lắng, và bạn nghĩ cho bản thân; nào, hãy dừng lo lắng, lo làm gì; và nó quay lại, rồi bạn trở thành kẻ lừa đảo. Bạn phải cố gắng và tìm số tiền này từ đâu đó. Vậy chúng tôi làm gì, chúng tôi đơn giản không ăn. Chúng tôi lấy tiền từ ngân sách thực phẩm, không thì chúng tôi không mua được xăng cho ô tô, đồng nghĩa rằng chúng tôi không thể đi đâu cả. Tôi không thể làm các công việc tình nguyện vì đồng nghĩa với quãng đường khứ hồi 60km về Ballarat, và 40km tới Maryborough."

Để đối phó với thất nghiệp, trong nghiên cứu này, nhiều người nói về nỗ lực của họ xây dựng các hoạt động, cấu trúc và các thủ tục hàng ngày. Với một số người, đó là tham gia các công việc tình nguyện, với một số người khác thì đó là kiếm các việc xung quanh nhà và làm chúng. Một bác 50 tuổi khác chia sẻ:

"Tôi giữ bản thân khá bận rộn. Tôi làm việc khá nhiều, xung quanh sân vườn, và ra ngoài tìm kiếm việc làm."

Trong buổi tối này, chúng ta cùng chia sẻ, thảo luận các góc nhìn về câu chuyện thất nghiệp, ảnh hưởng giai đoạn này tới tâm lý, và cách sống trong giai đoạn này một cách lành mạnh.

