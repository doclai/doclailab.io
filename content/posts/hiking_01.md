---
layout: post
title: Leo núi như một thói quen
date: 2024-01-04T10:13:55+07:00
tags: [hiking, habits]
---

Sau chuyến đi trên Chư Yang Lak, câu chuyện tôi đang suy nghĩ là về những chuyến đi tiếp theo, những công việc còn sót lại dưới đồng bằng, và việc làm sống lại blog này dưới tên miền `blog.doclai.com`. Một điều chuyển biến nữa là về cách viết và cách tôi nghĩ về chuyện viết. Chuyện này xảy ra khi tôi bắt đầu đọc lại, y như tên blog, những gì tôi từng đọc.

Nhờ dùng điện thoại eink gần đây, tôi dành thời gian để đọc lại, y như tên blog, nhiều hơn với Rilke và Tagore. Tôi bắt đầu nghĩ tới chuyện leo núi như là một thói quen mới hơn là các chuyến đi, nhờ có Hàm Lợn ở gần Hà Nội. Điều day dứt vẫn là chuyện đỉnh Hàm Lợn ở Hà Nội chỉ khô ráo từ tháng 11 cho tới tháng 3, chỉ phù hợp cho việc leo trong ngày.

Một điều để nhắm tới các khu rừng ở khu vực Tây Nguyên là về đặc trưng của đất nơi đây khiến địa hình có suối tiện cho việc ở lại một thời gian dài. Một trong số các yếu tố để trải nghiệm ngoài thiên nhiên được trọn vẹn, mà không phải nơi nào cũng có, là nguồn nước sạch. Với các khu rừng miền Bắc hầu hết là đất feralit, đất mùn pha cát, đất phù sa thì khó có thể giữ nước lại tạo thành suối. Trải nghiệm vừa rồi ở ĐakLak thực sự là một trải nghiệm đáng giá để một người có thể tận hưởng và trân trọng đời sống ngoài thiên nhiên với vẻ đẹp tràn đầy đích thực.

Một vài kế hoạch bắt đầu nhen nhóm, với một chuyến 7-14 ngày ở ẩn trên dãy Chư Yang Sin, song song với các chuyến "một đỏ" hàng tuần cho các bạn nhỏ ở Hàm Lợn.
