---
title: "Trang chủ"
---

~~Blog này ghi lại những bận tâm của tôi.~~

~~Lúc cảm thấy sự cô độc tràn trề trong tâm trí, không bạn bè, chẳng ai quan tâm thứ bạn làm tốt hay dở, không ai trả tiền cho bạn, khi đó, bạn muốn làm gì? Đó là câu hỏi tôi vẫn thường gặp.~~

Cập nhật 2025:

Blog này ghi lại những hành trình của tôi.

Bạn có thể liên hệ với tôi qua thư điện tử: [toi@doclai.com](mailto:toi@doclai.com)
