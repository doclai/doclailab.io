---
title: "Mối quan hệ. Căng thẳng. Thuốc."
date: 2023-09-19T06:05:11+07:00
tags: []
draft: false
---

Thảo luận về chuyện bị [mù trong thời gian sau của cuộc đời]()

Câu chuyện về [những con cá mập](https://www.psychologytoday.com/intl/blog/animal-emotions/202307/what-its-like-to-be-a-shark-dispelling-gory-myths)

Trong khi đó, [một cảm xúc dễ bị hiểu lầm nhiều nhất](https://www.psychologytoday.com/intl/blog/liking-the-child-you-love/202309/the-most-misunderstood-emotion) đó là sự giận dữ qua gợi ý từ một cuốn sách năm 1989 Anger: The Misunderstood Emotion remains 

Một thảo luận khác về [niềm vui của phát triển bản thân](https://www.psychologytoday.com/intl/blog/what-doesnt-kill-us/202309/the-joy-of-personal-growth)

Điểm quan trọng cho một mối quan hệ đủ đầy [là các tình bạn, chứ không phải hôn nhân](https://www.psychologytoday.com/intl/blog/living-single/202309/the-key-to-a-fulfilling-life-its-friendship-not-marriage), theo một khảo sát đầu năm nay bởi Pew Research Center tại Hoa Kỳ. Tuy người đã kết hôn đặt sự quan trọng của hôn nhân trong cuộc đời có tỉ lệ cao hơn tỉ lệ của những nhóm người khác (đã ly hôn, độc thân, có bạn tình), tất cả họ đều đặt chuyện có một công việc/sự nghiệp ưa thích lên hàng đầu. Vị trí theo sau đó là chuyện có bạn thân, với nhóm khảo sát tổng quát, và đối với nhóm đã kết hôn, thì là chuyện có con. Tuy có một thứ tự như vậy nhưng không có nghĩa là cuộc sống độc thân hạ thấp giá trị của hôn nhân. Ta có một thảo luận sâu hơn về chủ đề độc thân ở một [bài báo khác](https://onlinelibrary.wiley.com/doi/epdf/10.1111/jftr.12525) với một bài ghi chép riêng ở [đây](./researches/single_flourishing/).

Câu chuyện về [mối quan hệ cộng sinh](https://www.psychologytoday.com/intl/blog/explorations-in-positive-psychology/202309/creating-human-symbiotic-relationships) bàn về việc chuyển dịch từ mối quan hệ theo chiều dọc (vertical, không cân bằng về quyền lực, hiểu biết, hay thẩm quyền, ...) sang mối quan hệ chiều ngang (horizontal), luôn nhắc nhở ta rằng ta đang ở trên hành trình gỡ bỏ dần sự điều kiện hóa mà chúng ta có từ nhỏ. Hơn nữa, kể cả trong mối quan hệ chiều dọc, mang đặc tính thứ bậc khi mà một bên gia tăng cảm giác trách nhiệm, một bên gia tăng cảm giác phụ thuộc, việc thực hành sự nhân từ để có thể kiểm soát được mối quan hệ, được vận hành thực sự, mang tới kết nối mang tính cộng tác.

[Tiếp cận với các trải nghiệm khó khăn](https://www.psychologytoday.com/intl/blog/the-care-we-need/202309/why-approach-difficult-experiences) hơn là đè nén những trải nghiệm đó xuống, có tác dụng về mặt dài hạn. Hoạt động này cũng xuất hiện ở phương pháp Prolonged Exposure (hoặc Exposure Therapy) được nghiên cứu nhiều về PTSD. Xử lý các khúc mắc với các ký ức và đưa vào hình dung cuộc đời chính là chấp nhận sự kiện đó và đưa nó vào sự tự lý giải về self-concept. Qua việc khó khăn trong việc duy trì ghi nhớ có một phần do "ký ức chưa ngủ yên", hình dung ở bản thân có nhiều điểm giao nhưng không chứa toàn bộ các ký ức. Việc ghé lại các ký ức cũ tạo cơ hội cho mình nghĩ khác về những chuyện đã xảy ra. Ngoài ra việc tránh né các ký ức còn tránh đi các yếu tố liên quan tới ký ức ở đời sống thực. Tác giả có chia sẻ về một sách bài tập họ mới ra "Making Meaning of Difficult Experiences: A Self-Guided Program"

Gần đây ngồi cafe cùng với người bạn (chia sẻ ở một bài viết khác), câu chuyện [tìm kiếm sự bình yên trong bản thân ta](https://www.psychologytoday.com/intl/blog/with-love-and-gratitude/202309/finding-peace-and-serenity-within-ourselves) luôn là một câu chuyện khó khăn. Trong một nghiên cứu vào năm 2014, các chiều kích khác nhau để đo lường về mức độ bình yên, và các tư tưởng liên quan tới trải nghiệm bình yên như: cảm giác của cảm xúc bình tĩnh (emotional tone/sense of calm), sự tiên phong (agency/locus of control), lạc quan (hope/optimism), khoan dung với người khác (tolerance of others), có các nhu cầu cơ bản (access to basic necessities), an toàn cá nhân (personal safety/absence of violence), kết nối cộng đồng (a sense of group or social connectedness). 8 bước gợi ý để đạt được sự bình yên:
- Dành thời gian vào buổi sáng
- Tắt các thiết bị điện tử gây xao nhãng
- Sử dụng nhật ký về sự biết ơn và sử dụng thường xuyên. Khi ở trong môi trường không có xao nhãng, ta có thể tập trung vào sự biết ơn và thiền tập
- Tập hình dung sáng tạo
- Ý thức về sự may mắn
- Trân trọng sự dâng cao của trực giác
- Tạo một danh sách những việc cần làm và phân bố thời gian trong ngày
- Sử dụng các kỹ thuật kiểm soát thời gian để tới cuối ngày bạn có một giấc ngủ thoải mái

Anhedonia có nghĩa là không có khả năng trải nghiệm niềm vui thích, là một dấu hiệu phân biệt cho trầm cảm. [Một thảo luận về việc vượt qua anhedonia](https://www.psychologytoday.com/intl/blog/beyond-mental-health/202309/is-it-possible-to-enjoy-things-while-depressed) chia sẻ về các bước trước và sau hoạt động để thay đổi tác động của trầm cảm với hoạt động vùng não bộ liên quan tới sự tưởng thưởng, bằng thay đổi suy nghĩ và hành động (các phương pháp trị liệu tương tự như phương pháp CBT, thường đa số mọi người biết tới chiều ngược lại nhiều hơn). Ta có thể bắt đầu các sự kiện bằng việc lấy lại niềm hy vọng, nhẹ nhàng linh hoạt, và hiện diện ở thực tại; và hơn nữa, sau các sự kiện đấy, ta hãy tự tưởng thưởng và ghi nhớ lại các ký ức đẹp. Câu chuyện về sự hiện diện ở thực tại từng được thảo luận ở bài viết hôm qua.

[Mở khóa khả năng tiếp nhận phản hồi sinh học cho quản lý căng thẳng](https://www.psychologytoday.com/intl/blog/the-discomfort-zone/202309/unlocking-the-power-of-biofeedback-for-stress-management)

[So sánh tham vấn online với gặp mặt](https://www.psychologytoday.com/intl/blog/keeping-an-even-keel/202309/is-online-therapy-as-good-as-face-to-face)

[Nghiên cứu về chiến lược đối phó với thảm họa ở người trẻ](https://www.psychologytoday.com/intl/blog/disaster-by-choice/202309/children-and-adolescents-coping-with-disaster)

[Văn hóa đổi mới ở thung lũng Silicon](https://www.psychologytoday.com/intl/blog/the-power-of-experience/202309/learning-from-the-psychology-of-silicon-valleys-innovation)

[Những hiểu lầm về rối loạn sử dụng chất](https://www.psychologytoday.com/intl/blog/mind-matters-from-menninger/202309/5-common-misconceptions-about-substance-use-disorders)

[Sự mất tập trung về con người trong điều chế thuốc](https://www.psychologytoday.com/intl/blog/finding-meaning-in-lifes-struggles/202309/keeping-the-focus-on-humans-in-medicine)

[Nghiên cứu về tỉ lệ ly hôn của các cặp vợ chồng có con tự kỷ](https://www.psypost.org/2023/09/decades-long-study-explores-divorce-rates-among-parents-of-children-with-autism-207653)

[Tự do trong công việc](https://aeon.co/essays/what-kant-can-teach-us-about-work-on-the-problem-with-jobs?utm_source=rss-feed)

[Sự tận tâm có thể dẫn bạn tới bất cứ đâu](https://aeon.co/videos/trek-alongside-spiritual-pilgrims-on-a-treacherous-journey-across-pakistan)

[Homesick](https://longreads.com/2023/09/18/homesick/)

[Purple-haze](https://longreads.com/2023/09/18/purple-haze/)
